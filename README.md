# RcdMathLib

**RcdMathLib**: mathematical library for resource-limited devices. 
This library can be deployed for devices such as microcontrollers, 
embedded systems, or full-fledged devices like a PC. The 
[eclipse_projects](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/eclipse_projects) 
repository-directory contains an Eclipse project of resource-limited devices 
as well as of full-fledged computers. Read the **RcdMathLib** 
[documentation](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/doc/doxygen) 
to get started. 

## LICENSE

The code developed is licensed under the GNU Lesser General Public License (LGPL) version 2.1 as published
by the Free Software Foundation. All code files contain licensing information.

Author: Zakaria Kasmi