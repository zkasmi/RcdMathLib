/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Functions of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include "dist_based_position.h"

#include <math.h>

#include "matrix.h"
#include "vector.h"

void dist_based_get_absolute_error(matrix_t value_arr[],
                                   matrix_t approx_value_arr[],
                                   matrix_t absolute_error_arr[],
                                   uint8_t length)
{
    uint8_t i;

    if ((value_arr != NULL) && (approx_value_arr != NULL)
        && (absolute_error_arr != NULL)) {
        for (i = 0; i < length; i++) {
            absolute_error_arr[i] = fabs(
                value_arr[i] - approx_value_arr[i]);
        }
    }
}

matrix_t dist_based_get_distance_to_anchor(matrix_t ref_point[3],
                                           matrix_t point[3])
{
    matrix_t dist = 0.0;
    matrix_t diff_vec[3];

    vector_sub(3, ref_point, point, diff_vec);
    dist = vector_get_norm2(3, diff_vec);

    return dist;
}
