/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Jacobian function of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <math.h>

#include "matrix.h"
#include "vector.h"
#include "dist_based_fi.h"

void dist_based_jacobian_get_JTf(uint8_t ref_points_num,
                                 matrix_t ref_point_matrix[ref_points_num][3],
                                 matrix_t point[3],
                                 matrix_t dist_vec[ref_points_num],
                                 vector_t JTf[3])
{
    uint8_t i;
    matrix_t f_i = 0.0;
    matrix_t denom = 0.0;

    vector_clear(3, JTf);
    for (i = 0; i < ref_points_num; i++) {
        f_i =
            dist_based_fi(point,
                          (matrix_t *)&ref_point_matrix[i],
                          dist_vec[i]);
        denom = f_i + dist_vec[i];
        if (denom != 0) {
            JTf[0] += (point[0] - ref_point_matrix[i][0]) * f_i
                      / denom;
            JTf[1] += (point[1] - ref_point_matrix[i][1]) * f_i
                      / denom;
            JTf[2] += (point[2] - ref_point_matrix[i][2]) * f_i
                      / denom;
        }
    }
}

void dist_based_jacobian_get_JTJ(uint8_t ref_points_num,
                                 matrix_t ref_point_matrix[ref_points_num][3],
                                 matrix_t point[3],
                                 matrix_t dist_vec[ref_points_num],
                                 matrix_t JTJ[3][3])
{
    uint8_t i;
    matrix_t denom = 0.0;
    matrix_t f_i = 0.0;

    matrix_clear(3, 3, JTJ);
    for (i = 0; i < ref_points_num; i++) {
        f_i =
            dist_based_fi(point,
                          (matrix_t *)&ref_point_matrix[i],
                          dist_vec[i]) + dist_vec[i];
        denom = f_i * f_i;
        if (denom != 0) {
            JTJ[0][0] += (point[0] - ref_point_matrix[i][0]) *
                         (point[0] - ref_point_matrix[i][0])
                         / denom;
            JTJ[1][1] += (point[1] - ref_point_matrix[i][1]) *
                         (point[1] - ref_point_matrix[i][1])
                         / denom;
            JTJ[2][2] += (point[2] - ref_point_matrix[i][2]) *
                         (point[2] - ref_point_matrix[i][2])
                         / denom;
            JTJ[0][1] += (point[0] - ref_point_matrix[i][0]) *
                         (point[1] - ref_point_matrix[i][1])
                         / denom;
            JTJ[0][2] += (point[0] - ref_point_matrix[i][0]) *
                         (point[2] - ref_point_matrix[i][2])
                         / denom;
            JTJ[1][2] += (point[1] - ref_point_matrix[i][1]) *
                         (point[2] - ref_point_matrix[i][2])
                         / denom;
        }
    }

    JTJ[1][0] = JTJ[0][1];
    JTJ[2][0] = JTJ[0][2];
    JTJ[2][1] = JTJ[1][2];
}

void dist_based_jacobian_get_J(uint8_t ref_points_num, matrix_t point[3],
                               matrix_t ref_point_matrix[ref_points_num][3],
                               matrix_t J[ref_points_num][3])
{
    uint8_t i;
    matrix_t denom;

    for (i = 0; i < ref_points_num; i++) {
        denom =
            sqrt(
                pow(
                    point[0]
                    - ref_point_matrix[i][0],
                    2)
                +
                pow(
                    point[1]
                    - ref_point_matrix[i][1],
                    2)
                + pow(
                    point[2]
                    - ref_point_matrix[i][2],
                    2));
        if (denom != 0) {
            J[i][0] = (point[0] - ref_point_matrix[i][0]) / denom;
            J[i][1] = (point[1] - ref_point_matrix[i][1]) / denom;
            J[i][2] = (point[2] - ref_point_matrix[i][2]) / denom;
        }
    }
}

//compute: J(x)*s
void dist_based_jacobian_get_J_mul_s(uint8_t ref_points_num,
                                     matrix_t ref_point_matrix[ref_points_num][3],
                                     matrix_t point[3], matrix_t s[3],
                                     matrix_t J_s[ref_points_num])
{
    uint8_t i;
    matrix_t denom;
    matrix_t j_i_0 = 0;
    matrix_t j_i_1 = 0;
    matrix_t j_i_2 = 0;

    for (i = 0; i < ref_points_num; i++) {
        denom =
            sqrt(
                pow(
                    point[0]
                    - ref_point_matrix[i][0],
                    2)
                +
                pow(
                    point[1]
                    - ref_point_matrix[i][1],
                    2)
                + pow(
                    point[2]
                    - ref_point_matrix[i][2],
                    2));
        if (denom != 0) {
            j_i_0 = (point[0] - ref_point_matrix[i][0]) / denom;
            j_i_1 = (point[1] - ref_point_matrix[i][1]) / denom;
            j_i_2 = (point[2] - ref_point_matrix[i][2]) / denom;
        }
        J_s[i] = j_i_0 * s[0] + j_i_1 * s[1] + j_i_2 * s[2];
    }
}
