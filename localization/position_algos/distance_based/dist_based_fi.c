/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Error function of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <math.h>

#include "matrix.h"

matrix_t dist_based_fi(matrix_t point[3], matrix_t ref_point[3], matrix_t ri)
{

    matrix_t f_i =
        sqrt(
            (point[0] - ref_point[0])
            * (point[0]
               - ref_point[0])
            +
            (point[1] - ref_point[1])
            * (point[1]
               - ref_point[1])
            +
            (point[2] - ref_point[2])
            * (point[2]
               - ref_point[2])
            ) - ri;

    return f_i;
}

void dist_based_f_i(uint8_t ref_points_num,
                    matrix_t ref_point_mat[ref_points_num][3],
                    matrix_t point[3],
                    matrix_t d_vec[], matrix_t f_vec[])
{
    uint8_t i;

    for (i = 0; i < ref_points_num; i++) {
        f_vec[i] =
            sqrt(
                (point[0] - ref_point_mat[i][0])
                * (point[0]
                   - ref_point_mat[i][0])
                +
                (point[1]
                 - ref_point_mat[i][1])
                * (point[1]
                   - ref_point_mat[i][1])
                +
                (point[2]
                 - ref_point_mat[i][2])
                * (point[2]
                   - ref_point_mat[i][2])
                )
            - d_vec[i];
    }
}
