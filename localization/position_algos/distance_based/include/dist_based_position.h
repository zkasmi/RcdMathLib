/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     distance_based
 * @{
 *
 * @file
 * @brief       Functions of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef DIST_BASED_POSITION_H_
#define DIST_BASED_POSITION_H_

#include <math.h>
#include <inttypes.h>

#include "matrix.h"

/**
 * @brief   Computes the absolute error of a position of a distance-based localization system.
 *
 *
 * @param[in] value_arr[]                 true position.
 * @param[in] approx_value_arr[]          approximate position of the mobile device.
 * @param[in, out] absolute_error_arr[]   includes the absolute error.
 * @param[in] length                      arrays length.
 *
 */
void dist_based_get_absolute_error(matrix_t value_arr[],
                                   matrix_t approx_value_arr[],
                                   matrix_t absolute_error_arr[],
                                   uint8_t length);

/**
 * @brief   Computes the distance between a mobile station and a reference station
 *          of a distance-based localization system.
 *
 *
 * @param[in] ref_point   three-dimensional coordinates of the reference stations.
 * @param[in] point       three-dimensional coordinates of the mobile device.
 *
 * @return    the distance between the mobile station and the reference station.
 */
matrix_t dist_based_get_distance_to_anchor(matrix_t ref_point[3],
                                           matrix_t point[3]);

#endif /* DIST_BASED_POSITION_H_ */
