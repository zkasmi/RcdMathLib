/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     distance_based
 * @{
 *
 * @file
 * @brief       Error function of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef DIST_BASED_FI_H_
#define DIST_BASED_FI_H_

#include "matrix.h"

/**
 * @brief   Defines the error function of a distance-based localization system.
 * @details This error function is related to one reference station.
 *
 * @param[in] point[]           three-dimensional coordinates of the mobile device.
 * @param[in] ref_point[]       three-dimensional coordinates of a reference station.
 * @param[in] ri                distance to a reference station.
 *
 * @return    the error related to a reference station and destined position.
 *
 */
matrix_t dist_based_fi(matrix_t point[3], matrix_t ref_point[3], matrix_t ri);

/**
 * @brief   Defines the error function of a distance-based localization system.
 * @details This error function is related to multiple reference stations.
 *
 * @param[in] ref_points_num      Number of the reference stations.
 * @param[in] ref_point_mat[][]   three-dimensional coordinates of the reference stations.
 * @param[in] point[]             three-dimensional coordinates of the mobile device.
 * @param[in] d_vec[]             distances to the reference stations.
 * @param[out] f_vec[]            errors related to reference stations and destined position.
 *
 */
void dist_based_f_i(uint8_t ref_points_num,
                    matrix_t ref_point_mat[ref_points_num][3],
                    matrix_t point[3],
                    matrix_t d_vec[], matrix_t f_vec[]);

#endif /* DIST_BASED_FI_H_ */
