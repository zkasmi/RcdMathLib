/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     distance_based
 * @{
 *
 * @file
 * @brief       Jacobian function of distance-based localization systems.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef DIST_BASED_JACOBIAN_H_
#define DIST_BASED_JACOBIAN_H_

#include "matrix.h"
#include "vector.h"

/**
 * @brief   Defines \f$ J_f^{T} \vec{f} \f$ of distance-based localization system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] dist_vec[]            distances to the reference stations.
 * @param[in, out] JTf[]            includes the \f$ J_f^{T} \vec{f} \f$ vector.
 *
 */
void dist_based_jacobian_get_JTf(uint8_t ref_points_num,
                                 matrix_t ref_point_matrix[ref_points_num][3],
                                 matrix_t point[3],
                                 matrix_t dist_vec[ref_points_num],
                                 vector_t JTf[3]);

/**
 * @brief   Defines \f$ J_f^{T} J_{f} \f$ of distance-based localization
 *          system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] dist_vec[][]          distances to the reference stations.
 * @param[out] JTJ[][]              includes the \f$ J_f^{T} J_{f} \f$ matrix.
 *
 */
void dist_based_jacobian_get_JTJ(uint8_t ref_points_num,
                                 matrix_t ref_point_matrix[ref_points_num][3],
                                 matrix_t point[3],
                                 matrix_t dist_vec[ref_points_num],
                                 matrix_t JTJ[3][3]);

/**
 * @brief   Computes \f$ J_f^{T} \vec{s} \f$ of distance-based localization system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] s[]                   correction vector.
 * @param[out] J_s[]                includes the \f$ J_f^{T} \vec{s} \f$ vector.
 *
 */
void dist_based_jacobian_get_J_mul_s(uint8_t ref_points_num,
                                     matrix_t ref_point_matrix[ref_points_num][3], matrix_t point[3],
                                     matrix_t s[3], matrix_t J_s[ref_points_num]);

/**
 * @brief   Computes the Jacobian matrix of distance-based localization system.
 *
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[out] J[][]                includes the Jacobian Matrix.
 *
 */
void dist_based_jacobian_get_J(uint8_t ref_points_num, matrix_t point[3],
                               matrix_t ref_point_matrix[ref_points_num][3],
                               matrix_t J[ref_points_num][3]);

#endif /* DIST_BASED_JACOBIAN_H_ */
