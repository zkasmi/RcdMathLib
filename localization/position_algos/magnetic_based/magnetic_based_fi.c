/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Error function of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#include <math.h>
#include <inttypes.h>
#include <magnetic_based_position.h>

#include "matrix.h"

void magnetic_based_f_i(uint8_t ref_points_num,
                        matrix_t ref_points_matrix[][3], matrix_t point[],
                        matrix_t Bi_vec[], matrix_t f_vec[])
{

    for (int i = 0; i < ref_points_num; i++) {

        matrix_t X_Xi_sq = pow(point[0] - ref_points_matrix[i][0], 2);
        matrix_t Y_Yi_sq = pow(point[1] - ref_points_matrix[i][1], 2);
        matrix_t Z_Zi_sq = pow(point[2] - ref_points_matrix[i][2], 2);

        f_vec[i] = (K_T * (sqrt(X_Xi_sq + Y_Yi_sq + 4.0 * Z_Zi_sq)) /
                    pow((X_Xi_sq + Y_Yi_sq + Z_Zi_sq), 2))
                   - Bi_vec[i];
    }
}
