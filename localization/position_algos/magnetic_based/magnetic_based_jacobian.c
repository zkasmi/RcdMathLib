/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Jacobian function of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "magnetic_based_fi.h"
#include "magnetic_based_position.h"
#include "matrix.h"

void magnetic_based_jacobian_get_J(uint8_t ref_point_num,
                                   matrix_t ref_point_matrix[ref_point_num][3],
                                   matrix_t point[],
                                   matrix_t J[ref_point_num][3])
{

    for (int i = 0; i < ref_point_num; i++) {

        matrix_t X_Xi = point[0] - ref_point_matrix[i][0];
        matrix_t Y_Yi = point[1] - ref_point_matrix[i][1];
        matrix_t Z_Zi = point[2] - ref_point_matrix[i][2];

        matrix_t X_Xi_sq = pow(X_Xi, 2);
        matrix_t Y_Yi_sq = pow(Y_Yi, 2);
        matrix_t Z_Zi_sq = pow(Z_Zi, 2);

        J[i][0] =
            K_T * (X_Xi)
            * ((-3.0 * X_Xi_sq
                - 3.0 * Y_Yi_sq
                - 15.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));

        J[i][1] =
            K_T * (Y_Yi)
            * ((-3.0 * X_Xi_sq
                - 3.0 * Y_Yi_sq
                - 15.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));

        J[i][2] =
            4.0 * K_T * (Z_Zi)
            * ((-3.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));
    }
}

void magnetic_based_jacobian_get_JTJ(uint8_t ref_points_num,
                                     matrix_t ref_point_matrix[ref_points_num][3],
                                     matrix_t point[3], matrix_t *unused, matrix_t JTJ[3][3])
{
    matrix_t J[ref_points_num][3];

    magnetic_based_jacobian_get_J(ref_points_num, ref_point_matrix, point,
                                  J);
    matrix_trans_mul_itself(ref_points_num, 3, J, JTJ);

    (void)unused;
}

void magnetic_based_jacobian_get_JTf(uint8_t ref_points_num,
                                     matrix_t ref_points_matrix[ref_points_num][3],
                                     matrix_t point[3], matrix_t Bi_vec[ref_points_num],
                                     matrix_t JTf[3])
{

    matrix_t J[ref_points_num][3];
    matrix_t f_vec[ref_points_num];

    magnetic_based_f_i(ref_points_num, ref_points_matrix, point, Bi_vec,
                       f_vec);

    magnetic_based_jacobian_get_J(ref_points_num, ref_points_matrix, point,
                                  J);
    matrix_trans_mul_vec(ref_points_num, 3, J, ref_points_num, f_vec, JTf);
}

//compute: J(x)*s
void magnetic_based_jacobian_get_J_mul_s(uint8_t ref_points_num,
                                         matrix_t ref_point_matrix[ref_points_num][3],
                                         matrix_t point[3],
                                         matrix_t s[3],
                                         matrix_t J_s[ref_points_num])
{
    uint8_t i;
    matrix_t X_Xi, Y_Yi, Z_Zi;
    matrix_t X_Xi_sq, Y_Yi_sq, Z_Zi_sq;
    matrix_t j_i_0, j_i_1, j_i_2;

    for (i = 0; i < ref_points_num; i++) {
        X_Xi = point[0] - ref_point_matrix[i][0];
        Y_Yi = point[1] - ref_point_matrix[i][1];
        Z_Zi = point[2] - ref_point_matrix[i][2];

        X_Xi_sq = pow(X_Xi, 2);
        Y_Yi_sq = pow(Y_Yi, 2);
        Z_Zi_sq = pow(Z_Zi, 2);

        j_i_0 =
            K_T * (X_Xi)
            * ((-3.0 * X_Xi_sq
                - 3.0 * Y_Yi_sq
                - 15.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));
        j_i_1 =
            K_T * (Y_Yi)
            * ((-3.0 * X_Xi_sq
                - 3.0 * Y_Yi_sq
                - 15.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));
        j_i_2 =
            4.0 * K_T * (Z_Zi)
            * ((-3.0 * Z_Zi_sq)
               /
               (sqrt(
                    X_Xi_sq
                    + Y_Yi_sq
                    + 4.0
                    * Z_Zi_sq)
                * pow(
                    (X_Xi_sq
                     + Y_Yi_sq
                     + Z_Zi_sq),
                    3)));

        J_s[i] = j_i_0 * s[0] + j_i_1 * s[1] + j_i_2 * s[2];
    }
}
