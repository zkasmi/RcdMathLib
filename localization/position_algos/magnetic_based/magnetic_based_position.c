/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Functions of of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <math.h>
#include <complex.h>
#include <magnetic_based_position.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "matrix.h"
#include "vector.h"
#include "trilateration.h"

void magnetic_based_get_absolute_error(matrix_t value_arr[],
                                       matrix_t approx_value_arr[],
                                       matrix_t absolute_error_arr[],
                                       uint8_t length)
{
    uint8_t i;

    if ((value_arr != NULL) && (approx_value_arr != NULL)
        && (absolute_error_arr != NULL)) {
        for (i = 0; i < length; i++) {
            absolute_error_arr[i] = fabs(
                value_arr[i] - approx_value_arr[i]);
        }
    }
}

matrix_t magnetic_based_get_distances_to_anchors(matrix_t ref_point[3],
                                                 matrix_t point[3])
{
    matrix_t dist = 0.0;
    matrix_t diff_vec[3];

    vector_sub(3, ref_point, point, diff_vec);
    dist = vector_get_norm2(3, diff_vec);

    return dist;
}

matrix_t magnetic_based_get_magnetic_field(matrix_t ref_point[3],
                                           matrix_t target_point[3], matrix_t k)
{
    matrix_t magn_field = 0.0;
    matrix_t r_square = 0.0;
    uint8_t i;

    printf("ref_arr = ");
    vector_flex_print(3, ref_point, 5, 4);
    puts("");
    for (i = 0; i < 3; i++) {
        r_square += pow(target_point[i] - ref_point[i], 2);
    }

    printf("r_sq = %f\n", r_square);
    magn_field =
        k
        * sqrt(
            3
            * pow(
                target_point[2]
                - ref_point[2],
                2)
            + r_square)
        / pow(r_square, 2);

    return magn_field;
}

void magnetic_based_get_magnetic_field_vec(uint8_t m,
                                           matrix_t ref_point_matrix[m][3],
                                           matrix_t target_point[],
                                           matrix_t k,
                                           matrix_t magn_field_vec[])
{

    matrix_t r_square = 0.0;
    uint8_t i, j;

    for (i = 0; i < m; i++) {
        for (j = 0; i < 3; j++) {
            r_square +=
                pow(
                    target_point[i]
                    - ref_point_matrix[i][j],
                    2);
        }

        magn_field_vec[i] =
            k
            * sqrt(
                3
                * pow(
                    target_point[2]
                    - ref_point_matrix[i][2],
                    2)
                + r_square)
            / pow(r_square, 2);
        r_square = 0.0;
    }
}


void magnetic_based_get_distances(matrix_t magnetic_field_strength_arr[],
                                  matrix_t angular_theta_arr[],
                                  matrix_t distance_arr[], uint8_t length,
                                  matrix_t k)
{
    int i;

    for (i = 0; i < length; i++) {
        if (magnetic_field_strength_arr[i] == 0) {
            distance_arr[i] = MILPS_MAX_DIST;
        }
        else {
            if (angular_theta_arr[i] == 0) {
                distance_arr[i] =
                    pow(
                        k
                        / magnetic_field_strength_arr[i],
                        (matrix_t)1
                        / 3);
            }
            else {

                distance_arr[i] =
                    pow(
                        k
                        * sqrt(
                            1
                            + 3
                            * pow(
                                sin(
                                    angular_theta_arr[i]),
                                2))
                        / magnetic_field_strength_arr[i],
                        (matrix_t)1
                        / 3);
            }
        }
    }
}


matrix_t magnetic_based_get_r(matrix_t B, matrix_t theta, matrix_t k)
{
    matrix_t r = 0;

    if (B == 0) {
        return MILPS_MAX_DIST;
    }
    else {
        if (theta == 0) {
            r = pow(k / B, (matrix_t)1 / 3);
        }
        else {

            r = pow(k * sqrt(1 + 3 * pow(sin(theta), 2)) / B,
                    (matrix_t)1 / 3);
        }
    }

    return r;
}

// PC proprocessing
void magnetic_based_preprocessing_get_position(uint8_t anchor_num,
                                               matrix_t anchor_pos_matrix[anchor_num][3],
                                               matrix_t pseudo_inv_matrix[4][anchor_num],
                                               matrix_t homog_sol_arr[],
                                               matrix_t solution_x1[],
                                               matrix_t solution_x2[])
{

    matrix_t dist_arr[anchor_num];

    trilateration1(anchor_num, anchor_pos_matrix, pseudo_inv_matrix,
                   homog_sol_arr, dist_arr, solution_x1, solution_x2);

}
