/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     magnetic_based
 * @{
 *
 * @file
 * @brief       Error function of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#ifndef MAGNETIC_BASED_F_H_
#define MAGNETIC_BASED_F_H_
#include <math.h>
#include <string.h>

#include "matrix.h"

/**
 * @brief   Defines the error function of a magnetic-based localization system.
 * @details This error function is related to multiple reference stations.
 *
 * @param[in] ref_points_num         number of the reference stations.
 * @param[in] ref_points_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]                three-dimensional coordinates of the mobile device.
 * @param[in] Bi_vec[]               the measured magnetic field strength.
 * @param[in, out] f_vec[]           errors related to reference stations and destined position.
 *
 */
void magnetic_based_f_i(uint8_t ref_points_num, matrix_t ref_points_matrix[][3],
                        matrix_t point[], matrix_t Bi_vec[], matrix_t f_vec[]);

#endif /* MAGNETIC_BASED_F_H_ */
