/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     magnetic_based
 * @{
 *
 * @file
 * @brief       Jacobian function of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#ifndef MAGNETIC_BASED_JACOBIAN_H_
#define MAGNETIC_BASED_JACOBIAN_H_

#include "matrix.h"

/**
 * @brief   Computes the Jacobian matrix of magnetic-based localization system.
 *
 *
 * @param[in] ref_point_num         number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in, out] J[][]            includes the Jacobian Matrix.
 *
 */
void magnetic_based_jacobian_get_J(uint8_t ref_point_num,
                                   matrix_t ref_point_matrix[ref_point_num][3],
                                   matrix_t point[],
                                   matrix_t J[ref_point_num][3]);

/**
 * @brief   Defines \f$ J_f^{T} J_{f} \f$ of magnetic-based localization
 *          system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] unused                this variable can be set to NULL. It is introduced to guarantee
 *                                  the compatibility with the @ref dist_based_jacobian_get_JTJ,
 *                                  @ref modified_gauss_newton, and the @ref opt_levenberg_marquardt
 *                                  functions.
 * @param[in, out] JTJ[][]          includes the \f$ J_f^{T} J_{f} \f$ matrix.
 *
 */
void magnetic_based_jacobian_get_JTJ(uint8_t ref_points_num,
                                     matrix_t ref_point_matrix[ref_points_num][3],
                                     matrix_t point[3], matrix_t *unused, matrix_t JTJ[3][3]);
/**
 * @brief   Defines \f$ J_f^{T} \vec{f} \f$ of magnetic-based localization system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num         number of the reference stations.
 * @param[in] ref_points_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]                three-dimensional coordinates of the mobile device.
 * @param[in] Bi_vec[]               magnetic field strengths of the coils (reference stations).
 * @param[in, out] JTf[]             includes the \f$ J_f^{T} \vec{f} \f$ vector.
 *
 */
void magnetic_based_jacobian_get_JTf(uint8_t ref_points_num,
                                     matrix_t ref_points_matrix[ref_points_num][3],
                                     matrix_t point[3], matrix_t Bi_vec[ref_points_num],
                                     matrix_t JTf[3]);

/**
 * @brief   Computes \f$ J_f^{T} \vec{s} \f$ of magnetic-based localization system.
 * @details Where \f$ J_f \f$ is the Jacobian matrix. This function is a part of
 *          derivatives to minimize the sum of square errors.
 *
 * @param[in] ref_points_num        number of the reference stations.
 * @param[in] ref_point_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] point[]               three-dimensional coordinates of the mobile device.
 * @param[in] s[]                   correction vector.
 * @param[in, out] J_s[]            includes the \f$ J_f^{T} \vec{s} \f$ vector.
 *
 */
void magnetic_based_jacobian_get_J_mul_s(uint8_t ref_points_num,
                                         matrix_t ref_point_matrix[ref_points_num][3],
                                         matrix_t point[3],
                                         matrix_t s[3], matrix_t J_s[ref_points_num]);

#endif /* MAGNETIC_BASED_JACOBIAN_H_ */
