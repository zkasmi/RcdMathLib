/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     magnetic_based
 * @{
 *
 * @file
 * @brief       Functions of of DC-pulsed, magnetic localization system.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef MAGNETIC_BASED_POSITION_H_
#define MAGNETIC_BASED_POSITION_H_

#include <inttypes.h>
#include <math.h>

#include "matrix.h"

/**
 * The current running through the coil.
 */
#define I0         15

/**
 * The number of turns of the wire.
 */
#define NW         140

/**
 * The radius of the coil.
 */
#define R0         0.25

/**
 * The pi constant.
 */
#define PI         3.14159265358979323846

/**
 * The base area of the coil.
 */
#define AR         PI * R0 * R0     //Loop area

/**
 * The permeability of free space,.
 */
#define MU0        4 * PI * 1E-7    //TGm/A

/**
 * The K constant.
 */
#define K          MU0 * NW * I0 * AR / (4 * PI)

/**
 * From Mega Gauss to Tesla.
 */
#define MG_TO_T    1E7

/**
 * The number of turns of the wire.
 */
#define K_T        K * MG_TO_T

/**
 * The number of turns of the wire.
 */
#define MILPS_MAX_DIST     25

/**
 * @brief   Computes the absolute error of a position of magnet-based localization system.
 *
 *
 * @param[in] value_arr                 true position.
 * @param[in] approx_value_arr          approximate position of the mobile device.
 * @param[in, out] absolute_error_arr   includes the absolute error.
 * @param[in] length                    arrays length.
 *
 */
void magnetic_based_get_absolute_error(matrix_t value_arr[],
                                       matrix_t approx_value_arr[],
                                       matrix_t absolute_error_arr[],
                                       uint8_t length);

/**
 * @brief   Computes the distances between a mobile station and the reference stations
 *          of a magnet-based localization system.
 *
 *
 * @param[in] magnetic_field_strength_arr[]   includes the magnetic field strengths.
 * @param[in] angular_theta_arr[]             elevation angles.
 * @param[in, out] distance_arr[]             distance array.
 * @param[in] length                          length of the arrays.
 * @param[in] k                               magnetic constant.
 *
 */
void magnetic_based_get_distances(matrix_t magnetic_field_strength_arr[],
                                  matrix_t angular_theta_arr[],
                                  matrix_t distance_arr[], uint8_t length,
                                  matrix_t k);

/**
 * @brief   Computes the distance between a mobile station and a reference stations
 *          of a magnet-based localization system.
 *
 *
 * @param[in] B       magnetic field strength.
 * @param[in] theta   elevation angle.
 * @param[in] k       magnetic constant.
 *
 * @return    the distance between the mobile station and the reference station.
 */
matrix_t magnetic_based_get_r(matrix_t B, matrix_t theta, matrix_t k);

/**
 * @brief   Computes the distance between a mobile station and a reference station
 *          of magnetic-based localization system.
 *
 *
 * @param[in] ref_point[]   three-dimensional coordinates of the reference stations.
 * @param[in] point[]       three-dimensional coordinates of the mobile device.
 *
 * @return    the distance between the mobile station and the reference station.
 */
matrix_t magnetic_based_get_distances_to_anchors(matrix_t ref_point[3],
                                                 matrix_t point[3]);
/**
 * @brief   Computes the magnetic field strength from a mobile station to a reference station.
 *
 *
 * @param[in] ref_point[]     three-dimensional coordinates of a reference station.
 * @param[in] target_point[]  three-dimensional coordinates of the mobile device.
 * @param[in] k               magnetic constant.
 *
 * @return    the magnetic field strength.
 */
matrix_t magnetic_based_get_magnetic_field(matrix_t ref_point[3],
                                           matrix_t target_point[3],
                                           matrix_t k);
/**
 * @brief   Computes the magnetic field strengths from a mobile station to various reference
 *          stations.
 *
 * @param[in] m                         number of the reference stations.
 * @param[in] ref_point_matrix[][]      three-dimensional coordinates of the reference stations.
 * @param[in] target_point[]            three-dimensional coordinates of the mobile device.
 * @param[in] k                         magnetic constant.
 * @param[in, out] magn_field_vec[]     includes the magnetic field strengths.
 *
 */
void magnetic_based_get_magnetic_field_vec(uint8_t m,
                                           matrix_t ref_point_matrix[m][3],
                                           matrix_t target_point[],
                                           matrix_t k,
                                           matrix_t magn_field_vec[]);

/**
 * @brief     Computes the position of a mobile station by a magnetic-based localization system.
 * @details   The position is computed based on a pre-processed pseudo-inverse matrix and the
 *            homogeneous solution in the case of three reference stations.
 *
 * @param[in] anchor_num                        number of the reference stations.
 * @param[in] anchor_pos_matrix[][]             coordinates of the reference stations.
 * @param[in] pseudo_inv_matrix[][[]            pointer to the pre-processed pseudo-inverse matrix.
 * @param[in] homog_sol_arr[]                   pointer to the homogeneous solution.
 * @param[in, out] solution_x1[]                  includes the first solution.
 * @param[in, out] solution_x2[]                  includes the second solution.
 *
 */
void magnetic_based_preprocessing_get_position(uint8_t anchor_num,
                                               matrix_t anchor_pos_matrix[anchor_num][3],
                                               matrix_t pseudo_inv_matrix[4][anchor_num],
                                               matrix_t homog_sol_arr[],
                                               matrix_t solution_x1[],
                                               matrix_t solution_x2[]);

#endif /* MAGNETIC_BASED_POSITION_H_ */
