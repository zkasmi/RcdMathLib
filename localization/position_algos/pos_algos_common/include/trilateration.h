/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     pos_algos_common
 * @{
 *
 * @file
 * @brief       Implement the trilateration algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef TRILATERATION_H_
#define TRILATERATION_H_

#include <inttypes.h>

#include "matrix.h"

/**
 * @brief   Implement the trilateration algorithm using the pre-processed pseudo-inverse matrix.
 * @note    This function should be initialized with the pre-processed pseudo-inverse matrix of
 *          the equation system: \f$ A \vec{x} = 0 \f$.
 *          Caution!: The \p solution_x1 and the \p solution_x2 vectors have a length of 3 or 4 in
 *          the case of two-dimensional or three-dimensional space. The first element \f$(x_0)\f$
 *          of the \p solution_x1[] and the \p solution_x2[] vectors is a measure of the solvability
 *          of the multilateration problem. For example, in the three-dimensional space:
 *          \f$ d =  x_0 - \left( x_1^{2} + x_2^{2} + x_3^{2} \right)\f$.
 *
 *
 * @param[in] anchor_num              number of the reference stations.
 * @param[in] anchor_pos_matrix[][]   three-dimensional coordinates of the reference stations.
 * @param[in] pseudo_inv_matrix[][]   pointer to the pre-processed pseudo-inverse matrix.
 * @param[in] homog_sol_arr[]         the homogeneous solution.
 * @param[in] dist_arr[]              distances to the reference stations.
 * @param[in, out] solution_x1[]      includes the first solution. It has the length of 3 or 4 in
 *                                    the case of two-dimensional or three-dimensional space.
 * @param[in, out] solution_x2[]      includes the second solution. It has length of 3 or 4 in the
 *                                    case of two-dimensional or three-dimensional space.
 *
 */
void trilateration1(uint8_t anchor_num,
                    matrix_t anchor_pos_matrix[anchor_num][3],
                    matrix_t pseudo_inv_matrix[4][anchor_num],
                    matrix_t homog_sol_arr[],
                    matrix_t dist_arr[],
                    matrix_t solution_x1[],
                    matrix_t solution_x2[]
                    );

/**
 * @brief   Implement the trilateration algorithm.
 * @note    The pre-processed pseudo-inverse matrix of the equation system:
 *          \f$ A \vec{x} = 0 \f$ is computed on the mobile station.
 *          Caution!: The \p solution_x1 and the \p solution_x2 vectors have a length of 3 or 4 in
 *          the case of two-dimensional or three-dimensional space. The first element \f$(x_0)\f$
 *          of the \p solution_x1[] and the \p solution_x2[] vectors is a measure of the solvability
 *          of the multilateration problem. For example, in the three-dimensional space:
 *          \f$ d =  x_0 - \left( x_1^{2} + x_2^{2} + x_3^{2} \right)\f$.
 *
 * @param[in] anchor_num             number of the reference stations.
 * @param[in] anchor_pos_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] dist_arr[]             distances to the reference stations.
 * @param[in, out] solution_x1[]     includes the first solution. It has the length of 3 or 4 in
 *                                   the case of two-dimensional or three-dimensional space.
 * @param[in, out] solution_x2[]     includes the second solution. It has the length of 3 or 4 in
 *                                   the case of two-dimensional or three-dimensional space.
 *
 */
void trilateration2(uint8_t anchor_num,
                    matrix_t anchor_pos_matrix[anchor_num][3],
                    matrix_t dist_arr[],
                    matrix_t solution_x1[],
                    matrix_t solution_x2[]);

/**
 * @brief   Computes the matrix \f$ A \f$ of the equation system:
 *          \f$ A \vec{x} = \vec{b}\f$.
 *
 * @param[in] m                         number of the reference stations.
 * @param[in] anchor_pos_matrix[][]     three-dimensional coordinates of the reference stations.
 * @param[in, out] A_matrix[][]         pointer to the matrix \f$ A \f$.
 *
 */
void trilateration_get_A_matrix(uint8_t m, matrix_t anchor_pos_matrix[m][3],
                                matrix_t A_matrix[m][4]);
/**
 * @brief   Computes the vector \f$ \vec{b} \f$ of the equation system:
 *          \f$ A \vec{x} = \vec{b}\f$.
 *
 * @param[in] anchor_num                number of the reference stations.
 * @param[in] dist_arr[]                distances to the reference stations.
 * @param[in] anchor_pos_matrix[][]     three-dimensional coordinates of the reference stations.
 * @param[in, out] b_vec[]              pointer to the vector \f$ vec{b} \f$.
 *
 */
void trilateration_get_b_vector(uint8_t anchor_num, matrix_t dist_arr[],
                                matrix_t anchor_pos_matrix[anchor_num][3],
                                matrix_t b_vec[]);

/**
 * @brief   Compute the particular solution, which is the general solution of
 *          \f$ A \vec{x_0} = \vec{b_0}\f$.
 * @note    The particular solution is calculated in the case of three reference stations.
 *          The particular solution is computed using the pre-processed pseudo-inverse matrix.
 *
 * @param[in] pseudo_inv_matrix[][]        pointer to the pre-processed pseudo-inverse matrix.
 * @param[in] b_arr[]                      pointer the vector \f$ vec{b} \f$.
 * @param[in, out] particular_solu_arr[]   includes the particular solution.
 *
 */
void trilateration_preprocessed_get_particular_solution(
    matrix_t pseudo_inv_matrix[4][3],
    matrix_t b_arr[],
    matrix_t particular_solu_arr[]);

/**
 * @brief   Compute the particular solution, which is the general solution of
 *          \f$ A \vec{x_0} = \vec{b_0} \f$.
 * @note    The particular solution is calculated in the case of three reference stations.
 *          The particular solution is computed on the mobile station.
 *
 * @param[in] m                    number of the reference stations.
 * @param[in] n                    column number of the reference stations matrix.
 * @param[in] anchor_pos_mat[][]   three-dimensional coordinates of the reference stations.
 * @param[in] dist_arr[]           distances to the reference stations.
 * @param[in, out] Xp[]            includes the particular solution.
 */
void trilateration_get_particular_solution(uint8_t m, uint8_t n,
                                           matrix_t anchor_pos_mat[m][n],
                                           matrix_t dist_arr[], matrix_t Xp[]);

/**
 * @brief   Compute the rank and the solution of the homogeneous system
 *          \f$ A \vec{x_0} = 0 \f$.
 *
 * @param[in] anchor_num              number of the reference stations.
 * @param[in] anchor_pos_matrix[][]   three-dimensional coordinates of the reference stations.
 * @param[in, out] Xh[]               includes the homogeneous solution.
 *
 * @return  the rank of the matrix \f$ A \f$.
 */
uint8_t trilateration_get_rank_and_homogeneous_solution(uint8_t anchor_num,
                                                        matrix_t anchor_pos_matrix[anchor_num][3],
                                                        matrix_t Xh[]);
/**
 * @brief    Solve a quadratic equation.
 * @details  The quotients of the quadratic equation are derived from the particular and homogeneous
 *           solution.
 *
 * @param[in] particular_solu_arr[]        pointer to the particular solution.
 * @param[in] homogeneous_solution_arr[]   pointer to the homogeneous solution.
 * @param[in, out] solution_x1[]           presents the first solution.
 * @param[in, out] solution_x2[]           presents the second solution.
 *
 */
void trilateration_get_quadratic_equation_solution(
    matrix_t particular_solu_arr[],
    matrix_t homogeneous_solution_arr[],
    matrix_t solution_x1[],
    matrix_t solution_x2[]);

/**
 * @brief    Solve a linear equation.
 * @details  The linear equation is solved by using the pre-processed pseudo-inverse matrix and the
 *           vector \f$ \vec{b} \f$
 *
 * @param[in] line_num               row number of the pseudo-inverse matrix.
 * @param[in] col_num                column number of the pseudo-inverse matrix.
 * @param[in] pseudo_inv_matrix[][]  pointer to the pre-processed pseudo-inverse matrix.
 * @param[in] b_vec[]                pointer to the vector \f$ \vec{b} \f$.
 * @param[out] sol_vec[]             solution vector.
 *
 */
void trilateration_solve_linear_equation(uint8_t line_num, uint8_t col_num,
                                         matrix_t pseudo_inv_matrix[line_num][col_num],
                                         matrix_t b_vec[], matrix_t sol_vec[]);

#endif /* TRILATERATION_H_ */
