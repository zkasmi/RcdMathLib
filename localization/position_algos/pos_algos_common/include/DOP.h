/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     pos_algos_common
 * @{
 *
 * @file
 * @brief       Compute the Position Dilution of Precision (PDOP).
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#ifndef DOP_H_
#define DOP_H_

#include <inttypes.h>

#include "matrix.h"

/**
 * @brief   Compute the Position Dilution of Precision (PDOP).
 *
 * @param[in] m                 number of the reference stations.
 * @param[in] ref_Matrix        three-dimensional coordinates of the reference stations.
 * @param[in] true_pos          three-dimensional coordinates of the mobile device.
 *
 *
 * @return    the PDOP-value.
 *
 */
matrix_t get_PDOP(uint8_t m, matrix_t ref_Matrix[m][3], matrix_t true_pos[m]);

#endif /* DOP_H_ */
