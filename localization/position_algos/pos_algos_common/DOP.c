/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Compute the Position Dilution of Precision (PDOP).
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#include <math.h>

#include "DOP.h"

#include "moore_penrose_pseudo_inverse.h"
#include "matrix.h"

matrix_t get_PDOP(uint8_t m, matrix_t ref_Matrix[m][3],
                  matrix_t true_pos[m])
{

    matrix_t PDOP = 0.0;
    matrix_t H[m][4];
    matrix_t H_transpose[4][m];
    matrix_t H_transpose_H[4][4];
    matrix_t H_transpose_H_pinv[4][4];

    for (int i = 0; i < m; i++) {
        matrix_t R_i = sqrt(pow(ref_Matrix[i][0] - true_pos[0], 2)
                            + pow(ref_Matrix[i][1] - true_pos[1], 2)
                            + pow(ref_Matrix[i][2] - true_pos[2], 2));

        for (int l = 0; l < 3; l++) {
            H[i][l] = (ref_Matrix[i][l] - true_pos[l]) / R_i;
        }
        H[i][3] = -1;
    }

    matrix_transpose(m, 4, H, H_transpose);
    matrix_mul(4, m, H_transpose, m, 4, H, H_transpose_H);
    moore_penrose_get_pinv(4, 4, H_transpose_H, H_transpose_H_pinv);

    for (int i = 0; i < 3; i++) {
        PDOP = PDOP + H_transpose_H_pinv[i][i];
    }
    PDOP = sqrt(PDOP);
    return PDOP;
}
