/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Implement the trilateration algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "moore_penrose_pseudo_inverse.h"
#include "matrix.h"
#include "svd.h"
#include "trilateration.h"

void trilateration1(uint8_t anchor_num,
                    matrix_t anchor_pos_matrix[anchor_num][3],
                    matrix_t pseudo_inv_matrix[4][anchor_num],
                    matrix_t homog_sol_arr[],
                    matrix_t dist_arr[],
                    matrix_t solution_x1[],
                    matrix_t solution_x2[]
                    )
{

    matrix_t b_arr[anchor_num];
    matrix_t particular_solution_arr[4];

    if (anchor_num == 3) {
        trilateration_get_b_vector(anchor_num, dist_arr,
                                   anchor_pos_matrix, b_arr);
        trilateration_preprocessed_get_particular_solution(
            pseudo_inv_matrix, b_arr,
            particular_solution_arr);
        trilateration_get_quadratic_equation_solution(
            particular_solution_arr,
            homog_sol_arr, solution_x1, solution_x2);

    }

    else if (anchor_num > 3) {
        trilateration_get_b_vector(anchor_num, dist_arr,
                                   anchor_pos_matrix, b_arr);

        //compute the solution
        trilateration_solve_linear_equation(anchor_num, anchor_num,
                                            pseudo_inv_matrix, b_arr,
                                            solution_x1);
    }
}

//On MCU Processing
void trilateration2(uint8_t anchor_num,
                    matrix_t anchor_pos_matrix[anchor_num][3],
                    matrix_t dist_arr[],
                    matrix_t solution_x1[],
                    matrix_t solution_x2[])
{

    matrix_t b_arr[anchor_num];
    matrix_t A_matrix[anchor_num][4];
    matrix_t pseudo_inv_matrix[4][anchor_num];
    uint8_t rank = 0;

    if (anchor_num == 3) {
        //2nd compute the particular solution
        matrix_t Xp[4];
        trilateration_get_particular_solution(anchor_num, anchor_num,
                                              anchor_pos_matrix, dist_arr, Xp);

        //3th compute the homogeneous solution
        matrix_t Xh[4];
        rank = trilateration_get_rank_and_homogeneous_solution(
            anchor_num, anchor_pos_matrix, Xh);

        //solve the quadratic equation
        if (rank == 3) {
            trilateration_get_quadratic_equation_solution(Xp, Xh,
                                                          solution_x1,
                                                          solution_x2);
        }
        else {
            puts("rank!=3, the equation system is not solvable.");
        }

    }

    else if (anchor_num > 3) {
        //compute A-matrix and b_vector
        trilateration_get_A_matrix(anchor_num, anchor_pos_matrix,
                                   A_matrix);
        trilateration_get_b_vector(anchor_num, dist_arr,
                                   anchor_pos_matrix, b_arr);

        //compute the pseudo inverse
        moore_penrose_get_pinv(anchor_num, 4, A_matrix,
                               pseudo_inv_matrix);
        //compute the solution
        trilateration_solve_linear_equation(4, anchor_num,
                                            pseudo_inv_matrix, b_arr,
                                            solution_x1);
    }
}

void trilateration_preprocessed_get_particular_solution(
    matrix_t pseudo_inv_matrix[4][3],
    matrix_t b_arr[],
    matrix_t particular_solu_arr[])
{
    int i = 0;
    int j = 0;

    memset(particular_solu_arr, 0.0, 4 * sizeof(matrix_t));

    for (; i < 4; i++) {
        for (; j < 3; j++) {
            particular_solu_arr[i] += pseudo_inv_matrix[i][j]
                                      * b_arr[j];
            //DEBUGF("pseudo_inv_matrix[%d][%d]  = %f\n b_arr[%d] = %f\n", i, j, (double) pseudo_inv_matrix[i][j], j, (double) b_arr[j]);
            //printf("pseudo_inv_matrix[%d][%d]  = %f\n b_arr[%d] = %f\n", i, j, (double) pseudo_inv_matrix[i][j], j, (double) b_arr[j]);
        }
        j = 0;
    }
#ifdef DEBUG_ENABLED
    i = 0;
    for (; i < 4; i++) {
        DEBUGF("particular_solution_arr[%d] = %f\n", i,
               (double)particular_solu_arr[i]);
    }
#endif
}

uint8_t trilateration_get_rank_and_homogeneous_solution(uint8_t anchor_num,
                                                        matrix_t anchor_pos_matrix[anchor_num][3],
                                                        matrix_t Xh[])
{
    uint8_t rank = 0;
    matrix_t A[anchor_num][4];
    uint8_t m = anchor_num;
    uint8_t n = 4;
    uint8_t s_length = svd_get_single_values_num(m, n);
    matrix_t s[s_length];
    matrix_dim_t u_dim;

    svd_get_U_dim(m, n, &u_dim);
    matrix_t U[u_dim.row_num][u_dim.col_num];
    matrix_t S[u_dim.col_num][n];
    matrix_t V[n][n];

    trilateration_get_A_matrix(anchor_num, anchor_pos_matrix, A);
    svd(m, n, A, u_dim.row_num, u_dim.col_num, U, S, V, s_length, s);

    //the forth column of the V matix
    for (int i = 0; i < n; i++) {
        Xh[i] = V[i][3];
    }

    rank = matrix_get_rank(m, n, s, s_length);

    return rank;
}

void trilateration_get_particular_solution(uint8_t m, uint8_t n,
                                           matrix_t anchor_pos_mat[m][n],
                                           matrix_t dist_arr[], matrix_t Xp[])
{
    matrix_t A[m][4];
    matrix_t pinvA[4][m];
    matrix_t b[m];

    trilateration_get_b_vector(m, dist_arr, anchor_pos_mat, b);
    trilateration_get_A_matrix(m, anchor_pos_mat, A);
    moore_penrose_get_pinv(m, 4, A, pinvA);
    matrix_mul_vec(4, m, pinvA, b, Xp);
}

void trilateration_get_quadratic_equation_solution(matrix_t Xp[],
                                                   matrix_t Xh[],
                                                   matrix_t solution_x1[],
                                                   matrix_t solution_x2[])
{

    matrix_t a = 0, b = 0, c = 0, t1 = 0, t2 = 0, delta = 0;
    int i = 0;

    //compute polynomial coefficients: a*t^2 + b*t^+ c
    a = pow(Xh[1], 2)
        + pow(Xh[2], 2)
        + pow(Xh[3], 2);
    b = 2
        * (Xh[1] * Xp[1]
           + Xh[2] * Xp[2]
           + Xh[3] * Xp[3])
        - Xh[0];
    c = pow(Xp[1], 2) + pow(Xp[2], 2)
        + pow(Xp[3], 2) - Xp[0];

    //compute the quadratic equation a*t^2 + b*t+ c
    delta = b * b - 4 * a * c;
    if ((delta > 0) && (a != 0)) {
        if (solution_x1 != NULL) {
            t1 = (-b + sqrt(delta)) / (2 * a);
        }
        if (solution_x2 != NULL) {
            t2 = (-b - sqrt(delta)) / (2 * a);
        }
    }
    else if ((delta == 0) && (a != 0)) {
        if (solution_x1 != NULL) {
            t1 = -b / (2 * a);
        }
        if (solution_x2 != NULL) {
            t2 = t1;
        }

    }
    else if (delta < 0) {   // take only a real part
        if (solution_x1 != NULL) {
            t1 = -b / (2 * a);
        }
        if (solution_x2 != NULL) {
            t2 = -b / (2 * a);
        }
    }

    for (; i < 4; i++) {
        if (solution_x1 != NULL) {
            solution_x1[i] = Xp[i]
                             + t1 * Xh[i];
        }
        if (solution_x2 != NULL) {
            solution_x2[i] = Xp[i]
                             + t2 * Xh[i];
        }
    }

    //DEBUGF("a=%f, b=%f, c=%f, t1=%f, t2=%f\n", (double)a, (double)b, (double)c, (double)t1, (double)t2);
}

void trilateration_get_A_matrix(uint8_t m, matrix_t anchor_pos_matrix[m][3],
                                matrix_t A_matrix[m][4])
{
    int i, j;

    //the first column is equal 1
    for (i = 0; i < m; i++) {
        A_matrix[i][0] = 1;
    }

    //the second, third and firth columns are equal: -2x, -2y, -2z respectively
    for (i = 0; i < m; i++) {
        for (j = 1; j < 4; j++) {
            A_matrix[i][j] = -2 * anchor_pos_matrix[i][j - 1];

        }
    }
}

void trilateration_get_b_vector(uint8_t anchor_num, matrix_t dist_arr[],
                                matrix_t anchor_pos_matrix[anchor_num][3],
                                matrix_t b_vec[])
{

    uint8_t i, j;

    for (i = 0; i < anchor_num; i++) {
        b_vec[i] = dist_arr[i] * dist_arr[i];
        for (j = 0; j < 3; j++) {
            b_vec[i] -= anchor_pos_matrix[i][j]
                        * anchor_pos_matrix[i][j];
        }
    }
}

void trilateration_solve_linear_equation(uint8_t line_num, uint8_t col_num,
                                         matrix_t pseudo_inv_matrix[line_num][col_num],
                                         matrix_t b_vec[], matrix_t sol_vec[])
{
    int i, j;

    memset(sol_vec, 0, col_num * sizeof(matrix_t));

    for (i = 0; i < line_num; i++) {
        for (j = 0; j < col_num; j++) {
            sol_vec[i] += pseudo_inv_matrix[i][j] * b_vec[j];
        }
    }
}
