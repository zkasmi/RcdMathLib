/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Implement the Levenberg--Marquardt (LVM) algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#include <stdio.h>
#include <math.h>
#include <float.h>
#include <inttypes.h>

#include "matrix.h"
#include "vector.h"
#include "solve.h"
#include "utils.h"
#include "loc_levenberg_marquardt.h"

matrix_t loc_levenberg_marquardt_correction(uint8_t ref_points_num,
                                            matrix_t ref_points_matrix[ref_points_num][3],
                                            matrix_t point[3],
                                            matrix_t measured_data_vec[ref_points_num], matrix_t mu,
                                            matrix_t s[3],
                                            void (*f_i)(uint8_t ref_point_num,
                                                        matrix_t ref_point_mat[ref_points_num][3],
                                                        matrix_t point[3], matrix_t d_vec[],
                                                        matrix_t f_vec[]),
                                            void (*jacobian_get_JTJ)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3],
                                                matrix_t data_vec[ref_points_num],
                                                matrix_t JTJ[3][3]),
                                            void (*jacobian_get_JTf)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3],
                                                matrix_t data_vec[ref_points_num],
                                                matrix_t JTf[3]),
                                            void (*jacobian_get_J_mul_s)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3], matrix_t s[3],
                                                matrix_t J_s[ref_points_num])
                                            )
{

    matrix_t Fx[ref_points_num];
    matrix_t JTJ_mu2_I[3][3];
    matrix_t JTF[3];
    matrix_t ro_mu = 0;
    matrix_t Fx_square = 0;
    matrix_t Fx_plus_s_square = 0;
    matrix_t Fx_plus_J_mul_s_square = 0;
    matrix_t Fx_plus_J_mul_s[ref_points_num];
    matrix_t F_x_plus_s[ref_points_num];
    matrix_t point_plus_s[3];
    matrix_t denom;

    //compute: -(J'J + mu^2*I)
    loc_levenberg_marquardt_get_JTJ_mu2_I(ref_points_num, ref_points_matrix,
                                          point,
                                          measured_data_vec, mu, JTJ_mu2_I,
                                          jacobian_get_JTJ);
    matrix_mul_scalar(3, 3, JTJ_mu2_I, -1, JTJ_mu2_I); // -(J'*J+mu^2*eye(n)) is an (n,n) matrix

    jacobian_get_JTf(ref_points_num, ref_points_matrix, point,
                     measured_data_vec, JTF);

    //solve the equation: s = -(J'*J+mu^2*I)\(J'*Fx);
    solve_householder(3, 3, JTJ_mu2_I, JTF, s);

    //f(x)
    f_i(ref_points_num, ref_points_matrix, point, measured_data_vec, Fx);
    //f(x)^2
    Fx_square = vector_get_scalar_product(ref_points_num, Fx, Fx);

    //(x+s)
    vector_add(3, point, s, point_plus_s);

    //f(x+s)
    f_i(ref_points_num, ref_points_matrix, point_plus_s, measured_data_vec,
        F_x_plus_s);
    //f(x+s)^2
    Fx_plus_s_square = vector_get_scalar_product(ref_points_num, F_x_plus_s,
                                                 F_x_plus_s);

    //compute: J(x)*s
    jacobian_get_J_mul_s(ref_points_num, ref_points_matrix, point, s,
                         Fx_plus_J_mul_s);
    //compute: f(x) + J(x)*s
    vector_add(ref_points_num, Fx, Fx_plus_J_mul_s, Fx_plus_J_mul_s);

    //compute: ||f(x) + J(x)*s||^2
    Fx_plus_J_mul_s_square = vector_get_scalar_product(ref_points_num,
                                                       Fx_plus_J_mul_s,
                                                       Fx_plus_J_mul_s);

    denom = Fx_square - Fx_plus_J_mul_s_square;
    if (denom != 0) {
        ro_mu = (Fx_square - Fx_plus_s_square) / denom;
    }
    else {
        puts("ro_mu is infinite !!!");
        ro_mu = FLT_MAX;
    }

    return ro_mu;
}

uint8_t loc_levenberg_marquardt(uint8_t ref_points_num,
                                matrix_t ref_points_matrix[ref_points_num][3],
                                matrix_t start_pos[3],
                                matrix_t measured_data_vec[ref_points_num],
                                matrix_t eps, matrix_t tau, matrix_t beta0,
                                matrix_t beta1,
                                uint8_t max_iter_num,
                                matrix_t est_pos[3],
                                void (*f_i)(uint8_t ref_points_num,
                                            matrix_t ref_point_mat[ref_points_num][3],
                                            matrix_t point[3], matrix_t d_vec[],
                                            matrix_t f_vec[]),
                                void (*jacobian_get_JTJ)(uint8_t ref_points_num,
                                                         matrix_t ref_point_matrix[][3],
                                                         matrix_t point[3],
                                                         matrix_t data_vec[ref_points_num],
                                                         matrix_t JTJ[3][3]),
                                void (*jacobian_get_JTf)(uint8_t ref_points_num,
                                                         matrix_t ref_point_matrix[][3],
                                                         matrix_t point[3],
                                                         matrix_t data_vec[ref_points_num],
                                                         matrix_t JTf[3]),
                                void (*jacobian_get_J_mul_s)(uint8_t ref_points_num,
                                                             matrix_t ref_point_matrix[][3],
                                                             matrix_t point[3], matrix_t s[3],
                                                             matrix_t J_s[ref_points_num])
                                )
{

    matrix_t JTF[3];
    matrix_t JTJ_mu2_I[3][3];
    matrix_t s[3];
    matrix_t mu;
    matrix_t ro_mu;
    uint8_t it;

    /*
     * compute mu0 and -(J'J + mu0^2*I)
     */
    //store J'J in JTJ_mu2_I
    jacobian_get_JTJ(ref_points_num, ref_points_matrix, start_pos,
                     measured_data_vec, JTJ_mu2_I);
    mu = loc_levenberg_marquardt_get_mu0(tau, JTJ_mu2_I);

    //compute: J'J + mu0^2*I
    loc_levenberg_marquardt_get_JTJ_mu2_I(ref_points_num, ref_points_matrix,
                                          start_pos, measured_data_vec, mu,
                                          JTJ_mu2_I,
                                          jacobian_get_JTJ);
    //-(J'*J+mu0^2*eye(n)) is an (n,n) matrix
    matrix_mul_scalar(3, 3, JTJ_mu2_I, -1, JTJ_mu2_I);

    //compute JTF
    jacobian_get_JTf(ref_points_num, ref_points_matrix, start_pos,
                     measured_data_vec, JTF);

    //solve the equation: s=-(J'*J+mu^2*eye(n))\(J'*F);
    solve_householder(3, 3, JTJ_mu2_I, JTF, s);

    vector_copy(3, start_pos, est_pos);
    it = 0;
    while ((vector_get_norm2(3, s)
            > eps * (1 + vector_get_norm2(3, est_pos)))
           && (it < max_iter_num)) {   //norm(s,2)>eps*(1+norm(x,2))

        ro_mu = loc_levenberg_marquardt_correction(ref_points_num,
                                                   ref_points_matrix, est_pos,
                                                   measured_data_vec,
                                                   mu, s, f_i, jacobian_get_JTJ,
                                                   jacobian_get_JTf,
                                                   jacobian_get_J_mul_s);

        while (1) {
            if (ro_mu <= beta0) {
                mu = 2.0 * mu;
                ro_mu = loc_levenberg_marquardt_correction(
                    ref_points_num,
                    ref_points_matrix, est_pos,
                    measured_data_vec, mu, s, f_i,
                    jacobian_get_JTJ,
                    jacobian_get_JTf,
                    jacobian_get_J_mul_s);
            }
            else if (ro_mu >= beta1) {
                mu = mu / 2.0;
                break;
            }
            else {
                break;
            }
        }
        vector_add(3, est_pos, s, est_pos);
        it = it + 1;
    } //while
    return it;
}

matrix_t loc_levenberg_marquardt_get_mu0(matrix_t tau, matrix_t JTJ[3][3])
{
    matrix_t max_diag_JTJ = JTJ[0][0];

    for (uint8_t i = 1; i < 3; i++) {
        if (JTJ[i][i] > max_diag_JTJ) {
            max_diag_JTJ = JTJ[i][i];
        }
    }
    return tau * max_diag_JTJ;
}

//compute: J'J + mu^2*I
void loc_levenberg_marquardt_get_JTJ_mu2_I(uint8_t ref_points_num,
                                           matrix_t ref_points_matrix[ref_points_num][3],
                                           matrix_t point[3],
                                           matrix_t measured_data_vec[ref_points_num],
                                           matrix_t mu,
                                           matrix_t JTJ_mu2_I[3][3],
                                           void (*jacobian_get_JTJ)(
                                               uint8_t ref_points_num,
                                               matrix_t ref_point_matrix[ref_points_num][3],
                                               matrix_t point[3],
                                               matrix_t data_vec[ref_points_num],
                                               matrix_t JTJ[3][3])
                                           )
{
    uint8_t i;

    jacobian_get_JTJ(ref_points_num, ref_points_matrix, point,
                     measured_data_vec, JTJ_mu2_I); //JTJ_mu2_I = J'J

    for (i = 0; i < 3; i++) {
        JTJ_mu2_I[i][i] += pow(mu, 2);  // JTJ_mu2_I = J'*J+mu^2*I;
    }
}
