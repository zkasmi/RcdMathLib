/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Implement the Gauss--Newton algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <stdio.h>

#include "utils.h"
#include "matrix.h"
#include "vector.h"
#include "moore_penrose_pseudo_inverse.h"

uint8_t loc_gauss_newton(uint8_t ref_points_num,
                         matrix_t ref_points_matrix[ref_points_num][3],
                         vector_t start_pos[3],
                         matrix_t measured_data_vec[ref_points_num],
                         matrix_t eps, matrix_t fmin, uint8_t max_iter_num,
                         vector_t est_pos[3],
                         void (*f_i)(uint8_t ref_point_num,
                                     matrix_t ref_point_mat[ref_points_num][3],
                                     matrix_t point[3], matrix_t d_vec[],
                                     matrix_t f_vec[]),
                         void (*jacobian_get_JTJ)(uint8_t ref_points_num,
                                                  matrix_t ref_point_matrix[ref_points_num][3],
                                                  matrix_t point[3],
                                                  matrix_t data_vec[ref_points_num],
                                                  matrix_t JTJ[3][3]),
                         void (*jacobian_get_JTf)(uint8_t ref_points_num,
                                                  matrix_t ref_point_matrix[ref_points_num][3],
                                                  matrix_t point[3],
                                                  matrix_t data_vec[ref_points_num],
                                                  matrix_t JTf[3])
                         )
{

    matrix_t JT_J[3][3];
    matrix_t JT_f[3];
    matrix_t f_vec[ref_points_num];
    matrix_t f_error;
    matrix_t pinv_JTJ_mat[3][3];
    matrix_t correction_vec[3];
    matrix_t pos[3];
    matrix_t next_pos[3];
    matrix_t max_error, min_error;
    matrix_t step;
    uint8_t iter_num;

    f_i(ref_points_num, ref_points_matrix, start_pos, measured_data_vec,
        f_vec);
    f_error = vector_get_norm2(ref_points_num, f_vec);
    max_error = f_error;
    min_error = max_error;
    step = eps;

    vector_copy(3, start_pos, pos);
    vector_copy(3, start_pos, est_pos);
    iter_num = 0;
    while ((step >= eps) && (iter_num < max_iter_num) && (f_error > fmin)) {
        /*
         * Compute then correction terms & next positions
         */

        //JTJ
        jacobian_get_JTJ(ref_points_num, ref_points_matrix, pos,
                         measured_data_vec, JT_J);

        //JTf
        jacobian_get_JTf(ref_points_num, ref_points_matrix, pos,
                         measured_data_vec, JT_f);

        //solve: J'J*s = -J'*f
        moore_penrose_get_pinv(3, 3, JT_J, pinv_JTJ_mat);
        //s = (J'J)\J'*f
        matrix_mul_vec(3, 3, pinv_JTJ_mat, JT_f, correction_vec);

        // x = x - s
        vector_sub(3, pos, correction_vec, next_pos);

        //next step
        step = vector_get_euclidean_distance(3, pos, next_pos);

        // pos = next_pos
        vector_copy(3, next_pos, pos);

        //error vector
        f_i(ref_points_num, ref_points_matrix, pos, measured_data_vec,
            f_vec);
        f_error = vector_get_norm2(ref_points_num, f_vec);

        if (min_error > f_error) {  //store the position with the minimum error
            vector_copy(3, pos, est_pos);
            min_error = f_error;    // update min_error
        }

        max_error = utils_max(f_error, max_error); // update max_error
        iter_num++;
        if ((max_error - min_error) > 10) {
            break;
        }

    } //while

    return iter_num;
}
