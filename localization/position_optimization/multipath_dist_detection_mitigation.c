/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Implement the Multipath Distance Detection and Mitigation (MDDM) algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 * @author      Naouar Guerchali
 *
 * @}
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "matrix.h"
#include "vector.h"
#include "combinatorics.h"
#include "multipath_dist_detection_mitigation.h"
#include "norm_dist_rnd_generator.h"
#include "shell_sort.h"
#include "dist_based_position.h"
#include "DOP.h"
#include "trilateration.h"

matrix_t get_exact_distance_to_anchor(matrix_t ref_point[],
                                      uint32_t exact_point[])
{
    matrix_t d = sqrt(
        pow((exact_point[0] - ref_point[0]), 2)
        + pow((exact_point[1] - ref_point[1]),
              2)
        + pow((exact_point[2] - ref_point[2]),
              2));

    return d;
}

bool is_member(matrix_t vector, uint8_t n, matrix_t multipath[n])
{
    for (int i = 0; i < n; i++) {
        if (vector == multipath[i]) {
            return true;
        }
    }
    return false;
}

bool is_anchor(uint8_t m, matrix_t ref_matr[m][3], uint32_t point[3])
{

    for (uint8_t i = 0; i < m; i++) {
        if (point[0] == ref_matr[i][0] && point[1] == ref_matr[i][1]
            && point[2] == ref_matr[i][2]) {
            return true;
        }

    }
    return false;
}

void sim_UWB_dist(uint8_t m, matrix_t ref_matrix[m][3], uint32_t exact_point[],
                  matrix_t sigma, uint8_t n, matrix_t multipath[n], int seed,
                  matrix_t r_noised_vec[])
{
    matrix_t M_UW = 0.88;
    matrix_t sigma_UW = 1.522;
    matrix_t R = 0.0;

    // set seed
    get_rand_num(seed);

    for (int i = 0; i < m; i++) {
        matrix_t d = get_exact_distance_to_anchor(ref_matrix[i],
                                                  exact_point);

        if (is_member(i + 1, n, multipath) == true) {

            R = log(1 + d)
                * get_norm_distr_rand_num(M_UW,
                                          sigma_UW);

        }
        else {

            R = sigma * get_norm_distr_rand_num(0, 1);
        }
        r_noised_vec[i] = R + d;
    }
}

void get_optimal_partial_ref_matrix(uint8_t anchors_num,
                                    matrix_t ref_matrix[anchors_num][3],
                                    uint8_t k,
                                    uint8_t optimal_anchors_comb[k],
                                    matrix_t opt_partial_ref_matrix[k][3])
{
    for (int i = 0; i < k; i++) {
        for (int j = 0; j < 3; j++) {
            opt_partial_ref_matrix[i][j] =
                ref_matrix[optimal_anchors_comb[i]][j];

        }
    }
}

void get_optimal_partial_r_noised_vec(uint8_t k, matrix_t r_noised_vec[],
                                      uint8_t optimal_anchors_comb[k],
                                      matrix_t opt_sub_r_noised_vec[k])
{
    for (int i = 0; i < k; i++) {
        opt_sub_r_noised_vec[i] = r_noised_vec[optimal_anchors_comb[i]];

    }
}

void recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_matrix[m][3],
                              matrix_t noised_r_vec[m],
                              uint8_t anchors_optimal_combi[k],
                              matrix_t start_optimal_pos[3])
{
    int8_t status_num;
    uint8_t combi_arr[k];
    matrix_t r_vec[3];
    matrix_t part_opt_ref_matrix[k][3];
    matrix_t opt_part_r_noised_vec[k];
    uint8_t index_vector[k];
    matrix_t qres_optimal = FLT_MAX;
    matrix_t pos_solution_x1[4];

    // Initialize the combination generator
    status_num = combinatorics_init(m, k, combi_arr);

    if (status_num == COMBI_ERROR) {
        fprintf(stderr,
                "Error: couldn't initialize the combination generator\n");

        return;
    }

    while (status_num == COMBI_SUCCESS) {
        get_optimal_partial_ref_matrix(m, ref_matrix, k, combi_arr,
                                       part_opt_ref_matrix);

        get_optimal_partial_r_noised_vec(k, noised_r_vec, combi_arr,
                                         opt_part_r_noised_vec);
        // Trilateration
        trilateration2(k, part_opt_ref_matrix, opt_part_r_noised_vec,
                       pos_solution_x1,
                       NULL);

        matrix_t residual_vec[m];
        matrix_t residual_vec_copy[m];

        for (int i = 0; i < m; i++) {

            vector_sub(3, pos_solution_x1 + 1, ref_matrix[i],
                       r_vec);
            residual_vec[i] = vector_get_norm2(3, r_vec)
                              - noised_r_vec[i];
        }

        vector_square(m, residual_vec, residual_vec);

        vector_copy(m, residual_vec, residual_vec_copy);

        shell_sort(residual_vec_copy, m);

        // find index
        vector_get_index_vector(k, m, residual_vec, residual_vec_copy,
                                index_vector);

        matrix_t res_vec_copy_sum = 0;

        for (int i = 0; i < k; i++) {
            res_vec_copy_sum = res_vec_copy_sum
                               + residual_vec_copy[i];
        }

        if (res_vec_copy_sum <= qres_optimal) {
            qres_optimal = res_vec_copy_sum;
            memcpy(anchors_optimal_combi, index_vector,
                   k * sizeof(uint8_t));
            vector_copy(3, pos_solution_x1 + 1, start_optimal_pos);
        }
        status_num = combinatorics_get_next_without_rep(m, k,
                                                        combi_arr);
    } //while
}
