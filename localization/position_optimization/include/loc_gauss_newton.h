/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     position_optimization
 * @{
 *
 * @file
 * @brief       Implement the Gauss--Newton algorithm for position optimization.
 * @note        This function is adapted for localization algorithms.
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef LOC_GAUSS_NEWTON_H_
#define LOC_GAUSS_NEWTON_H_

#include <inttypes.h>

#include "matrix.h"
#include "vector.h"

/**
 * @brief   Implements the modified Gauss--Newton algorithm.
 * @details The user should provide pointers to the error and Jacobian functions.
 * @note    This function is optimized for localization algorithms.
 *
 * @param[in] ref_points_num           number of the reference stations.
 * @param[in] ref_points_matrix[][]    three-dimensional coordinates of the reference stations.
 * @param[in] start_pos[]              start (approximate) position.
 * @param[in] measured_data_vec[]      pointer to the measured data.
 * @param[in] eps                      accuracy bound.
 * @param[in] fmin                     termination tolerance on the error function.
 * @param[in] max_iter_num             maximal iteration number of the Gauss-Newton algorithm.
 * @param[in, out] est_pos[]           estimated (optimized) position.
 * @param[in] (*f_i)                   pointer to the error function.
 * @param[in] (*jacobian_get_JTJ)      pointer to the function that calculates the
 *                                     \f$ J_f^{T} J_{f} \f$ matrix.
 * @param[in] (*jacobian_get_JTf)      pointer to the function that calculates the
 *                                     \f$ J_f^{T} \vec{f} \f$ vector.
 *
 * @return  required iteration number.
 */
uint8_t loc_gauss_newton(uint8_t ref_points_num,
                         matrix_t ref_points_matrix[ref_points_num][3],
                         vector_t start_pos[3],
                         matrix_t measured_data_vec[ref_points_num],
                         matrix_t eps, matrix_t fmin, uint8_t max_iter_num,
                         vector_t est_pos[3],
                         void (*f_i)(uint8_t ref_point_num,
                                     matrix_t ref_point_mat[ref_points_num][3],
                                     matrix_t point[3], matrix_t d_vec[],
                                     matrix_t f_vec[]),
                         void (*jacobian_get_JTJ)(uint8_t ref_points_num,
                                                  matrix_t ref_point_matrix[ref_points_num][3],
                                                  matrix_t point[3],
                                                  matrix_t data_vec[ref_points_num],
                                                  matrix_t JTJ[3][3]),
                         void (*jacobian_get_JTf)(uint8_t ref_points_num,
                                                  matrix_t ref_point_matrix[ref_points_num][3],
                                                  matrix_t point[3],
                                                  matrix_t data_vec[ref_points_num],
                                                  matrix_t JTf[3])
                         );

#endif /* LOC_GAUSS_NEWTON_H_ */
