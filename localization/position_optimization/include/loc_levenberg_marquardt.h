/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     position_optimization
 * @{
 *
 * @file
 * @brief       Implement the Levenberg--Marquardt (LVM) algorithm for position optimization.
 * @note        This function is adapted for localization algorithms.
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 *
 * @}
 */

#ifndef LOC_LEVENBERG_MARQUARDT_H_
#define LOC_LEVENBERG_MARQUARDT_H_

#include <inttypes.h>

#include "matrix.h"

/**
 * @brief   Implements the Levenberg--Marquardt (LVM) algorithm.
 * @details The user should provide pointers to the error and Jacobian functions.
 * @note    This function is optimized for localization algorithms.
 *
 * @param[in] ref_points_num              number of the reference stations.
 * @param[in] ref_points_matrix[][]       three-dimensional coordinates of the reference stations.
 * @param[in] start_pos[]                 start (approximate) position.
 * @param[in] measured_data_vec[]         pointer to the measured data.
 * @param[in] eps                         accuracy bound.
 * @param[in] tau                         \f$ \tau \f$ factor.
 * @param[in] beta0                       \f$ \beta_0 \f$ factor.
 * @param[in] beta1                       \f$ \beta_1 \f$ factor.
 * @param[in] max_iter_num                maximal iteration number of the LVM algorithm.
 * @param[out] est_pos[]                  estimated (optimized) position.
 * @param[in] (*f_i)                      pointer to the error function.
 * @param[in] (*jacobian_get_JTJ)         pointer to the function that calculates the matrix
 *                                        \f$ J_f^{T} J_{f} \f$.
 * @param[in] (*jacobian_get_JTf)         pointer to the function that calculates the vector
 *                                        \f$ J_f^{T} \vec{f} \f$.
 * @param[in] (*jacobian_get_J_mul_s)     pointer to the function that calculates the vector
 *                                        \f$ J_f^{T} \vec{s} \f$.
 *
 * @return  required iteration number.
 */
uint8_t loc_levenberg_marquardt(uint8_t ref_points_num,
                                matrix_t ref_points_matrix[ref_points_num][3],
                                matrix_t start_pos[3],
                                matrix_t measured_data_vec[ref_points_num],
                                matrix_t eps, matrix_t tau, matrix_t beta0,
                                matrix_t beta1, uint8_t max_iter_num,
                                matrix_t est_pos[3],
                                void (*f_i)(uint8_t ref_points_num,
                                            matrix_t ref_point_mat[ref_points_num][3],
                                            matrix_t point[3], matrix_t d_vec[], matrix_t f_vec[]),
                                void (*jacobian_get_JTJ)(uint8_t ref_points_num,
                                                         matrix_t ref_point_matrix[][3],
                                                         matrix_t point[3],
                                                         matrix_t data_vec[ref_points_num],
                                                         matrix_t JTJ[3][3]),
                                void (*jacobian_get_JTf)(uint8_t ref_points_num,
                                                         matrix_t ref_point_matrix[][3],
                                                         matrix_t point[3],
                                                         matrix_t data_vec[ref_points_num],
                                                         matrix_t JTf[3]),
                                void (*jacobian_get_J_mul_s)(uint8_t ref_points_num,
                                                             matrix_t ref_point_matrix[][3],
                                                             matrix_t point[3],
                                                             matrix_t s[3],
                                                             matrix_t J_s[ref_points_num])
                                );

/**
 * @brief   Implements the correction-function of the Levenberg--Marquardt (LVM) algorithm.
 * @details The user should provide pointers to the error and Jacobian functions.
 * @note    This function is optimized for localization algorithms.
 *
 * @param[in] ref_points_num           number of the reference stations.
 * @param[in] ref_points_matrix[][]    three-dimensional coordinates of the reference stations.
 * @param[in] point[]                  a position to adjust.
 * @param[in] measured_data_vec[]      pointer to the measured data.
 * @param[in] mu                       regularization parameter \f$ \mu \f$.
 * @param[out] s[]                     correction vector.
 * @param[in] (*f_i)                   pointer to the error function.
 * @param[in] (*jacobian_get_JTJ)      pointer to the function that calculates the matrix
 *                                     \f$ J_f^{T} J_{f} \f$.
 * @param[in] (*jacobian_get_JTf)      pointer to the function that calculates the vector
 *                                     \f$ J_f^{T} \vec{f} \f$.
 * @param[in] (*jacobian_get_J_mul_s)  pointer to the function that calculates the vector
 *                                     \f$ J_f^{T} \vec{s} \f$
 *
 *
 * @return  the parameter \f$ \rho_{\mu} \f$
 */
matrix_t loc_levenberg_marquardt_correction(uint8_t ref_points_num,
                                            matrix_t ref_points_matrix[ref_points_num][3],
                                            matrix_t point[3],
                                            matrix_t measured_data_vec[ref_points_num],
                                            matrix_t mu, matrix_t s[3],
                                            void (*f_i)(uint8_t ref_point_num,
                                                        matrix_t ref_point_mat[ref_points_num][3],
                                                        matrix_t point[3],
                                                        matrix_t d_vec[], matrix_t f_vec[]),
                                            void (*jacobian_get_JTJ)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3],
                                                matrix_t data_vec[ref_points_num],
                                                matrix_t JTJ[3][3]),
                                            void (*jacobian_get_JTf)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3],
                                                matrix_t data_vec[ref_points_num],
                                                matrix_t JTf[3]),
                                            void (*jacobian_get_J_mul_s)(
                                                uint8_t ref_points_num,
                                                matrix_t ref_point_matrix[ref_points_num][3],
                                                matrix_t point[3], matrix_t s[3],
                                                matrix_t J_s[ref_points_num])
                                            );
/**
 * @brief   Compute the initial value \f$ \mu_0 \f$ of the Levenberg--Marquardt (LVM) algorithm.
 * @details The user should provide a pointer to the matrix
 *          \f$ J_f^{T} J_{f} \f$.
 *
 *
 * @param[in] tau        \f$ \tau \f$ factor.
 * @param[in] JTJ[][]    pointer to the matrix \f$ J_f^T J_f \f$.
 *
 * @return   \f$ \rho_{\mu} \f$-parameter
 */
matrix_t loc_levenberg_marquardt_get_mu0(matrix_t tau, matrix_t JTJ[3][3]);

//compute: J'J + mu^2*I

/**
 * @brief   Compute the matrix
 *          \f$ J_f^{T} J_f + \mu^{(i)})^{2} I \f$.
 * @note    This function is optimized for localization algorithms.
 *
 * @param[in] ref_points_num           number of the reference stations.
 * @param[in] ref_points_matrix[][]    three-dimensional coordinates of the reference stations.
 * @param[in] point[]                  a position to adjust.
 * @param[in] measured_data_vec[]      pointer to the measured data.
 * @param[in] mu                       \f$ \rho_{\mu} \f$-parameter
 * @param[out] JTJ_mu2_I[][]           includes the matrix
 *                                     \f$ J_f^{T} J_f + \mu^{(i)})^{2} I \f$.
 * @param[in] (*jacobian_get_JTJ)      pointer to the function that calculates the matrix
 *                                     \f$ J_f^{T} J_{f} \f$.
 *
 */
void loc_levenberg_marquardt_get_JTJ_mu2_I(uint8_t ref_points_num,
                                           matrix_t ref_points_matrix[ref_points_num][3],
                                           matrix_t point[3],
                                           matrix_t measured_data_vec[ref_points_num], matrix_t mu,
                                           matrix_t JTJ_mu2_I[3][3],
                                           void (*jacobian_get_JTJ)(
                                               uint8_t ref_points_num,
                                               matrix_t ref_point_matrix[ref_points_num][3],
                                               matrix_t point[3],
                                               matrix_t data_vec[ref_points_num],
                                               matrix_t JTJ[3][3])
                                           );

#endif /* LOC_LEVENBERG_MARQUARDT_H_*/
