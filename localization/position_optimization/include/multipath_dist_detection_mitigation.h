/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     position_optimization
 * @{
 *
 * @file
 * @brief       Implement the Multipath Distance Detection and Mitigation (MDDM) algorithm.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 * @author      Naouar Guerchali
 *
 * @}
 */

#ifndef POSITIONING_ROBUST_OTF_H_
#define POSITIONING_ROBUST_OTF_H_

#include <inttypes.h>

#include "matrix.h"

/**
 * @brief   Computes the exact distance between a mobile station and a reference station.
 *
 *
 * @param[in] ref_point    three-dimensional coordinates of a reference station.
 * @param[in] exact_point  three-dimensional coordinates of the mobile device.
 *
 * @return    the exact distance between the mobile station and the reference station.
 */
matrix_t get_exact_distance_to_anchor(matrix_t ref_point[],
                                      uint32_t exact_point[]);
/**
 * @brief   Determine if a candidate is a multipath or not.
 *
 * @param[in] vector          a candidate.
 * @param[in] n               size of the multipath-vector.
 * @param[in] multipath[]     pointer to the multipath-vector.
 *
 * @return true, if a candidate is a multipath.
 * @return false, if not.
 *
 */
bool is_member(matrix_t vector, uint8_t n, matrix_t multipath[n]);

/**
 * @brief   Determine if a point is an anchor or not.
 *
 * @param[in] m               number of the reference points.
 * @param[in] ref_matr[][]    three-dimensional coordinates of the reference stations.
 * @param[in] point[]         three-dimensional coordinates of a point.
 *
 * @return true, if a point is an anchor.
 * @return false, if not.
 */
bool is_anchor(uint8_t m, matrix_t ref_matr[m][3], uint32_t point[3]);

/**
 * @brief    Simulate an UWB-based localization system.
 * @details  The UWB system is simulated by noising the distances from a mobile to the reference
 *           station.
 *
 * @param[in] m               number of the reference points.
 * @param[in] ref_matrix[][]  three-dimensional coordinates of the reference stations.
 * @param[in] exact_point[]   three-dimensional coordinates of a true position.
 * @param[in] sigma           \f$ \sigma_{M,W} \f$ parameter.
 * @param[in] n               number of the multipath-affected distances.
 * @param[in] multipath[]     multipath vector.
 * @param[in] seed            seed value.
 * @param[in] r_noised_vec[]  noised distances.
 *
 */
void sim_UWB_dist(uint8_t m, matrix_t ref_matrix[m][3], uint32_t exact_point[],
                  matrix_t sigma, uint8_t n, matrix_t multipath[n], int seed,
                  matrix_t r_noised_vec[]);
/**
 * @brief    Compute the optimal partial matrix including reference points.
 *
 * @param[in] anchors_num                  number of the reference points.
 * @param[in] ref_matrix[][]               three-dimensional coordinates of the reference stations.
 * @param[in] k                            k anchors (possible sub-experiments).
 * @param[in] optimal_anchors_comb[]       optimal anchor combination.
 * @param[out] opt_partial_ref_matrix[][]  optimal partial matrix of anchors.
 *
 */
void get_optimal_partial_ref_matrix(uint8_t anchors_num,
                                    matrix_t ref_matrix[anchors_num][3],
                                    uint8_t k, uint8_t optimal_anchors_comb[k],
                                    matrix_t opt_partial_ref_matrix[k][3]);

/**
 * @brief    Compute noised distances corresponding to the optimal partial matrix.
 *
 * @param[in] k                                 k anchors (possible sub-experiments).
 * @param[in] r_noised_vec[]                    include noised distances.
 * @param[in] optimal_anchors_comb[]            optimal k-anchor combination.
 * @param[in, out] opt_sub_r_noised_vec[][]     noised distances corresponding to the optimal
 *                                              k-anchors.
 *
 */
void get_optimal_partial_r_noised_vec(uint8_t k, matrix_t r_noised_vec[],
                                      uint8_t optimal_anchors_comb[k],
                                      matrix_t opt_sub_r_noised_vec[k]);

/**
 * @brief   Implement the Multipath Distance Detection and Mitigation (MDDM) algorithm.
 *
 * @param[in] k                         k anchors (possible sub-experiments).
 * @param[in] m                         number of anchors.
 * @param[in] ref_Matrix[][]            three-dimensional coordinates of the reference stations.
 * @param[in] r_noised_vec[]            include noised distances.
 * @param[in] anchors_optimal[]         optimal k-anchor combination.
 * @param[in, out] start_optimal[]      optimal position.
 *
 */
void recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_Matrix[m][3],
                              matrix_t r_noised_vec[m],
                              uint8_t anchors_optimal[k],
                              matrix_t start_optimal[3]);

#endif /* POSITIONING_ROBUST_OTF_H_ */
