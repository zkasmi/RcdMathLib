/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de> 
 *               2020 Freie UniversitĂ¤t Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @defgroup  matrix_decompositions MATRIX_DECOMPOSITIONS
 * @ingroup   linear_algebra
 *
 * @brief    Matrix decomposition algorithms.
 *
 * @details  This module provides algorithms of matrix decomposition such as
 *           Householder, Givens, or Golub-Kahan-Reinsch algorithms.
 *
 * @author   Zakaria Kasmi
 */
