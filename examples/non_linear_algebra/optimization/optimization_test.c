/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of optimization algorithms.
 *
 * @details     Optimization algorithms examples (see the @ref modified_gauss_newton.h
 *              "modified GN" and @ref levenberg_marquardt.h "LVM" optimization methods).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>
#include <math.h>

#include "matrix.h"
#include "vector.h"
#include "levenberg_marquardt.h"
#include "modified_gauss_newton.h"

/**
 * @brief   Calculate the Jacobian matrix of the function @ref optimization_get_f_error.
 *
 * @param[in] x0_vec[]  start values.
 * @param[out] J[][]    calculated Jacobian matrix.
 *
 */
void optimization_get_J(vector_t x0_vec[], matrix_t J[][3])
{
    uint8_t i;
    matrix_t denom;
    uint8_t row_num = 4;

    matrix_t ref_value_matrix[4][3] = { { 0, 0, 1.67 },
                                        { 4.5, 0, 0.75 },
                                        { 4.5, 4.45, 0.75 },
                                        { 0, 4.92, 0.86 } };

    for (i = 0; i < row_num; i++) {
        denom = sqrt(pow(x0_vec[0] - ref_value_matrix[i][0], 2) +
                     pow(x0_vec[1] - ref_value_matrix[i][1], 2) +
                     pow(x0_vec[2] - ref_value_matrix[i][2], 2));
        if (denom != 0) {
            J[i][0] = (x0_vec[0] - ref_value_matrix[i][0]) / denom;
            J[i][1] = (x0_vec[1] - ref_value_matrix[i][1]) / denom;
            J[i][2] = (x0_vec[2] - ref_value_matrix[i][2]) / denom;
        }
    }
}

/**
 * @brief   Calculate the error vector of the approximation.
 *
 * @param[in] x0_vec[]             start values.
 * @param[in] measured_data_vec[]  measured data vector.
 * @param[out] f_vec[]             calculated error vector.
 *
 */
void optimization_get_f_error(vector_t x0_vec[], vector_t measured_data_vec[],
                              vector_t f_vec[])
{
    uint8_t i;
    uint8_t row_num = 4;
    matrix_t ref_point_mat[4][3] = { { 0, 0, 1.67 },
                                     { 4.5, 0, 0.75 },
                                     { 4.5, 4.45, 0.75 },
                                     { 0, 4.92, 0.86 } };

    for (i = 0; i < row_num; i++) {
        f_vec[i] =
            sqrt(
                (x0_vec[0] - ref_point_mat[i][0])
                * (x0_vec[0]
                   - ref_point_mat[i][0])
                +
                (x0_vec[1]
                 - ref_point_mat[i][1])
                * (x0_vec[1]
                   - ref_point_mat[i][1])
                +
                (x0_vec[2]
                 - ref_point_mat[i][2])
                * (x0_vec[2]
                   - ref_point_mat[i][2])
                )
            - measured_data_vec[i];
    }
}

void optimization_test(void)
{
    puts(
        "***************** Optimization test *****************");

    matrix_t true_values_matrix[9][3] = {
        { 1, 1, 0 },   // T1
        { 1, 2, 0 },   // T2
        { 1, 3, 0 },   // T3
        { 2, 1, 0 },   // T4
        { 2, 2, 0 },   // T5
        { 2, 3, 0 },   // T6
        { 3, 1, 0 },   // T7
        { 3, 2, 0 },   // T8
        { 3, 3, 0 },   // T9
    };

    matrix_t x0_matrix[9][3] = {
        { 0.9645166, 0.9894337, -0.1127879 },    // X0_1
        { 0.9706874, 1.9937843, -0.0767517 },    // X0_2
        { 0.9759567, 2.9980380, -0.0421651 },    // X0_3
        { 1.9666907, 0.9892634, -0.1184331 },    // X0_4
        { 1.9734193, 1.9945307, -0.0776960 },    // X0_5
        { 1.9791861, 2.9997600, -0.0385668 },    // X0_6
        { 2.9676093, 0.9886335, -0.1290797 },    // X0_7
        { 2.9749588, 1.9950495, -0.0824135 },    // X0_8
        { 2.9814874, 3.0015139, -0.0370967 },    // X0_9
    };

    matrix_t measured_values_matrix[9][4] = {
        { 2.189355547, 3.732117187, 4.992518309, 4.126940038 },     // M1
        { 2.791860082, 4.115904867, 4.358726079, 3.195059925 },     // M2
        { 3.577157155, 4.685985423, 3.883094303, 2.320377599 },     // M3
        { 2.791860082, 2.810684972, 4.347183679, 4.474971454 },     // M4
        { 3.285646100, 3.303836609, 3.600904464, 3.633252051 },     // M5
        { 3.974524884, 3.992093430, 3.006899725, 2.893757310 },     // M6
        { 3.577157155, 1.968162419, 3.857113556, 5.001588788 },     // M7
        { 3.974524884, 2.625676627, 2.990106568, 4.264874121 },     // M8
        { 4.560484620, 3.452531771, 2.238079928, 3.655150652 },     // M9
    };

    uint8_t vector_values_num = 9;
    uint8_t f_length = 4;
    uint8_t i;

    //GN & LVM-parameters
    matrix_t fmin = 1e-11;
    matrix_t eps = 1e-3;
    matrix_t beta0 = 0.2;
    matrix_t beta1 = 0.8;
    matrix_t tau = 1e-6;
    uint8_t max_iter_num = 100;
    uint8_t iter_num;
    matrix_t est_values_vec[3];

    printf("\nseeked_values_matrix = ");
    matrix_flex_print(vector_values_num, 3, true_values_matrix, 7, 3);
    puts("");

    puts("############## PARAMETERS #################");
    printf("eps = %.0e\ntau = %.0e\nfmin = %.0e\n", eps, tau, fmin);

    for (i = 0; i < vector_values_num; i++) {
        /************************************** Gauss-Newton **************************************/

        printf(
            "\n----------------------------------------- T%u ----------------------------------\n",
            i + 1);
        printf("true vector value = {%.4f, %.4f, %.4f}\n",
               true_values_matrix[i][0],
               true_values_matrix[i][1],
               true_values_matrix[i][2]);
        printf("start vector value = ");
        vector_flex_print(3, x0_matrix[i], 5, 7);
        puts("");

        iter_num = modified_gauss_newton(f_length, 3, x0_matrix[i],
                                         measured_values_matrix[i], eps, fmin,
                                         max_iter_num, est_values_vec,
                                         &optimization_get_f_error, &optimization_get_J);
        printf("Gauss-Newton solution = ");
        vector_flex_print(3, est_values_vec, 5, 7);
        puts("");
        printf("iteration number = %u\n", iter_num);

        vector_clear((uint8_t)3, est_values_vec);
        iter_num = opt_levenberg_marquardt(f_length, 3, x0_matrix[i],
                                           measured_values_matrix[i], eps, tau,
                                           beta0, beta1, max_iter_num,
                                           est_values_vec, &optimization_get_f_error,
                                           optimization_get_J);
        printf("Levenberg-Marquardt solution = ");
        vector_flex_print(3, est_values_vec, 5, 7);
        puts("");
        printf("iteration number = %u", iter_num);
    }
}


/**
 * @brief   Calculate the error vector using exponential data.
 * @details The error function is:
 *       \f$
 *            \vec{f}(x_1, x_2) =
 *              \begin{bmatrix}
 *                 x_1 \mathrm{e}^{x_2} - y_1, ~\hdots, x_1\mathrm{e}^{8x_2}-y_8
 *               \end{bmatrix}^{T},
 *       \f$
 *
 * @param[in] x_vec[]      start vector.
 * @param[in] data_vec[]   data vector.
 * @param[out] f_vec[]     calculated error vector.
 *
 */
void optimization_get_exp_f(vector_t x_vec[], vector_t data_vec[],
                            vector_t f_vec[])
{
    vector_t x1, x2;
    uint8_t i;

    x1 = x_vec[0];
    x2 = x_vec[1];
    for (i = 1; i < 9; i++) {
        f_vec[i - 1] = x1 * exp(i * x2) - data_vec[i - 1];
    }
}

/**
 * @brief   Calculate the Jacobian matrix using exponential data.
 * @details The Jacobian matrix is:
 *           \f$
 *              {J_f} =
 *                       \begin{bmatrix}
 *                         \frac{\partial f_1}{\partial x1} & \frac{\partial f_1}{\partial x_2}  \\
 *                         \frac{\partial f_2}{\partial x1} & \frac{\partial f_2}{\partial x_2}  \\
 *                         \vdots & \vdots &  \\
 *                         \frac{\partial f_n}{\partial x1} & \frac{\partial f_n}{\partial x_2} \\
 *                       \end{bmatrix}  =
 *                      \begin{bmatrix}
 *                        \mathrm{e}^{x_2} & \mathrm{e}^{x_2}  x_1\\
 *                        \mathrm{e}^{2 x_2} & 2\mathrm{e}^{2x_2}  x_1  \\
 *                        \vdots & \vdots &  \\
 *                        \mathrm{e}^{8 x_2} & 8\mathrm{e}^{8x_2}  x_1 \\
 *                      \end{bmatrix}.
 *             \f$
 *
 * @param[in] x_vec[]      start vector.
 * @param[in] J[]          Jacobian matrix.
 *
 */
void optimization_get_exp_Jacobian(vector_t x_vec[], matrix_t J[][2])
{
    vector_t x1, x2;
    uint8_t i;

    x1 = x_vec[0];
    x2 = x_vec[1];
    for (i = 1; i < 9; i++) {
        J[i - 1][0] = exp(i * x2);
        J[i - 1][1] = i * x1 * exp(i * x2);
    }
}

void optimization_exponential_data_test(void)
{
    vector_t d_vec[] = { 8.3, 11.0, 14.7, 19.7, 26.7, 35.2, 44.4, 55.9 };
    vector_t start_x_vec[] = { 6, .3 };
    vector_t f_vec[8];
    matrix_t J[8][2];

    // Parameters of GNM & LVM
    matrix_t beta0 = 0.2;
    matrix_t beta1 = 0.8;
    matrix_t eps = 1e-3;
    matrix_t tau = 1e-6;
    matrix_t f_min = 1e-11;
    uint8_t max_it_num = 3;
    uint8_t iter_num = 0;
    matrix_t est_x_vec[2];

    optimization_get_exp_f(start_x_vec, d_vec, f_vec);
    optimization_get_exp_Jacobian(start_x_vec, J);

    puts(
        "\n\n############### Test Gauss-Newton & LVM algorithms ###############");
    puts("\nExponential Data:");
    iter_num = modified_gauss_newton(8, 2, start_x_vec, d_vec, eps, f_min,
                                     max_it_num, est_x_vec,
                                     &optimization_get_exp_f,
                                     &optimization_get_exp_Jacobian);
    printf("start value = {%.6f, %.6f}\n", start_x_vec[0], start_x_vec[1]);
    printf("Gauss-Newton solution = {%.6f, %.6f}\n", est_x_vec[0], est_x_vec[1]);
    printf("iteration number = %d\n", iter_num);

    iter_num = 0;
    vector_clear(2, est_x_vec);

    iter_num = opt_levenberg_marquardt(8, 2, start_x_vec, d_vec, eps, tau,
                                       beta0, beta1, max_it_num,
                                       est_x_vec, &optimization_get_exp_f,
                                       &optimization_get_exp_Jacobian);
    printf("Levenberg-Marquardt solution = {%.6f, %.6f}\n", est_x_vec[0],
           est_x_vec[1]);
    printf("iteration number = %d\n", iter_num);

}

/**
 * @brief    Calculate the error vector using sinusoidal data.
 * @details  The error function is:
 *          \f$
 *            \vec{f}(x_1, x_2, x_3, x_4)=
 *                \begin{bmatrix}
 *                  x_1 \sin\left( x_2 +x_3\right) +x_4 - y_1 \\
 *                       \vdots \\
 *                  x_1 \sin\left( 12 x_2 +x_3\right) +x_4 - y_{12}
 *                \end{bmatrix}.
 *          \f$
 *
 * @param[in] x_vec[]      start vector.
 * @param[in] data_vec[]   data vector.
 * @param[out] f_vec[]     calculated error vector.
 *
 */
void optimization_get_sin_f(vector_t x_vec[], vector_t data_vec[],
                            vector_t f_vec[])
{
    vector_t x1, x2, x3, x4;
    uint8_t i;

    x1 = x_vec[0];
    x2 = x_vec[1];
    x3 = x_vec[2];
    x4 = x_vec[3];
    for (i = 1; i < 13; i++) {
        f_vec[i - 1] = x1 * sin(x2 * i + x3) + x4 - data_vec[i - 1];
    }
}

/**
 * @brief   Calculate the Jacobian matrix using sinusoidal data.
 * @details The Jacobian matrix is
 *  \f$
 *     J_f   =
 *		\left[\begin{matrix}
 *		   \sin\left( x_2 +x_3\right)    &  x_1 \cos\left( x_2 +x_3\right)     &
 *		                                                  x_1\cos\left( x_2 +x_3\right)   \\
 *		   \sin\left( 2 x_2 +x_3\right)  &  2 x_1 \cos\left( 2 x_2 +x_3\right) &
 *		                                                  x_1 \cos\left( 2 x_2 +x_3\right)\\
 *		  \vdots                         &         \vdots                     \\
 *		  \sin\left( 12 x_2 +x_3\right)  & 12 x_1 \cos\left( 12 x_2 +x_3\right) &
 *		                                                  x_1 \cos\left( 12 x_2 +x_3\right)
 *		\end{matrix}\right].
 *	\f$
 *
 * @param[in] x_vec[]      start vector.
 * @param[in] J[]          Jacobian matrix.
 *
 */
void optimization_get_sin_Jacobian(vector_t x_vec[], matrix_t J[][4])
{
    vector_t x1, x2, x3;
    uint8_t i;

    x1 = x_vec[0];
    x2 = x_vec[1];
    x3 = x_vec[2];
    for (i = 1; i < 13; i++) {
        J[i - 1][0] = sin(i * x2 + x3);
        J[i - 1][1] = i * x1 * cos(i * x2 + x3);
        J[i - 1][2] = x1 * cos(i * x2 + x3);
        J[i - 1][3] = 1;
    }
}

void optimization_sinusoidal_data_test(void)
{
    vector_t d_vec[] = { 61, 65, 72, 78, 85, 90, 92, 92, 88, 81, 72, 63 };
    vector_t start_x_vec[] = { 17, 0.5, 10.5, 77 };
    vector_t f_vec[12];
    matrix_t J[12][4];

    // Parameters of GNM & LVM
    matrix_t beta0 = 0.2;
    matrix_t beta1 = 0.8;
    matrix_t eps = 1e-3;
    matrix_t tau = 1e-6;
    matrix_t f_min = 1e-11;
    uint8_t max_it_num = 2;
    uint8_t iter_num = 0;
    matrix_t est_x_vec[4];

    optimization_get_sin_f(start_x_vec, d_vec, f_vec);
    optimization_get_sin_Jacobian(start_x_vec, J);

//  puts("############### Test Gauss-Newton & LVM algorithms ###############");
    puts("\nSinusoidal Data:");
    iter_num = modified_gauss_newton(12, 4, start_x_vec, d_vec, eps, f_min,
                                     max_it_num, est_x_vec,
                                     &optimization_get_sin_f,
                                     &optimization_get_sin_Jacobian);
    printf("start value = {%.6f, %.6f, %.6f, %.6f}\n", start_x_vec[0],
           start_x_vec[1],
           start_x_vec[2], start_x_vec[3]);
    printf("Gauss-Newton solution = {%.6f, %.6f, %.6f, %.6f}\n", est_x_vec[0],
           est_x_vec[1],
           est_x_vec[2], est_x_vec[3]);
    printf("iteration number = %d\n", iter_num);

    iter_num = 0;
    vector_clear(4, est_x_vec);

    iter_num = opt_levenberg_marquardt(12, 4, start_x_vec, d_vec, eps, tau,
                                       beta0, beta1, max_it_num,
                                       est_x_vec, &optimization_get_sin_f,
                                       &optimization_get_sin_Jacobian);
    printf("Levenberg-Marquardt solution = {%.6f, %.6f, %.6f, %.6f}\n", est_x_vec[0],
           est_x_vec[1],
           est_x_vec[2], est_x_vec[3]);
    printf("iteration number = %d\n", iter_num);
}
