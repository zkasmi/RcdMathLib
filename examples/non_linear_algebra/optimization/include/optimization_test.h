/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of optimization algorithms.
 *
 * @details     Optimization algorithms examples (see the @ref modified_gauss_newton.h
 *              "modified GN" and @ref levenberg_marquardt.h "LVM" optimization methods).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef EXAMPLES_OPTIMIZATION_TEST_H_
#define EXAMPLES_OPTIMIZATION_TEST_H_

/**
 * @brief   Examples of optimization algorithms using the LVM and GN algorithms.
 *
 */
void optimization_test(void);


/**
 * @brief    Examples of optimization algorithms using exponential data.
 * @details  The model function is:
 *           \f$
 *              g(\vec{x}, t) = x_1 \mathrm{e}^{x_2t},
 *           \f$ where \f$\vec{x} = [x_1, x_2]^T\f$ and \f$ \vec{x_0} = [6,.3]\f$ is the initial
 *            guess. The data set is \f$ d(t_i, y_i)\f$, whereby \f$ t_i \f$ is equal to
 *            \f$ \lbrace 1, \hdots, 8 \rbrace\f$ and \f$ y_i\f$ is equal to \f$\lbrace 8.3, 11.0,
 *                14.7, 19.7, 26.7, 35.2, 44.4, 55.9 \rbrace \f$.
 *
 */
void optimization_exponential_data_test(void);

/**
 * @brief    Examples of optimization algorithms using sinusoidal data.
 * @details  The model function is:
 *           \f$
 *             g(\vec{x}, t) = x_1 \sin\left( x_2t +x_3\right) + x_4,
 *           \f$ whereby \f$\vec{x} = [x_1, x_2, x_3, x_4]^T\f$ and \f$\vec{x_0} =
 *               [17, 0.5, 10.5, 77]\f$ is the initial guess. The set of data points is
 *                \f$ d(t_i, y_i) \f$, where \f$t_i \f$ is equal to \f$\lbrace 1, \hdots,
 *                12 \rbrace\f$ and \f$y_i\f$ is equal to \f$\lbrace 61, 65, 72, 78, 85, 90, 92, 92,
 *                 88, 81, 72, 63 \rbrace\f$.
 */
void optimization_sinusoidal_data_test(void);

#endif /* EXAMPLES_OPTIMIZATION_TEST_H_ */
