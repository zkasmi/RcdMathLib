## Examples of Solving Multi-variant Nonlinear Equation Systems

### About

These examples show how to use algorithms for solving multi-variant nonlinear
equation systems. The algorithms implemented to solve multi-variant nonlinear 
equation systems are based on the damped or the Newton-Raphson methods. 

### How to run

Type `make all` to program your board. 

## Note 1

The data type used can be set in the matrix.h and vector.h files. The user 
can choose between the float or the double data types. We recommend to set 
the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application. 

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.

