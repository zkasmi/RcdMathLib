/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of solving non-linear equation systems.
 *
 * @details     Solving non-linear equation systems examples (see @ref fsolve.h "fsolve" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef FSOLVE_TEST_H_
#define FSOLVE_TEST_H_

/**
 * @brief   Examples of solving non-linear equation systems.
 *
 */
void fsolve_test(void);

#endif /* FSOLVE_TEST_H_ */
