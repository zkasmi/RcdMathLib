/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of solving non-linear equation systems.
 *
 * @details     Solving non-linear equation systems examples (see @ref fsolve.h "fsolve" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <float.h>

#include "vector.h"
#include "matrix.h"
#include "damped_newton_raphson.h"
#include "newton_raphson.h"
#include "fsolve.h"

/********************** f1 ******************************/

/*
 *  f1(x1,x2) = [x1^3 + x2 - 1;
 *               x2^3 - x1 + 1]
 */
/**
 *
 * @brief   The non-linear system f1.
 * @details \f$ f_1 \left(x_1, x_2 \right)  =
 *		\begin{bmatrix}
 *		   x_1^3 + x_2 - 1 \\
 *		   x_2^3 - x_1 + 1 \\
 *	        \end{bmatrix} \f$
 *
 * @param[in] x_arr[]   pointer to the \f$ x_1 \f$ and \f$ x_2 \f$ values.
 * @param[in] f1_vec[]  pointer to the \f$ f_1 \f$ function values.
 *
 */
void get_non_lin_sys_f1_(vector_t x_arr[], vector_t f1_vec[])
{

    f1_vec[0] = pow(x_arr[0], 3) + x_arr[1] - 1;
    f1_vec[1] = pow(x_arr[1], 3) - x_arr[0] + 1;

}

/*
 *  J1(x1,x2) = [3*x1^2, 1;
 *               -1, 3*x2^2]
 */

/**
 *
 * @brief   The Jacobian matrix of the non-linear system \f$ f_1 \f$.
 * @details \f$ J_1 \left( x_1, x_2\right) =
 *		\begin{bmatrix}
 *		   3 \times x_1^2 & 1 \\
 *		     -1           & 3 \times x_2^2 \\
 *	        \end{bmatrix} \f$
 *
 * @param[in] x_arr[]   pointer to the \f$ x_1 \f$ and \f$ x_2 \f$ values.
 * @param[in] J1[][]   pointer to the Jacobian function \f$ J_1 \f$.
 *
 */
void get_non_lin_sys_J1_(vector_t x_arr[], matrix_t J1[][2])
{
    J1[0][0] = 3 * pow(x_arr[0], 2);
    J1[0][1] = 1;
    J1[1][0] = -1;
    J1[1][1] = 3 * pow(x_arr[1], 2);
}

/****************************** f2 ********************************/

/*
 *  f2(x1,x2,x3) = [3*x1-cos(x2*x3)-1/2;
 *                  x1^2-81*(x2+0.1)^2+sin(x3)+1.06;
 *                  exp(-x1*x2)+20*x3+(10*pi-3)/3
 *                  ]
 */
/**
 *
 * @brief   The non-linear system \f$ f_2 \f$.
 * @details \f$ f_2 \left( x_1, x_2, x_3 \right)  =
 *		\begin{bmatrix}
 *		   3 x_1 - \cos\left( x_2 \times x_3 \right) - \dfrac{1}{2} \\
 *		   x_1^2 - 81 \left( x_2 + 0.1 \right)^2 + \sin(x_3) + 1.06  \\
 *		   \exp \left( -x1 \times x_2 \right) + 20 x_3 + \dfrac{10 \pi -3}{3} \\
 *	        \end{bmatrix} \f$
 *
 * @param[in] x_arr[]   pointer to the \f$ x_1 \f$, \f$ x_2 \f$, and \f$ x_3 \f$ values.
 * @param[in] f2_vec[]  pointer to the \f$ f_2 \f$ function values.
 *
 */
void get_non_lin_sys_f2_(vector_t x_arr[], vector_t f2_vec[])
{

    f2_vec[0] = 3 * x_arr[0] - cos(x_arr[1] * x_arr[2]) - 0.5;
    f2_vec[1] = pow(x_arr[0], 2) - 81 * pow((x_arr[1] + 0.1), 2)
                + sin(x_arr[2]) + 1.06;
    f2_vec[2] = exp(-x_arr[0] * x_arr[1]) + 20 * x_arr[2]
                + (10 * M_PI - 3) / 3;
}

/*
 *       [               3,   x3*sin(x2*x3), x2*sin(x2*x3)]
 * Jf2 = [            2*x1,  -162*x2 - 81/5,       cos(x3)]
 *       [ -x2*exp(-x1*x2), -x1*exp(-x1*x2),            20]
 */
/**
 *
 * @brief   The Jacobian matrix of the non-linear system \f$ f_2 \f$.
 * @details \f$ J_2 \left( x_1, x_2, x_3 \right) =
 *		\begin{bmatrix}
 *		     3 & x_3 \sin\left( x_2 x_3 \right)  & x_2 \sin\left( x_2 x_3 \right) \\
 *		   2 x_1 & -162 x2 - \dfrac{81}{5}       & \cos(x_3) \\
 *		-x_2 \exp\left( -x_1 x_2 \right) & -x_1 \exp\left( -x_1 x_2 \right) & 20
 *	        \end{bmatrix} \f$
 *
 * @param[in] x_arr[]  pointer to the \f$ x_1 \f$, \f$ x_2 \f$, and \f$ x_3 \f$ values.
 * @param[in] J2[][]   pointer to the Jacobian function \f$ J_2 \f$.
 *
 */
void get_non_lin_sys_J2_(vector_t x_arr[], matrix_t J2[][3])
{
    J2[0][0] = 3;
    J2[0][1] = x_arr[2] * sin(x_arr[1] * x_arr[2]);
    J2[0][2] = x_arr[1] * sin(x_arr[1] * x_arr[2]);
    J2[1][0] = 2 * x_arr[0];
    J2[1][1] = -162 * x_arr[1] - 16.2;
    J2[1][2] = cos(x_arr[2]);
    J2[2][0] = -x_arr[1] * exp(-x_arr[0] * x_arr[1]);
    J2[2][1] = -x_arr[0] * exp(-x_arr[0] * x_arr[1]);
    J2[2][2] = 20;
}

/****************************** f3 ********************************/
/*
 *
 *  f3(x1,x2,x3) = [exp(-x1*x2) + log(x1)-exp(-2);
 *                  exp(x1)-sqrt(x3)/x1-exp(1)+2;
 *                  x1 + x2 - x2*x3+5
 *                  ]
 */
/**
 *
 * @brief   The non-linear system \f$ f_3 \f$.
 * @details \f$ f_3 \left( x_1, x_2, x_3 \right) =
 *		\begin{bmatrix}
 *		   \exp \left( -x_1 x_2 \right) + \log \left( x_1 \right)-\exp \left( -2 \right) \\
 *                  \exp \left( x_1 \right) - \dfrac{\sqrt{x_3}}{x_1} - \exp \left( 1 \right) + 2\\
 *		    x_1 + x_2 - x_2 x_3 + 5 \\
 *	        \end{bmatrix} \f$
 *
 * @param[in] x_arr[]   pointer to the \f$ x_1 \f$, \f$ x_2 \f$, and \f$ x_3 \f$ values.
 * @param[in] f3_vec[]  pointer to the \f$ f_3 \f$ function values.
 *
 */
void get_non_lin_sys_f3_(vector_t x_arr[], vector_t f3_vec[])
{
    double complex z;

    f3_vec[0] = exp(-x_arr[0] * x_arr[1]) + log(x_arr[0]) - exp(-2);
    if (x_arr[2] < 0) {
        z = csqrtl(x_arr[2]);
        f3_vec[1] = exp(x_arr[0]) - creal(z) / x_arr[0] - exp(1) + 2;
    }
    else {
        f3_vec[1] = exp(x_arr[0]) - sqrt(x_arr[2]) / x_arr[0] - exp(1)
                    + 2;
    }

    f3_vec[2] = x_arr[0] + x_arr[1] - x_arr[1] * x_arr[2] + 5;
}

/*
 *       [   1/x1 - x2*exp(-x1*x2), -x1*exp(-x1*x2),                  0]
 * Jf3 = [ exp(x1) + x3^(1/2)/x1^2,               0, -1/(2*x1*x3^(1/2))]
 *       [                       1,          1 - x3,                -x2]
 */
/**
 *
 * @brief   The Jacobian matrix of the non-linear system \f$ f_3 \f$.
 * @details \f$ J_3 \left( x_1, x_2, x_3 \right) =
 *          \begin{bmatrix}
 *	        \dfrac{1}{x_1}-x_2 \exp\left(-x_1 x_2\right) & -x_1\exp\left(-x_1 x_2\right) & 0 \\
 *		\exp\left(x_1\right) + \dfrac{\sqrt{x_3}}{x_1^2} & 0 & \dfrac{-1}{2x_1\sqrt{x_3}} \\
 *		 1 & 1-x_3  & -x_2
 *	    \end{bmatrix} \f$
 *
 * @param[in] x_arr[]  pointer to the \f$ x_1 \f$, \f$ x_2 \f$, and \f$ x_3 \f$ values.
 * @param[in] J3[][]   pointer to the Jacobian function \f$ J_3 \f$.
 *
 */
void get_non_lin_sys_J3_(vector_t x_arr[], matrix_t J3[][3])
{
    double complex z;

    J3[0][0] = 1 / x_arr[0] - x_arr[1] * exp(-x_arr[0] * x_arr[1]);
    J3[0][1] = -x_arr[0] * exp(-x_arr[0] * x_arr[1]);
    J3[0][2] = 0;
    if (x_arr[2] < 0) {
        z = csqrtl(x_arr[2]);
        J3[1][0] = exp(x_arr[0]) + creal(z) / pow(x_arr[0], 2);
    }
    else {
        J3[1][0] = exp(x_arr[0]) + sqrt(x_arr[2]) / pow(x_arr[0], 2);
    }
    J3[1][1] = 0;
    if (x_arr[2] < 0) {
        z = csqrtl(x_arr[2]);
        if ((x_arr[0] == 0) || (creal(z) == 0)) {
            //J3[1][2] = -1;
            J3[1][2] = -DBL_MAX;
        }
        else {
            J3[1][2] = -1 / (2 * x_arr[0] * creal(z));
        }

    }
    else {
        J3[1][2] = -1 / (2 * x_arr[0] * sqrt(x_arr[2]));
    }

    J3[2][0] = 1;
    J3[2][1] = 1 - x_arr[2];
    J3[2][2] = -x_arr[1];
}

void fsolve_test(void)
{
    puts("############ Test Solve Non-linear Algebra Module ############");
    puts(
        "\n******************************* f1 ******************************");
    vector_t f1_vec[2];
    vector_t x1_arr[2] = { 3, 2 };
    matrix_t J1[2][2];
    get_non_lin_sys_f1_(x1_arr, f1_vec);
    get_non_lin_sys_J1_(x1_arr, J1);
    uint8_t f1_length = 2;
    vector_t x0_arr1[2] = { .5, .5 };
    uint8_t n = 2;
    vector_t est_x1[n];
    uint8_t iter_num;
    enum NON_LIN_ALGORITHM algo;

    algo = Newton_Raphson;
    iter_num = fsolve(f1_length, n, x0_arr1, algo, est_x1,
                      &get_non_lin_sys_f1_, &get_non_lin_sys_J1_);
//    iter_num = newton_raphson(f1_length, n, x0_arr1, eps, max_it_num, est_x1,
//                              &get_non_lin_sys_f1_, &get_non_lin_sys_J1_);
    printf("iter_num = %u\nx1 = ", iter_num);
    vector_flex_print(n, est_x1, 3, 3);
    puts("");
    get_non_lin_sys_f1_(est_x1, f1_vec);
    printf("f1(x1) = ");
    vector_flex_print(f1_length, f1_vec, 3, 3);

    puts("\nDamped Newton");
    iter_num = 0;
    vector_clear(n, est_x1);
    vector_clear(f1_length, f1_vec);
    algo = Damped_Newton_Raphson;
    iter_num = fsolve(f1_length, n, x0_arr1, algo, est_x1,
                      &get_non_lin_sys_f1_, &get_non_lin_sys_J1_);
    printf("iter_num = %u\nx1 = ", iter_num);
    vector_flex_print(n, est_x1, 3, 3);
    puts("");
    get_non_lin_sys_f1_(est_x1, f1_vec);
    printf("f1(x1) = ");
    vector_flex_print(f1_length, f1_vec, 3, 3);

    puts(
        "/\n******************************** f2 ******************************/");
    vector_t f2_vec[3];
    matrix_t J2[3][3];
    vector_t x2_arr[3] = { 0.1, 0.2, 0.3 };
    get_non_lin_sys_f2_(x2_arr, f2_vec);
    get_non_lin_sys_J2_(x2_arr, J2);
    uint8_t f2_length = 3;
    n = 3;
    vector_t est_x2[n];
    vector_t x0_arr2[3] = { 1, 1, 1 };
    algo = Newton_Raphson;
    iter_num = fsolve(f2_length, n, x0_arr2, algo, est_x2,
                      &get_non_lin_sys_f2_, &get_non_lin_sys_J2_);
//    iter_num = newton_raphson(f2_length, n, x0_arr2, eps, max_it_num, est_x2,
//                              &get_non_lin_sys_f2_, &get_non_lin_sys_J2_);
    printf("iter_num = %u\nx2 = ", iter_num);
    vector_flex_print(n, est_x2, 3, 9);
    puts("");
    get_non_lin_sys_f2_(est_x2, f2_vec);
    printf("f2(x2) = ");
    vector_flex_print(f2_length, f2_vec, 3, 3);
    puts("");

    puts("\nDamped Newton");
    iter_num = 0;
    vector_clear(n, est_x2);
    vector_clear(f2_length, f2_vec);
    algo = Damped_Newton_Raphson;
    iter_num = fsolve(f2_length, n, x0_arr2, algo, est_x2,
                      &get_non_lin_sys_f2_, &get_non_lin_sys_J2_);
    printf("iter_num = %u\nx2 = ", iter_num);
    vector_flex_print(n, est_x2, 3, 9);
    puts("");
    get_non_lin_sys_f2_(est_x2, f2_vec);
    printf("f2(x2) = ");
    vector_flex_print(f2_length, f2_vec, 3, 3);

    puts(
        "/\n******************************** f3 ******************************/");

    vector_t f3_vec[3];
    vector_t x3_arr[3] = { 1e1, 2e2, 3e3 };
    matrix_t J3[3][3];
    get_non_lin_sys_f3_(x3_arr, f3_vec);
    get_non_lin_sys_J3_(x3_arr, J3);

    uint8_t f3_length = 3;
    n = 3;
    vector_t est_x3[n];
    vector_t x0_arr3[3] = { 1, 1, 1 };
    algo = Newton_Raphson;
    iter_num = fsolve(f3_length, n, x0_arr3, algo, est_x3,
                      &get_non_lin_sys_f3_,
                      &get_non_lin_sys_J3_);
//    iter_num = newton_raphson(f3_length, n, x0_arr3, eps, max_it_num, est_x3,
//                              &get_non_lin_sys_f3_, &get_non_lin_sys_J3_);
    printf("iter_num = %u\nx2 = ", iter_num);
    vector_flex_print(n, est_x3, 3, 9);
    puts("");
    get_non_lin_sys_f3_(est_x3, f3_vec);
    printf("f3(x3) = ");
    vector_flex_print(f3_length, f3_vec, 3, 3);
    puts("");

    puts("\nDamped Newton");
    iter_num = 0;
    vector_clear(n, est_x3);
    vector_clear(f3_length, f3_vec);
    iter_num = fsolve(f3_length, n, x0_arr3, algo, est_x3,
                      &get_non_lin_sys_f3_,
                      &get_non_lin_sys_J3_);
    printf("iter_num = %u\nx3 = ", iter_num);
    vector_flex_print(n, est_x3, 3, 9);
    puts("");
    get_non_lin_sys_f3_(est_x3, f3_vec);
    printf("f3(x3) = ");
    vector_flex_print(f3_length, f3_vec, 3, 3);
    puts("");

}
