/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the Householder algorithm.
 *
 * @details     Householder algorithm examples (see the @ref qr_householder.h
 *              "Householder" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdbool.h>
#include <stdio.h>

#include "matrix.h"
#include "qr_householder.h"


void householder_test(void)
{
    puts("############ Test the Householder algorithm ###############");
    matrix_t A[10][5] = { { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
                          { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
                          { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
                          { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
                          { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 },
                          { 0.0975, 0.1419, 0.7577, 0.8235, 0.4898 },
                          { 0.2785, 0.4218, 0.7431, 0.6948, 0.4456 },
                          { 0.5469, 0.9157, 0.3922, 0.3171, 0.6463 },
                          { 0.9575, 0.7922, 0.6555, 0.9502, 0.7094 },
                          { 0.9649, 0.9595, 0.1712, 0.0344, 0.7547 } };
    uint8_t m = 10, n = 5;
    matrix_t copy_A[m][n];
    matrix_t Q_red[m][n];
    matrix_t Q[m][m];

    matrix_copy(m, n, A, copy_A);
    matrix_clear(m, n, Q_red);
    qr_householder_decomp(m, n, copy_A, n, Q_red, true);
    printf("Q_red = ");
    matrix_flex_print(m, n, Q_red, 7, 4);
    printf("R_red = ");
    matrix_flex_print(n, n, copy_A, 7, 4);

    matrix_copy(m, n, A, copy_A);
    matrix_clear(m, m, Q);
    qr_householder_decomp(m, n, copy_A, m, Q, false);
    printf("Q = ");
    matrix_flex_print(m, m, Q, 7, 4);
    printf("R = ");
    matrix_flex_print(m, n, copy_A, 7, 4);

    matrix_t B[4][4] = {
        { 6, 0, 0, 6 },
        { 2, 5, 0, 4 },
        { 0, 6, 2, 3 },
        { 0, 1, 5, 5 }
    };
    m = 4;
    n = 4;
    matrix_t Q1_red[m][n];
    qr_householder_decomp(m, n, B, m, Q1_red, true);
    printf("Q1 = ");
    matrix_flex_print(m, m, Q1_red, 7, 4);
    printf("R1 = ");
    matrix_flex_print(m, n, B, 7, 4);
}
