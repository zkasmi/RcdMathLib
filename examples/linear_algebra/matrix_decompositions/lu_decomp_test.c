/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the LU algorithm with pivoting.
 *
 * @details     LU algorithm with pivoting examples (see the @ref lu_decomp.h
 *              "Gaussian Elimination with pivoting" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>

#include "matrix.h"
#include "lu_decomp.h"

void lu_decomp_test(void)
{
    puts("############ Test the LU decomposition algorithm ###############");
    matrix_t A[5][5] = { { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
                         { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
                         { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
                         { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
                         { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 }, };
    uint8_t n;

    n = 5;
    matrix_t L[n][n];
    matrix_t P[n][n];
    lu_decomp(n, A, L, P);
    printf("L = ");
    matrix_flex_print(n, n, L, 7, 4);
    printf("U = ");
    matrix_flex_print(n, n, A, 7, 4);
    printf("P = ");
    matrix_flex_print(n, n, P, 7, 4);

    matrix_t B[11][11] = {
        { 0.4387,  0.6797,  0.5060,  0.2435,  0.9172,  0.1299,  0.2630,  0.9961,  0.2599,  0.1450,
          0.4173 },
        { 0.3816,  0.6551,  0.6991,  0.9293,  0.2858,  0.5688,  0.6541,  0.0782,  0.8001,  0.8530,
          0.0497 },
        { 0.7655,  0.1626,  0.8909,  0.3500,  0.7572,  0.4694,  0.6892,  0.4427,  0.4314,  0.6221,
          0.9027 },
        { 0.7952,  0.1190,  0.9593,  0.1966,  0.7537,  0.0119,  0.7482,  0.1067,  0.9106,  0.3510,
          0.9448 },
        { 0.1869,  0.4984,  0.5472,  0.2511,  0.3804,  0.3371,  0.4505,  0.9619,  0.1818,  0.5132,
          0.4909 },
        { 0.4898,  0.9597,  0.1386,  0.6160,  0.5678,  0.1622,  0.0838,  0.0046,  0.2638,  0.4018,
          0.4893 },
        { 0.4456,  0.3404,  0.1493,  0.4733,  0.0759,  0.7943,  0.2290,  0.7749,  0.1455,  0.0760,
          0.3377 },
        { 0.6463,  0.5853,  0.2575,  0.3517,  0.0540,  0.3112,  0.9133,  0.8173,  0.1361,  0.2399,
          0.9001 },
        { 0.7094,  0.2238,  0.8407,  0.8308,  0.5308,  0.5285,  0.1524,  0.8687,  0.8693,  0.1233,
          0.3692 },
        { 0.7547,  0.7513,  0.2543,  0.5853,  0.7792,  0.1656,  0.8258,  0.0844,  0.5797,  0.1839,
          0.1112 },
        { 0.2760,  0.2551,  0.8143,  0.5497,  0.9340,  0.6020,  0.5383,  0.3998,  0.5499,  0.2400,
          0.7803 }
    };

    n = 11;
    matrix_t L1[n][n];
    matrix_t P1[n][n];
    lu_decomp(n, B, L1, P1);
    printf("L1 = ");
    matrix_flex_print(n, n, L1, 7, 4);
    printf("U1 = ");
    matrix_flex_print(n, n, B, 7, 4);
    printf("P1 = ");
    matrix_flex_print(n, n, P1, 7, 4);
}
