/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the SVD algorithm.
 *
 * @details     SVD algorithm examples (see the @ref svd.h
 *              "Singular Value Decomposition (SVD)" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>
#include <inttypes.h>

#include "svd.h"
#include "matrix.h"
#include "vector.h"


void svd_test(void)
{
    puts("############ Test the SVD algorithm ###############");
    uint8_t m, n;
    uint8_t i = 0;

    //m<n
    matrix_t matrix1_arr[3][4] = { { 1.0000, -3.6000, -1.2000, -2.8400 },
                                   { 1.0000, -7.2400, -7.1800, -2.2400 },
                                   { 1.0000, -12.0600, -1.3400, -3.6600 } };

    m = 3;
    n = 4;
    matrix_t matrix2_arr[3][4] = {
        { 1, -22, 30, -20 },
        { 1, -26, -42, -10 },
        { 1, 18, -30, -14 }
    };
    svd_compute_print_U_S_V_s(m, n, matrix1_arr, ++i);
    svd_compute_print_U_S_V_s(m, n, matrix2_arr, ++i);

    printf(
        "######## Test %d: Singular Value Decomposition m < n : (2,3) ########\n",
        ++i);
    matrix_t matrix3_arr[2][3] = {
        { 3, 1, 1 },
        { -1, 3, 1 }
    };
    m = 2;
    n = 3;
    svd_compute_print_U_S_V_s(m, n, matrix3_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m > n : (3,2) ########\n",
        ++i);
    matrix_t matrix4_arr[3][2] = {
        { 1, 2 },
        { 2, 2 },
        { 2, 1 }
    };
    m = 3;
    n = 2;
    svd_compute_print_U_S_V_s(m, n, matrix4_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m = n : (4,4) ########\n",
        ++i);

    matrix_t matrix5_arr[4][4] = {
        { 6, 0, 0, 6 },
        { 2, 5, 0, 4 },
        { 0, 6, 2, 3 },
        { 0, 1, 5, 5 }
    };
    m = 4;
    n = 4;
    svd_compute_print_U_S_V_s(m, n, matrix5_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m < n : (4,7) ########\n",
        ++i);
    matrix_t matrix6_arr[4][7] = {
        { 2, 5, 4, 6, 3, 2, 1 },
        { 1, 2, 4, 6, 0, 0, 6 },
        { 2, 1, 1, 5, 4, 6, 3 },
        { 3, 3, 1, 0, 4, 5, 0 }
    };
    m = 4;
    n = 7;
    svd_compute_print_U_S_V_s(m, n, matrix6_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m = n : (3,3) ########\n",
        ++i);
    matrix_t matrix7_arr[3][3] = {
        { 1., 2., 3 },
        { 4., 5., 6. },
        { 7., 8., 10. }
    };
    m = 3;
    n = 3;
    svd_compute_print_U_S_V_s(m, n, matrix7_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m < n : (5,7) ########\n",
        ++i);
    matrix_t matrix8_arr[5][7] = {
        { 6, 2, 7, 3, 6, 0, 1 },
        { 7, 1, 6, 3, 6, 4, 0 },
        { 3, 5, 1, 1, 0, 2, 3 },
        { 5, 5, 7, 3, 1, 3, 0 },
        { 6, 5, 4, 1, 2, 7, 1 }
    };
    m = 5;
    n = 7;
    svd_compute_print_U_S_V_s(m, n, matrix8_arr, i);

    //m>n
    printf(
        "######## Test %d: Singular Value Decomposition m > n : (7,5) ########\n",
        ++i);
    matrix_t matrix9_arr[7][5] = {
        { 6.395, 2.897, 7.333, 3.777, 6.543 },
        { 7.635, 1.235, 6.301, 3.356, 6.777 },
        { 3.595, 5.901, 1.408, 1.697, 0.539 },
        { 5.501, 5.562, 7.271, 3.907, 1.651 },
        { 6.198, 5.069, 4.238, 1.388, 2.855 },
        { 0.000, 1.999, 4.103, 0.561, 2.908 },
        { 3.834, 3.172, 0.009, 7.069, 1.075 }
    };

    m = 7;
    n = 5;
    svd_compute_print_U_S_V_s(m, n, matrix9_arr, i);

    printf(
        "######## Test %d: Singular Value Decomposition m > n : (5,7) ########\n",
        ++i);
    matrix_t matrix10_arr[5][7] = {
        { 6.395, 2.897, 7.333, 3.777, 6.543, 0.000, 1.999 },
        { 7.635, 1.235, 6.301, 3.356, 6.777, 4.103, 0.561 },
        { 3.595, 5.901, 1.408, 1.697, 0.539, 2.908, 3.834 },
        { 5.501, 5.562, 7.271, 3.907, 1.651, 3.172, 0.009 },
        { 6.198, 5.069, 4.238, 1.388, 2.855, 7.069, 1.075 }
    };
    m = 5;
    n = 7;
    svd_compute_print_U_S_V_s(m, n, matrix10_arr, i);
}
