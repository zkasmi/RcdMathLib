/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the Givens algorithm.
 *
 * @details     Givens algorithm examples (see @ref qr_givens.h the "Givens" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef GIVENS_TEST_H_
#define GIVENS_TEST_H_

/**
 * @brief   Examples of the Givens algorithm.
 *
 */
void givens_test(void);

#endif /* GIVENS_TEST_H_ */
