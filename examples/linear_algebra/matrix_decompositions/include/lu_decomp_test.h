/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the LU algorithm with pivoting.
 *
 * @details     LU algorithm with pivoting examples (see the @ref lu_decomp.h
 *              "Gaussian Elimination with pivoting" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef LU_DECOMP_TEST_H_
#define LU_DECOMP_TEST_H_

/**
 * @brief   Examples of the LU algorithm with pivoting.
 *
 */
void lu_decomp_test(void);

#endif /* LU_DECOMP_TEST_H_ */
