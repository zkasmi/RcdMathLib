/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the SVD algorithm.
 *
 * @details     SVD algorithm examples (see the @ref svd.h
 *              "Singular Value Decomposition (SVD)" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef SVD_TEST_H_
#define SVD_TEST_H_

/**
 * @brief   Examples of the Givens algorithm.
 *
 */
void svd_test(void);

#endif /* SVD_TEST_H_ */
