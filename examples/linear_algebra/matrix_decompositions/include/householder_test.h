/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the Householder algorithm.
 *
 * @details     Householder algorithm examples (see the @ref qr_householder.h
 *              "Householder" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef HOUSEHOLDER_TEST_H_
#define HOUSEHOLDER_TEST_H_

/**
 * @brief   Examples of the Householder algorithm.
 *
 */
void householder_test(void);

#endif /* HOUSEHOLDER_TEST_H_ */
