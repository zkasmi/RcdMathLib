/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the Givens algorithm.
 *
 * @details     Givens algorithm examples (see @ref qr_givens.h the "Givens" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>

#include "qr_givens.h"
#include "matrix.h"

void givens_test(void)
{
    puts("############ Test the Givens algorithm ###############");
    matrix_t xj, xij;

    xj = 100;
    xij = 130;

    matrix_t c_s_t_r_vec[4];
    qr_givens_get_params(xj, xij, c_s_t_r_vec);

    printf("c = %7.4f\n", c_s_t_r_vec[0]);
    printf("s = %7.4f\n", c_s_t_r_vec[1]);
    printf("t = %7.4f\n", c_s_t_r_vec[2]);
    printf("r = %7.4f\n", c_s_t_r_vec[3]);

    puts("************ MATRIX A ************");
    matrix_t A[10][5] = {
        { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
        { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
        { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
        { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
        { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 },
        { 0.0975, 0.1419, 0.7577, 0.8235, 0.4898 },
        { 0.2785, 0.4218, 0.7431, 0.6948, 0.4456 },
        { 0.5469, 0.9157, 0.3922, 0.3171, 0.6463 },
        { 0.9575, 0.7922, 0.6555, 0.9502, 0.7094 },
        { 0.9649, 0.9595, 0.1712, 0.0344, 0.7547 }
    };

    uint8_t m, n;
    m = 10;
    n = 5;

    matrix_t copy_A[m][n];

    puts("+++++++ Reduced QR-form +++++++");
    matrix_copy(m, n, A, copy_A);
    matrix_t red_Q[m][n];
    qr_givens_decomp(m, n, copy_A, n, red_Q, true);
    printf("red_Q = ");
    matrix_flex_print(m, n, red_Q, 7, 4);
    puts("");
    printf("red_R = ");
    matrix_flex_print(n, n, copy_A, 7, 4);
    puts("+++++++ Full QR-form +++++++");
    matrix_copy(m, n, A, copy_A);
    matrix_t full_Q[m][m];
    qr_givens_decomp(m, n, copy_A, m, full_Q, false);
    printf("full_Q = ");
    matrix_flex_print(m, m, full_Q, 7, 4);
    puts("");
    printf("full_R = ");
    matrix_flex_print(m, n, copy_A, 7, 4);

    puts("************ MATRIX B ************");
    matrix_t B[7][7] =
    {
        { 0.8147, 0.5469, 0.8003, 0.0357, 0.6555, 0.8235, 0.7655 },
        { 0.9058, 0.9575, 0.1419, 0.8491, 0.1712, 0.6948, 0.7952 },
        { 0.1270, 0.9649, 0.4218, 0.9340, 0.7060, 0.3171, 0.1869 },
        { 0.9134, 0.1576, 0.9157, 0.6787, 0.0318, 0.9502, 0.4898 },
        { 0.6324, 0.9706, 0.7922, 0.7577, 0.2769, 0.0344, 0.4456 },
        { 0.0975, 0.9572, 0.9595, 0.7431, 0.0462, 0.4387, 0.6463 },
        { 0.2785, 0.4854, 0.6557, 0.3922, 0.0971, 0.3816, 0.7094 }
    };
    m = 7;
    n = 7;
    matrix_t copy_B[m][n];
    puts("+++++++ Reduced QR-form +++++++");
    matrix_copy(m, n, B, copy_B);
    matrix_t red_Q1[m][n];
    qr_givens_decomp(m, n, copy_B, n, red_Q1, true);
    printf("red_Q = ");
    matrix_flex_print(m, n, red_Q1, 7, 4);
    puts("");
    printf("red_R = ");
    matrix_flex_print(n, n, copy_B, 7, 4);
    puts("+++++++ Full QR-form +++++++");
    matrix_copy(m, n, B, copy_B);
    matrix_t full_Q1[m][m];
    qr_givens_decomp(m, n, copy_B, m, full_Q1, false);
    printf("full_Q = ");
    matrix_flex_print(m, m, full_Q1, 7, 4);
    puts("");
    printf("full_R = ");
    matrix_flex_print(m, n, copy_B, 7, 4);

}
