## Pseudo-inverse matrix examples 

### About

These examples show how to use algorithms to compute the pseudo-inverse matrices.
The algorithms implemented are the Moore-Penrose and the QR-based methods.

### How to run

Type `make all` to program your board.

## Note 1

The data type of the matrices can be set in the matrix.h file. The user can choose
between the float or the double data types.  

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.