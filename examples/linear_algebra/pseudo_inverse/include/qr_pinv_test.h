/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the QR-based pseudo-inverse algorithm.
 *
 * @details     QR-based pseudo-inverse algorithm examples (see the
 *              @ref qr_pseudo_inverse.h "QR Pseudo-Inverse" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef QR_PINV_TEST_H_
#define QR_PINV_TEST_H_

/**
 * @brief   Examples of the QR-based pseudo-inverse algorithm.
 *
 */
void qr_pinv_test(void);

#endif /* TESTS_QR_PINV_TEST_H_ */
