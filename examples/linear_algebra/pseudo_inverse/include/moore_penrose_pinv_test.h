/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the Moore--Penrose algorithm.
 *
 * @details     Moore--Penrose algorithm examples (see the @ref moore_penrose_pseudo_inverse.h
 *              "Moore--Penrose" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef MOORE_PENROSE_PINV_TEST_H_
#define MOORE_PENROSE_PINV_TEST_H_

/**
 * @brief   Examples of the Moore--Penrose algorithm.
 *
 */
void moore_penrose_pinv_test(void);

#endif /* MOORE_PENROSE_PINV_TEST_H_ */
