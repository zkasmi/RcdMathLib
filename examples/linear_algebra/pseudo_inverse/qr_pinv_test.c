/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the QR-based pseudo-inverse algorithm.
 *
 * @details     QR-based pseudo-inverse algorithm examples (see the
 *              @ref qr_pseudo_inverse.h "QR Pseudo-Inverse" approach).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <qr_pseudo_inverse.h>
#include <stdio.h>

#include "matrix.h"
#include "qr_common.h"

void qr_pinv_test(void)
{
    puts("\n\n++++++++++++++++++++++++++++++ QR Pseudo-Inverse ++++++++++++++++++++++++++++++\n");
    puts("************ Householder: MATRIX A ************");
    matrix_t A[10][5] = {
        { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
        { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
        { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
        { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
        { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 },
        { 0.0975, 0.1419, 0.7577, 0.8235, 0.4898 },
        { 0.2785, 0.4218, 0.7431, 0.6948, 0.4456 },
        { 0.5469, 0.9157, 0.3922, 0.3171, 0.6463 },
        { 0.9575, 0.7922, 0.6555, 0.9502, 0.7094 },
        { 0.9649, 0.9595, 0.1712, 0.0344, 0.7547 }
    };

    uint8_t m, n;
    m = 10;
    n = 5;
    matrix_t pinv_A[n][m];

    qr_get_pinv(m, n, A, pinv_A, QR_Householder);

    printf("pinv_A = ");
    matrix_flex_print(n, m, pinv_A, 8, 4);
    puts("");

    puts("************ Householder: MATRIX B ************");
    matrix_t B[7][7] =
    {
        { 0.8147, 0.5469, 0.8003, 0.0357, 0.6555, 0.8235, 0.7655 },
        { 0.9058, 0.9575, 0.1419, 0.8491, 0.1712, 0.6948, 0.7952 },
        { 0.1270, 0.9649, 0.4218, 0.9340, 0.7060, 0.3171, 0.1869 },
        { 0.9134, 0.1576, 0.9157, 0.6787, 0.0318, 0.9502, 0.4898 },
        { 0.6324, 0.9706, 0.7922, 0.7577, 0.2769, 0.0344, 0.4456 },
        { 0.0975, 0.9572, 0.9595, 0.7431, 0.0462, 0.4387, 0.6463 },
        { 0.2785, 0.4854, 0.6557, 0.3922, 0.0971, 0.3816, 0.7094 }
    };

    m = 7;
    n = 7;
    matrix_t pinv_B[m][n];

    qr_get_pinv(m, n, B, pinv_B, QR_Householder);

    printf("pinv_B = ");
    matrix_flex_print(n, m, pinv_B, 8, 4);
    puts("");

    puts("************ Givens: MATRIX A ************");

    m = 10;
    n = 5;

    qr_get_pinv(m, n, A, pinv_A, QR_Givens);

    printf("pinv_A = ");
    matrix_flex_print(n, m, pinv_A, 8, 4);
    puts("");

    puts("************ Givens: MATRIX B ************");

    m = 7;
    n = 7;

    qr_get_pinv(m, n, B, pinv_B, QR_Givens);

    printf("pinv_B = ");
    matrix_flex_print(n, m, pinv_B, 8, 4);
    puts("");

}
