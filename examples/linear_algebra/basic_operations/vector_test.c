/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of vector computations.
 *
 * @details     Vector computation examples of the @ref vector.h functions.
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>

#include "vector.h"

void vector_test(void)
{
    puts("############ Test Vector Algebra ###############");
    vector_t a[5] = { 0.0975, 0.2785, 0.5469, 0.9575, 0.9649 };
    vector_t b[5] = { 0.1576, 0.9706, 0.9572, 0.4854, 0.8003 };
    vector_t c[5];
    vector_t d[5] = { 0.1576, 0.9706, 0.9572, 0.4854, 0.8003 };

    uint8_t size = 5;

    vector_add(size, a, b, c);
    printf("c = a + b = ");
    vector_flex_print(size, c, 3, 4);
    puts("");

    vector_sub(size, a, b, c);
    printf("c = a - b = ");
    vector_flex_print(size, c, 3, 4);
    puts("");

    vector_mul(size, a, b, c);
    printf("c = a * b = ");
    vector_flex_print(size, c, 3, 4);
    puts("");

    vector_t mean = vector_get_mean_value(size, a);
    printf("mean(a) = %.4f\n", mean);

    vector_t norm2 = vector_get_norm2(size, a);
    printf("norm2(a) = %.4f\n", norm2);

    vector_t squ_norm2 = vector_get_square_norm2(size, b);
    printf("squ_norm2(b) = %.4f\n", squ_norm2);

    vector_t scal = vector_get_scalar_product(size, a, b);
    printf("scal(a) = %.4f\n", scal);

    vector_t sum = vector_get_sum(size, a);
    printf("sum(a) = %.4f\n", sum);

    vector_t euc_dist = vector_get_euclidean_distance(size, a, b);
    printf("euc_dist(a,b) = %.4f\n", euc_dist);

    if (vector_is_equal(size, b, d)) {
        puts("vectors are equal !!");
    }
    else {
        puts("vectors are not equal !!");
    }

    vector_copy(size, a, d);
    vector_flex_print(size, d, 3, 4);
    puts("");

    vector_clear(size, d);
    vector_flex_print(size, d, 3, 4);
    puts("");

}
