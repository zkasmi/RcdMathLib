/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of matrix computations.
 *
 * @details     Matrix computation examples of the (see @ref matrix.h "matrix" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>

#include "utils.h"
#include "matrix.h"

void matrix_test(void)
{
    puts("############ Basic Matrix Algebra ###############");
    uint8_t m = 3;
    uint8_t n = 4;
    matrix_t diag_elem = M_PI;

    matrix_t matrix1[3][4] = { { 0.2785, 0.9649, 0.9572, 0.1419 },
                               { 0.5469, 0.1576, 0.4854, 0.4218 },
                               { 0.9575, 0.9706, 0.8003, 0.9157 },
                              };

    matrix_t matrix2[3][4] = { { 0.7922, 0.0357, 0.6787, 0.3922 },
                               { 0.9595, 0.8491, 0.7577, 0.6555 },
                               { 0.6557, 0.9340, 0.7431, 0.1712 },
                              };
    matrix_t res_matrix[m][n];
    matrix_t trans_matrix[n][m];
    matrix_t res_mul_matrix[m][m];
    matrix_t diag_vec[3] = { 1.1, 2.3, 3.5 };

    matrix_add(m, n, matrix1, matrix2, res_matrix);
    printf("matrix1 + matrix2 = ");
    matrix_flex_print(m, n, res_matrix, 7, 4);

    matrix_sub(m, n, matrix1, matrix2, res_matrix);
    printf("matrix1 - matrix2 = ");
    matrix_flex_print(m, n, res_matrix, 7, 4);

    matrix_transpose(m, n, matrix2, trans_matrix);
    printf("trans(matrix2) = ");
    matrix_flex_print(n, m, trans_matrix, 7, 4);

    matrix_mul(m, n, matrix1, n, m, trans_matrix, res_mul_matrix);
    printf("matrix1 x matrix2 = ");
    matrix_flex_print(m, m, res_mul_matrix, 7, 4);

    matrix_get_diag_mat(m, n, diag_elem, res_matrix);
    printf("dig_matrix = ");
    matrix_flex_print(m, n, res_matrix, 7, 4);

    matrix_get_diag_mat_new(m, n, res_matrix, m, diag_vec);
    printf("dig_matrix = ");
    matrix_flex_print(m, n, res_matrix, 7, 4);

    matrix_t matrix3[5][5] = {
        { 0.8147, 0.0975, 0.1576, 0.1419, 0.6557 },
        { 0.9058, 0.2785, 0.9706, 0.4218, 0.0357 },
        { 0.1270, 0.5469, 0.9572, 0.9157, 0.8491 },
        { 0.9595, 0.8491, 0.7577, 0.6555, 0.6787 },
        { 0.6557, 0.9340, 0.7431, 0.1712, 0.9595 },
    };

    matrix_swap_rows(5, matrix3, 3, 1);
    printf("swapped matrix3 = ");
    matrix_flex_print(5, 5, matrix3, 7, 4);

    puts(" *** Test Partial Matrix Multiplication *** ");
    matrix_t A[11][7] =
    {
        { 0.8147, 0.9706, 0.8491, 0.0462, 0.1869, 0.4984, 0.5472 },
        { 0.9058, 0.9572, 0.9340, 0.0971, 0.4898, 0.9597, 0.1386 },
        { 0.1270, 0.4854, 0.6787, 0.8235, 0.4456, 0.3404, 0.1493 },
        { 0.9134, 0.8003, 0.7577, 0.6948, 0.6463, 0.5853, 0.2575 },
        { 0.6324, 0.1419, 0.7431, 0.3171, 0.7094, 0.2238, 0.8407 },
        { 0.0975, 0.4218, 0.3922, 0.9502, 0.7547, 0.7513, 0.2543 },
        { 0.2785, 0.9157, 0.6555, 0.0344, 0.2760, 0.2551, 0.8143 },
        { 0.5469, 0.7922, 0.1712, 0.4387, 0.6797, 0.5060, 0.2435 },
        { 0.9575, 0.9595, 0.7060, 0.3816, 0.6551, 0.6991, 0.9293 },
        { 0.9649, 0.6557, 0.0318, 0.7655, 0.1626, 0.8909, 0.3500 },
        { 0.1576, 0.0357, 0.2769, 0.7952, 0.1190, 0.9593, 0.1966 }
    };

    matrix_t B[7][5] = {
        { 0.2511, 0.9172, 0.0540, 0.0119, 0.6020 },
        { 0.6160, 0.2858, 0.5308, 0.3371, 0.2630 },
        { 0.4733, 0.7572, 0.7792, 0.1622, 0.6541 },
        { 0.3517, 0.7537, 0.9340, 0.7943, 0.6892 },
        { 0.8308, 0.3804, 0.1299, 0.3112, 0.7482 },
        { 0.5853, 0.5678, 0.5688, 0.5285, 0.4505 },
        { 0.5497, 0.0759, 0.4694, 0.1656, 0.0838 }
    };

    uint8_t l;
    uint8_t a_row_begin, a_row_end, a_col_begin, a_col_end;
    uint8_t b_row_begin, b_row_end, b_col_begin, b_col_end;
    m = 11;
    n = 7;
    l = 5;
    matrix_t C[5][3];

    a_row_begin = 3, a_row_end = 7;
    a_col_begin = 2, a_col_end = 4;
    b_row_begin = 3, b_row_end = 5;
    b_col_begin = 1, b_col_end = 3;
    matrix_part_mul(n, A, l, B,
                    a_row_begin, a_row_end, a_col_begin, a_col_end,
                    b_row_begin, b_row_end, b_col_begin, b_col_end,
                    3, C);

    printf("part_A = ");
    matrix_flex_part_print(m, n, A, a_row_begin, a_row_end, a_col_begin,
                           a_col_end, 7, 4);
    printf("part_B = ");
    matrix_flex_part_print(n, l, B, b_row_begin, b_row_end, b_col_begin,
                           b_col_end, 7, 4);

    printf("C = ");
    matrix_flex_print(5, 3, C, 7, 4);

    puts(" *** Test The 2-norm of a Matrix, *** ");

    double two_norm = matrix_get_two_norm(m, n, A);
    printf("A_two_norm = %7.4f\n", two_norm);

    matrix_t D[7][5] = {
        { -0.2743, -4.9913, 0.3375, -6.5684, -6.9048 },
        { -4.9657, -2.0716, -4.7133, -2.7536, -4.3030 },
        { -0.4857, -3.2137, -0.9424, -0.7667, -5.7025 },
        { -5.0518, -4.1867, -0.9702, 0.4721, -0.6457 },
        { 0.4341, -0.3534, -3.9564, -5.9608, -4.5103 },
        { -4.2001, -2.3179, -2.4574, -2.4494, -2.7717 },
        { -5.4272, -2.6022, -6.3932, -3.2449, -5.6748 }
    };
    m = 7;
    n = 5;
    two_norm = matrix_get_two_norm(m, n, D);
    printf("D_two_norm = %7.4f\n", two_norm);

    double frob_norm = matrix_get_frob_norm(m, n, D);
    printf("D_frob_norm = %7.4f\n", frob_norm);
}

void inv_triangular_matrices_test(void)
{

    puts(
        "*********** Test the Inverse of Upper Triangular Matrices ***********");
    matrix_t U[11][7] =
    {
        { 0.8147, 0.9706, 0.8491, 0.0462, 0.1869, 0.4984, 0.5472 },
        { 0.0000, 0.9572, 0.9340, 0.0971, 0.4898, 0.9597, 0.1386 },
        { 0.0000, 0.0000, 0.6787, 0.8235, 0.4456, 0.3404, 0.1493 },
        { 0.0000, 0.0000, 0.0000, 0.6948, 0.6463, 0.5853, 0.2575 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.7094, 0.2238, 0.8407 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.7513, 0.2543 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.8143 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 }
    };
    uint8_t m, n;

    puts("Square Matrix:");
    m = 7;
    n = 7;
    printf("U1 = ");
    matrix_flex_print(m, n, U, 7, 4);
    matrix_t inv_U1[n][m];
    matrix_get_inv_upp_triang(m, n, U, inv_U1);
    printf("inv_U1 = ");
    matrix_flex_print(n, m, inv_U1, 7, 4);

    puts("Rectangular Matrix:");
    m = 11;
    n = 7;
    printf("U2 = ");
    matrix_flex_print(m, n, U, 7, 4);
    matrix_t inv_U2[n][m];
    matrix_get_inv_upp_triang(m, n, U, inv_U2);
    printf("inv_U2 = ");
    matrix_flex_print(n, m, inv_U2, 7, 4);

    puts(
        "*********** Test the Inverse of Lower Triangular Matrices ***********");
    matrix_t L[11][7] =
    {
        { 0.8147, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.9058, 0.9572, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.1270, 0.4854, 0.6787, 0.0000, 0.0000, 0.0000, 0.0000 },
        { 0.9134, 0.8003, 0.7577, 0.6948, 0.0000, 0.0000, 0.0000 },
        { 0.6324, 0.1419, 0.7431, 0.3171, 0.7094, 0.0000, 0.0000 },
        { 0.0975, 0.4218, 0.3922, 0.9502, 0.7547, 0.7513, 0.0000 },
        { 0.2785, 0.9157, 0.6555, 0.0344, 0.2760, 0.2551, 0.8143 },
        { 0.5469, 0.7922, 0.1712, 0.4387, 0.6797, 0.5060, 0.2435 },
        { 0.9575, 0.9595, 0.7060, 0.3816, 0.6551, 0.6991, 0.9293 },
        { 0.9649, 0.6557, 0.0318, 0.7655, 0.1626, 0.8909, 0.3500 },
        { 0.1576, 0.0357, 0.2769, 0.7952, 0.1190, 0.9593, 0.1966 }
    };
    m = 7;
    n = 7;
    puts("Square Matrix:");
    printf("L1 = ");
    matrix_flex_print(m, n, L, 7, 4);
    matrix_t inv_L1[n][m];
    matrix_get_inv_low_triang(m, n, L, inv_L1);
    printf("inv_L1 = ");
    matrix_flex_print(n, m, inv_L1, 7, 4);

    puts("Rectangular Matrix:");
    m = 11;
    n = 7;
    printf("L2 = ");
    matrix_flex_print(m, n, L, 7, 4);
    matrix_t inv_L2[n][m];
    matrix_get_inv_low_triang(m, n, L, inv_L2);
    printf("inv_L2 = ");
    matrix_flex_print(n, m, inv_L2, 7, 4);
}

void triangular_matrices_test(void)
{
    matrix_t A[10][5] = {
        { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
        { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
        { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
        { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
        { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 },
        { 0.0975, 0.1419, 0.7577, 0.8235, 0.4898 },
        { 0.2785, 0.4218, 0.7431, 0.6948, 0.4456 },
        { 0.5469, 0.9157, 0.3922, 0.3171, 0.6463 },
        { 0.9575, 0.7922, 0.6555, 0.9502, 0.7094 },
        { 0.9649, 0.9595, 0.1712, 0.0344, 0.7547 }
    };

    uint8_t m = 10, n = 5;
    matrix_t copy_A[m][n];

    matrix_copy(m, n, A, copy_A);
    matrix_t B[m][n];
    matrix_t C[n][m];
    matrix_t D[n][m];

    puts("++++++++++ upper triangular ++++++++++");
    matrix_transpose(m, n, A, C);
    matrix_get_upp_triang(m, n, A, B);
    matrix_get_upp_triang(m, n, A, A);
    matrix_get_upp_triang(n, m, C, D);

    printf("A = ");
    matrix_flex_print(m, n, A, 7, 4);
    printf("B = ");
    matrix_flex_print(m, n, B, 7, 4);
    printf("C = ");
    matrix_flex_print(n, m, C, 7, 4);
    printf("D = ");
    matrix_flex_print(n, m, D, 7, 4);

    puts("++++++++++ lower triangular ++++++++++");
    matrix_copy(m, n, copy_A, A);
    printf("A = ");
    matrix_flex_print(m, n, A, 7, 4);
    matrix_transpose(m, n, A, C);
    matrix_get_low_triang(m, n, A, B);
    matrix_get_low_triang(m, n, A, A);
    matrix_get_low_triang(n, m, C, D);

    printf("A = ");
    matrix_flex_print(m, n, A, 7, 4);
    printf("B = ");
    matrix_flex_print(m, n, B, 7, 4);
    printf("C = ");
    matrix_flex_print(n, m, C, 7, 4);
    printf("D = ");
    matrix_flex_print(n, m, D, 7, 4);

}
