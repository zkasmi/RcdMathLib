/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of vector computations.
 *
 * @details     Vector computation examples (see @ref vector.h "vector" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef VECTOR_TEST_H_
#define VECTOR_TEST_H_

/**
 * @brief   Examples of vector operations.
 *
 */
void vector_test(void);

#endif /* VECTOR_TEST_H_ */
