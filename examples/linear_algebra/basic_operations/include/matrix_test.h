/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of matrix computations.
 *
 * @details     Matrix computation examples (see @ref matrix.h "matrix" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef MATRIX_TEST_H_
#define MATRIX_TEST_H_

/**
 * @brief   Test some matrix operations.
 *
 */
void matrix_test(void);

/**
 * @brief   Examples of the inverse of triangular matrices.
 *
 */
void inv_triangular_matrices_test(void);

/**
 * @brief   Examples of triangular matrices.
 *
 */
void triangular_matrices_test(void);

#endif /* MATRIX_TEST_H_ */
