## Basic matrix operations examples

### About

This example shows how to use the basic matrix operations such as the addition 
or the multiplication.

### How to run

Type `make all` to program your board.

## Note 1

The data type of the matrices can be set in the matrix.h file. The user can choose
between the float or the double data types.  

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.