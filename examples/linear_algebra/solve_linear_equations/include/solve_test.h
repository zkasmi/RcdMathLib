/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of solving linear equation systems.
 *
 * @details     Solving linear equation systems examples (see @ref solve.h "solve" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef SOLVE_TEST_H_
#define SOLVE_TEST_H_

/**
 * @brief   Examples of solving linear equation systems.
 *
 */
void solve_test(void);

/**
 * @brief   Example of solving an (10,5) linear equation system.
 *
 */
void solve_big_matrix_test(void);

#endif /* SOLVE_TEST_H_ */
