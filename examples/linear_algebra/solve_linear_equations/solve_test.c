/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of solving linear equation systems.
 *
 * @details     Solving linear equation systems examples (see @ref solve.h "solve" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>

#include "solve.h"
#include "matrix.h"
#include "vector.h"

void solve_test(void)
{
    int8_t error_no;

    puts("############ Solve Linear Algebra Module ############");

    puts("******* Determined system *******");
    uint8_t m = 3;
    uint8_t n = m;
    enum ALGORITHM algo = Moore_Penrose;
    matrix_t A1[3][3] = { { 2, 1, 1 },
                          { -1, 1, -1 },
                          { 1, 2, 3 } };
    matrix_t copy_A1[m][n];
    matrix_t b1[3] = { 2, 3, -10 };
    matrix_t x[3];

    printf("A1 = ");
    matrix_flex_print(m, n, A1, 3, 1);

    puts("Moore Penrose: ");
    matrix_copy(m, n, A1, copy_A1);
    error_no = solve(m, n, copy_A1, b1, x, algo);
    puts("");

    if (error_no > 0) {
        printf("x1 = ");
        vector_flex_print(n, x, 3, 1);
        puts("");
    }

    puts("Householder: ");
    algo = Householder;
    matrix_copy(m, n, A1, copy_A1);
    error_no = solve(m, n, copy_A1, b1, x, algo);

    if (error_no > 0) {
        puts("");
        printf("x1 = ");
        vector_flex_print(n, x, 3, 1);
        puts("");
    }

    puts("Givens: ");
    algo = Givens;
    matrix_copy(m, n, A1, copy_A1);
    error_no = solve(m, n, copy_A1, b1, x, algo);

    if (error_no > 0) {
        puts("");
        printf("x1 = ");
        vector_flex_print(n, x, 3, 1);
        puts("");
    }

    puts("Gauss: ");
    algo = Gauss;
    matrix_copy(m, n, A1, copy_A1);
    error_no = solve(m, n, copy_A1, b1, x, algo);

    if (error_no > 0) {
        puts("");
        printf("x1 = ");
        vector_flex_print(n, x, 3, 1);
        puts("");
    }

    puts("******* Overdetermined system *******");
    m = 4;
    n = 3;
    matrix_t A2[4][3] = { { 25, 5, 1 },
                          { 64, 8, 1 },
                          { 100, 10, 1 },
                          { 144, 12, 1 }, };
    matrix_t copy_A2[m][n];
    matrix_t b2[4] = { 106.8, 177.2, 232.6, 279.2 };
    printf("A2 = ");
    matrix_flex_print(m, n, A2, 3, 1);

    puts("Moore Penrose: ");
    algo = Moore_Penrose;
    matrix_copy(m, n, A2, copy_A2);
    error_no = solve(m, n, copy_A2, b2, x, algo);

    if (error_no > 0) {
        printf("x2 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Householder: ");
    algo = Householder;
    matrix_copy(m, n, A2, copy_A2);
    error_no = solve(m, n, copy_A2, b2, x, algo);

    if (error_no > 0) {
        printf("x2 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Givens: ");
    algo = Givens;
    matrix_copy(m, n, A2, copy_A2);
    error_no = solve(m, n, copy_A2, b2, x, algo);

    if (error_no > 0) {
        printf("x2 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("******* Underdetermined system *******");
    m = 3;
    n = 4;
    matrix_t A3[3][4] = { { 2, -4, 2, -14 },
                          { -1, 2, -2, 11 },
                          { -1, 2, -1, 7 } };
    matrix_t copy_A3[m][n];
    matrix_t b3[3] = { 10, -6, -5 };
    printf("A3 = ");
    matrix_flex_print(m, n, A3, 3, 1);

    puts("Moore Penrose: ");
    algo = Moore_Penrose;
    matrix_copy(m, n, A3, copy_A3);
    error_no = solve(m, n, A3, b3, x, algo);

    if (error_no > 0) {
        printf("x3 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Householder: ");
    algo = Householder;
    matrix_copy(m, n, A3, copy_A3);
    error_no = solve(m, n, copy_A3, b3, x, algo);

    if (error_no > 0) {
        printf("x3 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Givens: ");
    algo = Givens;
    matrix_copy(m, n, A3, copy_A3);
    error_no = solve(m, n, copy_A3, b3, x, algo);

    if (error_no > 0) {
        printf("x3 = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("*** Test: Solve with LU decomposition ***");
    algo = Gauss;
    matrix_t A4[5][5] = {
        { 0.1712, 0.0971, 0.0344, 0.1869, 0.7547 },
        { 0.7060, 0.8235, 0.4387, 0.4898, 0.2760 },
        { 0.0318, 0.6948, 0.3816, 0.4456, 0.6797 },
        { 0.2769, 0.3171, 0.7655, 0.6463, 0.6551 },
        { 0.0462, 0.9502, 0.7952, 0.7094, 0.1626 }
    };
    matrix_t x4[5];
    matrix_t b4[5] = { 0.1190, 0.4984, 0.9597, 0.3404, 0.5853 };

    m = 5;
    n = 5;
    matrix_t copy_A4[m][m];
    matrix_copy(m, n, A4, copy_A4);
    error_no = solve(m, n, copy_A4, b4, x4, algo);

    if (error_no > 0) {
        printf("x4 = ");
        vector_flex_print(n, x4, 3, 4);
        puts("");
    }

    matrix_t dot_b4[5];
    matrix_mul_vec(m, m, A4, x4, dot_b4);
    printf("dot_b4 = ");
    vector_flex_print(n, dot_b4, 3, 4);
    puts("");
}

void solve_big_matrix_test(void)
{
    matrix_t A[10][5] = { { 0.8147, 0.1576, 0.6557, 0.7060, 0.4387 },
                          { 0.9058, 0.9706, 0.0357, 0.0318, 0.3816 },
                          { 0.1270, 0.9572, 0.8491, 0.2769, 0.7655 },
                          { 0.9134, 0.4854, 0.9340, 0.0462, 0.7952 },
                          { 0.6324, 0.8003, 0.6787, 0.0971, 0.1869 },
                          { 0.0975, 0.1419, 0.7577, 0.8235, 0.4898 },
                          { 0.2785, 0.4218, 0.7431, 0.6948, 0.4456 },
                          { 0.5469, 0.9157, 0.3922, 0.3171, 0.6463 },
                          { 0.9575, 0.7922, 0.6555, 0.9502, 0.7094 },
                          { 0.9649, 0.9595, 0.1712, 0.0344, 0.7547 } };
    uint8_t m = 10, n = 5;
    int8_t error_no;
    enum ALGORITHM algo;
    matrix_t copy_A[m][n];
    matrix_t b[10] = { 1.2902, 0.8819, 0.9721, 1.2347, 0.9185, 0.9844, 1.0627,
                       1.0280, 1.7283, 1.0618 };
    matrix_t x[n];

    printf("A = ");
    matrix_flex_print(m, n, A, 3, 4);

    puts("Moore Penrose: ");
    algo = Moore_Penrose;
    matrix_copy(m, n, A, copy_A);
    error_no = solve(m, n, copy_A, b, x, algo);
    puts("");

    if (error_no > 0) {
        printf("x = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Householder: ");
    algo = Householder;
    matrix_copy(m, n, A, copy_A);
    error_no = solve(m, n, copy_A, b, x, algo);

    if (error_no > 0) {
        puts("");
        printf("x = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }

    puts("Givens: ");
    algo = Givens;
    matrix_copy(m, n, A, copy_A);
    error_no = solve(m, n, copy_A, b, x, algo);

    if (error_no > 0) {
        puts("");
        printf("x = ");
        vector_flex_print(n, x, 3, 4);
        puts("");
    }
}
