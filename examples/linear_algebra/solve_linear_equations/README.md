## Solving linear equations examples 

### About

These examples show how to use algorithms to solve linear equation systems.
The algorithms implemented to solve linear equations are based on the Moore-penrose, 
Householder, Givens and Gaussian elimination with pivoting methods. 

### How to run

Type `make all` to program your board.

## Note 1

The data type used can be set in the matrix.h and vector.h files. The user 
can choose between the float or the double data types. We recommend to set 
the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application. 

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.

