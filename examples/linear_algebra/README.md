## Linear algebra examples 

### About

These examples show how to use the RcdMathLib's linear algebra module composed 
of the following sub-modules: basic_operations, matrix_decompositions, pseudo_inverse, 
solve_linear_equations, and utilities sub-modules.

## Note 1

The data type used can be set in the matrix.h and vector.h files. The user 
can choose between the float or the double data types. We recommend to set 
the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application. 

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.

