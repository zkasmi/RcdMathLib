## Utilities examples 

### About

These examples show how to use the utilities of the RcdMathLib.
The utilities module provide various functions needed by the linear algebra-module 
as well as other module such as methods to compute moving average or the save 
square root.

### How to run

Type `make all` to program your board. 

## Note 1

The data type used can be set in the matrix.h and vector.h files. The user 
can choose between the float or the double data types. We recommend to set 
the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application. 

## Note 2

Set the same data type in the the matrix.h and vector.h files to avoid a data 
type conflict in the application.

