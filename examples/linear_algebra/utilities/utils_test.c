/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of the utility functions.
 *
 * @details     Utility examples (see @ref utils.h "utilities" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>

#include "utils.h"
#include "vector.h"

void utils_test(void)
{
    puts("############ Test Utils ###############");
    double deg_ang = 45;
    double rad_ang = utils_to_radian(deg_ang);
    printf("rad_ang = %.4f\n", rad_ang);

    float f1 = 4.78;
    float f2 = 11.77;
    float f_max = (float)utils_max(f1, f2);
    printf("f_max = %.4f\n", f_max);

    float f_min = (float)utils_min(f1, f2);
    printf("f_min = %.4f\n", f_min);

    uint8_t a1 = 9;
    uint8_t b1 = 13;
    utils_swap(&a1, &b1);
    printf("a1 = %u, b1 = %u\n", a1, b1);

    printf("sind(45) = %.4f\n", utils_sind(deg_ang));

    vector_t arr[10] = { 4, 8, 6, -1, -2, -3, -1, 3, 4, 5 };
    vector_t mv_aver_arr[10];
    uint8_t win_size = 5;
    double mean = 0.0;

    printf("arr = ");
    vector_flex_print(10, arr, 3, 0);
    puts("");
    printf("win_size = %u\n", win_size);
    utils_moving_average(10, arr, win_size, mv_aver_arr);
    printf("mv_av_arr = ");
    vector_flex_print(10, mv_aver_arr, 3, 4);
    puts("");
    mean = utils_mean(10, arr);
    printf("mean = %3.4f\n", mean);

    puts("!!! Median algorithm !!!");
    vector_t arr1[9] = { 100, 91, 92, 85, 40, 77, 92, 91, 94 };
    vector_t median_val = 0.0;

    printf("arr1 = ");
    vector_flex_print(9, arr1, 3, 0);
    puts("");
    median_val = utils_get_median(arr1, 9);
    printf("median1 = %3.4f\n", median_val);

    vector_t arr2[13] = { 0.4854, 0.8003, 0.1419, 0.4218, 0.9157, 0.7922,
                          0.9595, 0.6557, 0.0357, 0.8491, 0.9340, 0.6787,
                          0.7577 };
    printf("arr2 = ");
    vector_flex_print(13, arr2, 3, 4);
    puts("");
    median_val = utils_get_median(arr2, 13);
    printf("median2 = %3.4f\n", median_val);

    vector_t arr3[12] = { 0.9502, 0.0344, -0.4387, 0.3816, 0.7655, 0.7952,
                          0.1869, 0.4898, 0.4456, 0.6463, -0.7094, 0.7547 };
    printf("arr3 = ");
    vector_flex_print(12, arr3, 3, 4);
    puts("");
    median_val = utils_get_median(arr3, 12);
    printf("median3 = %3.4f\n", median_val);
}
