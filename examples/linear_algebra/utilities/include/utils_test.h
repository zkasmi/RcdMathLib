/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of the utility functions.
 *
 * @details     Utility examples (see @ref utils.h "utilities" functions).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef UTILS_TEST_H_
#define UTILS_TEST_H_

/**
 * @brief   Examples of the utility functions.
 *
 */
void utils_test(void);

#endif /* UTILS_TEST_H_*/
