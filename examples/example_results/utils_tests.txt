******************* Testing the MicroPosMath-Lib!! 09/18/19 22:26:21 *******************

########### Test: utiles ###########
arr = {  4,   8,   6,  -1,  -2,  -3,  -1,   3,   4,   5}
win_size = 5
mv_av_arr = {0.8000, 2.4000, 3.6000, 3.4000, 3.0000, 1.6000, -0.2000, -0.8000, 0.2000, 1.6000}
mean = 2.3000
!!! Median algorithm !!!
arr1 = {100,  91,  92,  85,  40,  77,  92,  91,  94}
median1 = 91.0000
arr2 = {0.4854, 0.8003, 0.1419, 0.4218, 0.9157, 0.7922, 0.9595, 0.6557, 0.0357, 0.8491, 0.9340, 0.6787, 0.7577}
median2 = 0.7577
arr3 = {0.9502, 0.0344, -0.4387, 0.3816, 0.7655, 0.7952, 0.1869, 0.4898, 0.4456, 0.6463, -0.7094, 0.7547}
median3 = 0.4677
