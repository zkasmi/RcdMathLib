/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of common algorithms of localization systems.
 *
 * @details     Common algorithms examples of positioning systems (see the
 *              @ref pos_algos_common "positioning algorithms common" module).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#include <stdio.h>

#include "DOP.h"
#include "matrix.h"
#include "trilateration.h"

void pos_algos_common_test(void)
{
    matrix_t ref_pos_matrix[4][3] = { { 0, 0, 1.67 },       // P1
                                      { 4.5, 0, 0.75 },     // P2
                                      { 4.5, 4.45, 0.75 },  // P3
                                      { 0, 4.92, 0.86 }     // P4
                                     };

    uint8_t m = 4;
    double A_matrix[m][4];
    double true_pos[] = { 1, 1, 0 };

    double pdop_val = get_PDOP(m, ref_pos_matrix, true_pos);

    printf("pdop_val = %.7f\n", pdop_val);

    trilateration_get_A_matrix(m, ref_pos_matrix, A_matrix);
    printf("A_matrix = ");
    matrix_flex_print(m, 4, A_matrix, 3, 7);
    puts("");
}
