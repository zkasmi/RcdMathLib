/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of common algorithms of localization systems.
 *
 * @details     Common algorithms examples of positioning systems (see the
 *              @ref pos_algos_common "positioning algorithms common" module).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *
 * @}
 */

#ifndef POS_ALGOS_COMMON_TEST_H_
#define POS_ALGOS_COMMON_TEST_H_

/**
 * @brief   Example of common localization algorithms.
 *
 */
void pos_algos_common_test(void);

#endif /* POS_ALGOS_COMMON_TEST_H_ */
