/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of localization algorithms of distance-based positioning systems.
 *
 * @details     Localization algorithms examples using distance measures (see the
 *              @ref dist_based_position.h "methods of distance-based" localization systems).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <stdio.h>

#include "trilateration.h"
#include "matrix.h"
#include "vector.h"
#include "dist_based_position.h"

void distance_based_test(void)
{
    puts("***************** Distance-based system example *****************");
    matrix_t ref_pos_matrix[4][3] = { { 0, 0, 1.67 },       // P1
                                      { 4.5, 0, 0.75 },     // P2
                                      { 4.5, 4.45, 0.75 },  // P3
                                      { 0, 4.92, 0.86 }     // P4
                                     };

    matrix_t true_pos[3] = { 1.0000, 3.0000, 0.0000 }; // is usually unknown
    /* measured distances to the reference stations */
    matrix_t measured_dist_arr[4] = { 3.577157155, 4.685985423, 3.883094303,
                                      2.320377599 };
    /* estimated position */
    matrix_t est_pos[4];
    uint8_t ref_point_num = 4;
    trilateration2(ref_point_num, ref_pos_matrix, measured_dist_arr,
                   est_pos, NULL);

    printf("true position = ");
    vector_flex_print(3, true_pos, 5, 4);
    puts("");

    printf("estimated position = ");
    vector_flex_print(3, &est_pos[1], 5, 7);
    puts("");
}
