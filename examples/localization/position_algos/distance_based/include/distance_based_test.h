/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of localization algorithms of distance-based positioning systems.
 *
 * @details     Localization algorithms examples using distance measures (see the
 *              @ref dist_based_position.h "methods of distance-based" localization systems).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef DISTANCE_BASED_TEST_H_
#define DISTANCE_BASED_TEST_H_

/**
 * @brief    Example of a distance-based localization system.
 * @details  This example shows how to use the trilateration algorithm, whereby an estimated
 *           position is calculated based on the distance measurements.
 */
void distance_based_test(void);

#endif /* DISTANCE_BASED_TEST_H_ */
