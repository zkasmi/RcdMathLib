/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of localization algorithms of magnetic-based positioning systems.
 *
 * @details     Localization algorithms examples using artificially generated DC-pulsed, magnetic
 *              signals (see the @ref magnetic_based_position.h "methods of magnetic-based"
 *              localization systems).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <stdio.h>

#include "matrix.h"
#include "vector.h"
#include "utils.h"
#include "trilateration.h"
#include "magnetic_based_position.h"

void magnetic_based_test(void)
{
    puts("***************** Magnetic-based system example *****************");
    matrix_t ref_pos_matrix[4][3] = {
        { 100.395, 16.781, 1.084 },     //P1
        { 100.937, 22.941, 1.072 },     //P2
        { 106.229, 22.716, 1.064 },     //P3
        { 106.3, 16.8, 2.35 }           //P4
    };

    matrix_t true_pos[3] = { 102, 18, 2.308 }; // is usually unknown // Sensor8a3

    /* magnetic field strengths to the reference stations */
    matrix_t B_vec[4] = { 43.2630, 3.0620, 1.4890, 4.5361 };
    /* Elevation angles to the reference stations */
    matrix_t theta_vec[4] = { 31.3710, 12.4870, 11.7280, 0.5352 };
    uint8_t ref_point_num = 4;
    matrix_t r0_vec[ref_point_num];

    for (uint8_t i = 0; i < ref_point_num; i++) {

        r0_vec[i] = magnetic_based_get_r(B_vec[i],
                                         utils_to_radian(theta_vec[i]), K_T);

    }

    /* estimated position */
    matrix_t est_pos[4];
    trilateration2(ref_point_num, ref_pos_matrix, r0_vec,
                   est_pos, NULL);

    printf("true position = ");
    vector_flex_print(3, true_pos, 5, 4);
    puts("");

    printf("estimated position = ");
    vector_flex_print(3, &est_pos[1], 5, 7);
    puts("");

}
