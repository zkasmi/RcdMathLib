/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Example of the algorithm for the recognition and mitigation of multipath effects.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <float.h>
#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>

#include "multipath_dist_detection_mitigation.h"
#include "loc_levenberg_marquardt.h"
#include "norm_dist_rnd_generator.h"
#include "dist_based_jacobian.h"
#include "dist_based_fi.h"
#include "shell_sort.h"
#include "vector.h"
#include "DOP.h"

void multipath_algo_own_norm_distr_test(void)
{
    //to get the console output in the debugging mode.
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    uint8_t n = 8;      //length of alphabet
    uint8_t k = 4;      //length of figures

    matrix_t ref_matrix[][3] = { { 1, 1, 1 },
                                 { 20, 1, 1 },
                                 { 20, 20, 1 },
                                 { 1, 20, 1 },
                                 { 1, 1, 20 },
                                 { 20, 1, 20 },
                                 { 20, 20, 20 },
                                 { 1, 20, 20 } };

    matrix_t opt_noised_r_vec[k];

    matrix_t sol_x[3];
    matrix_t optimal_start_pos[3];
    matrix_t tmp_start_optimal[3];
    uint8_t optimal_anchor_combi_arr[k];
    uint8_t PDOP_Threshold = 8;
    uint8_t maxit = 100;
    uint8_t it = 0;
    matrix_t tau = 1e-6;
    matrix_t eps = 1e-5;
    matrix_t beta0 = 0.2;
    matrix_t beta1 = 0.8;
    uint8_t d_max = 19;
    uint8_t grid_cell_length = 2;
    uint8_t axis_point_num = d_max / grid_cell_length;
    uint32_t exact_point[3];
    //matrix_t multipath[] = {5,3};
    //matrix_t multipath[] = {4,3}; // for 4 anchors
    matrix_t multipath[] = { 3 }; // for 4 anchors
    uint8_t anchor_num = n;
    uint32_t room_point_num;
    matrix_t noised_r_vec[anchor_num];
    uint32_t counter = 0;
    matrix_t mean_lvm_call_num = 0;
    matrix_t sigma = 0.1;
    int32_t initial_seed_val = 3;
    int32_t seed = 0;

    /************************************************/

    puts("################ START ################");

    // initialize seed's value
    seed = initial_seed_val;

    //Sampling the room
    for (uint32_t z = 0; z <= axis_point_num; z++) {
        exact_point[2] = ref_matrix[0][2] + grid_cell_length * z;

        for (uint32_t y = 0; y <= axis_point_num; y++) {
            exact_point[1] = ref_matrix[0][1]
                             + grid_cell_length * y;

            for (uint32_t x = 0; x <= axis_point_num; x++) {
                exact_point[0] = ref_matrix[0][0]
                                 + grid_cell_length * x;

                if (!is_anchor(anchor_num, ref_matrix,
                               exact_point)) {
                    printf("seed = %ld\n", (long) seed);
                    sim_UWB_dist(n, ref_matrix, exact_point,
                                 sigma, 2, multipath,
                                 seed, noised_r_vec);
                    seed = 2 * initial_seed_val + 1;
                    initial_seed_val++;

                    recog_mitigate_multipath(k, n,
                                             ref_matrix,
                                             noised_r_vec,
                                             optimal_anchor_combi_arr,
                                             optimal_start_pos);

                    // PDOP & Levenberg
                    matrix_t opt_ref_matrix_comb_matrix[k][3];

                    get_optimal_partial_ref_matrix(n,
                                                   ref_matrix, k,
                                                   optimal_anchor_combi_arr,
                                                   opt_ref_matrix_comb_matrix);
                    matrix_t PDOP =
                        get_PDOP(k,
                                 opt_ref_matrix_comb_matrix,
                                 optimal_start_pos);

                    get_optimal_partial_r_noised_vec(k,
                                                     noised_r_vec,
                                                     optimal_anchor_combi_arr,
                                                     opt_noised_r_vec);

                    if (PDOP > PDOP_Threshold) {
                        vector_get_elements(
                            noised_r_vec, k,
                            optimal_anchor_combi_arr,
                            opt_noised_r_vec);
                        vector_copy(3,
                                    optimal_start_pos,
                                    tmp_start_optimal);
                        it =
                            loc_levenberg_marquardt(
                                3,
                                opt_ref_matrix_comb_matrix,
                                optimal_start_pos,
                                opt_noised_r_vec,
                                eps,
                                tau,
                                beta0,
                                beta1,
                                maxit,
                                sol_x,
                                dist_based_f_i,
                                dist_based_jacobian_get_JTJ,
                                dist_based_jacobian_get_JTf,
                                dist_based_jacobian_get_J_mul_s);
                        mean_lvm_call_num++;

                    }
                    else {
                        it = 0;
                        vector_copy(3,
                                    optimal_start_pos,
                                    sol_x);
                    }

                    puts(
                        "------------------------------------------------------------");
                    printf("PDOP = %f | it = %d\n", PDOP,
                           it);

                    printf("r_noised_vec = ");
                    //matrix_print_array(r_noised_vec, n, 7);
                    vector_flex_print(n, noised_r_vec, 5,
                                      7);
                    puts("");
                    printf(
                        "anchors_optimal = {%u, %u, %u, %u}\n",
                        optimal_anchor_combi_arr[0],
                        optimal_anchor_combi_arr[1],
                        optimal_anchor_combi_arr[2],
                        optimal_anchor_combi_arr[3]);
                    printf(
                        "exact point = {%lu, %lu, %lu}\n",
                        (unsigned long)exact_point[0],
                        (unsigned long)exact_point[1],
                        (unsigned long)exact_point[2]);
                    if (it != 0) {
                        printf(
                            "start_optimal = {%f, %f, %f}\n",
                            tmp_start_optimal[0],
                            tmp_start_optimal[1],
                            tmp_start_optimal[2]);
                    }

                    printf("x_sol = {%.7f, %.7f, %.7f}\n",
                           sol_x[0],
                           sol_x[1],
                           sol_x[2]);
                    counter++;
                }//x
            }//if
        }//y
    }//Z

    room_point_num = counter;
    puts(
        "----------------------------- End Simulation -------------------------------");
    printf("Counter = %lu\n", (unsigned long) counter);
    printf("room point num = %lu\n", (unsigned long) room_point_num);
    printf("anchors num = %u\n", n);
    printf("LVM call number = %f\n", mean_lvm_call_num);
    printf("mean LVM call number = %f\n",
           mean_lvm_call_num / room_point_num);
}
