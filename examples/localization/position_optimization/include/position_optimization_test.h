/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Examples of optimization algorithms for localization systems.
 *
 * @details     Optimization algorithms examples for localization systems. (see the @ref
 *              loc_gauss_newton.h "modified GN" and @ref loc_levenberg_marquardt.h "LVM"
 *              optimization methods).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef POSITION_OPTIMIZATION_TEST_H_
#define POSITION_OPTIMIZATION_TEST_H_

/**
 * @brief   Examples of optimization algorithms of a localization system.
 *
 */
void position_optimization_test(void);

#endif /* POSITION_OPTIMIZATION_TEST_H_ */
