/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Example of the algorithm for the recognition and mitigation of multipath effects.
 *
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#ifndef MULTIPATH_ALGO_OWN_NORM_DISTR_TEST_H_
#define MULTIPATH_ALGO_OWN_NORM_DISTR_TEST_H_

/**
 * @brief   Example of the algorithm for the recognition and mitigation of multipath effects.
 *
 */
void multipath_algo_own_norm_distr_test(void);

#endif /* MULTIPATH_ALGO_OWN_NORM_DISTR_TEST_H_ */
