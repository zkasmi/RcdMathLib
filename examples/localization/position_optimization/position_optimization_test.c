/*
 * Copyright (C) 2020 Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 *               2020 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
 */

/**
 * @{
 *
 * @file
 * @brief       Examples of optimization algorithms for localization systems.
 *
 * @details     Optimization algorithms examples for localization systems. (see the
 *              @ref loc_gauss_newton.h "modified GN" and @ref loc_levenberg_marquardt.h "LVM"
 *              optimization methods).
 *
 * @author      Zakaria Kasmi <zkasmi@inf.fu-berlin.de>
 * @author      Naouar Guerchali
 * @author      Abdelmoumen Norrdine <a.norrdine@googlemail.com>
 *
 * @}
 */

#include <stdio.h>
#include <stdint.h>

#include "loc_levenberg_marquardt.h"
#include "loc_gauss_newton.h"
#include "dist_based_fi.h"
#include "dist_based_position.h"
#include "dist_based_jacobian.h"
#include "vector.h"

void position_optimization_test(void)
{
    matrix_t ref_pos_matrix[4][3] = { { 0, 0, 1.67 },       // P1
                                      { 4.5, 0, 0.75 },     // P2
                                      { 4.5, 4.45, 0.75 },  // P3
                                      { 0, 4.92, 0.86 }     // P4
    };

    uint8_t ref_num = 4;
    double true_pos[] = { 1.0000, 1.0000, 0.0000 };
    double measured_dist_arr[] = { 2.1893555, 3.7321172, 4.9925183, 4.1269400 };
    vector_t start_pos[] = { 0.9645166, 0.9894337, -0.1127879 };
    double est_pos[3];

    uint8_t iter_num;
    matrix_t fmin = 1e-11;
    matrix_t eps = 1e-3;
    matrix_t beta0 = 0.2;
    matrix_t beta1 = 0.8;
    matrix_t tau = 1e-6;
    uint8_t max_iter_num = 100;

    iter_num = loc_gauss_newton(ref_num, ref_pos_matrix, start_pos,
                                measured_dist_arr, eps, fmin, max_iter_num, est_pos,
                                &dist_based_f_i, &dist_based_jacobian_get_JTJ,
                                &dist_based_jacobian_get_JTf);
    printf("True position = ");
    vector_flex_print(3, true_pos, 3, 4);
    puts("");
    printf("Optimized position with Gauss-Newton algorithm = ");
    vector_flex_print(3, est_pos, 5, 7);
    puts("");
    printf("iteration number = %u\n", iter_num);

    vector_clear((uint8_t) 3, est_pos);

    iter_num = loc_levenberg_marquardt(ref_num, ref_pos_matrix, start_pos,
                                       measured_dist_arr, eps, tau, beta0, beta1, max_iter_num,
                                       est_pos,
                                       &dist_based_f_i, &dist_based_jacobian_get_JTJ,
                                       &dist_based_jacobian_get_JTf,
                                       &dist_based_jacobian_get_J_mul_s);
    printf("Optimized position with Levenberg-Marquardt algorithm = ");
    vector_flex_print(3, est_pos, 5, 7);
    puts("");
    printf("iteration number = %u\n", iter_num);

}
