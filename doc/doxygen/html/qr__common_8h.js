var qr__common_8h =
[
    [ "QR_ALGORITHM", "qr__common_8h.html#ac62c54358ecc0c35cdef1c3fb37cc73b", [
      [ "QR_Householder", "qr__common_8h.html#ac62c54358ecc0c35cdef1c3fb37cc73bad33bdadaec01995a7dee94f202c598e3", null ],
      [ "QR_Givens", "qr__common_8h.html#ac62c54358ecc0c35cdef1c3fb37cc73bac5a89d2d2ca110939bfab32732fc0f1a", null ]
    ] ],
    [ "qr_common_backward_subst", "qr__common_8h.html#adad26253c0e08a0d9fe2dfe0f6d64112", null ],
    [ "qr_common_get_reduced_QR", "qr__common_8h.html#a3144f99da14a69ec80bf9ded23714612", null ]
];