var magnetic__based__position_8c =
[
    [ "magnetic_based_get_absolute_error", "magnetic__based__position_8c.html#ad5a7513337486e277ea028480403569d", null ],
    [ "magnetic_based_get_distances", "magnetic__based__position_8c.html#ae584dff239a0111c47b7c5d1e197af59", null ],
    [ "magnetic_based_get_distances_to_anchors", "magnetic__based__position_8c.html#ac852e4c0dcb8e9901aa3fab5efd91599", null ],
    [ "magnetic_based_get_magnetic_field", "magnetic__based__position_8c.html#a361f3f3c4f9516a1aa6107367ebfda8d", null ],
    [ "magnetic_based_get_magnetic_field_vec", "magnetic__based__position_8c.html#aac026f28838a5b30c8f6f189e3ab354c", null ],
    [ "magnetic_based_get_r", "magnetic__based__position_8c.html#a01bb024303e8eb202a19dbd70b291c06", null ],
    [ "magnetic_based_preprocessing_get_position", "magnetic__based__position_8c.html#af316f67cb87759b57bc815326b8d1cef", null ]
];