var searchData=
[
  ['rcdmathlib_20documentation_248',['RcdMathLib Documentation',['../index.html',1,'']]],
  ['r0_249',['R0',['../magnetic__based__position_8h.html#a30ab9da60435727bc80839d416461b8f',1,'magnetic_based_position.h']]],
  ['recog_5fmitigate_5fmultipath_250',['recog_mitigate_multipath',['../multipath__dist__detection__mitigation_8h.html#a2840a12d90b5b26b59d50f449f754fbd',1,'recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_Matrix[m][3], matrix_t r_noised_vec[m], uint8_t anchors_optimal[k], matrix_t start_optimal[3]):&#160;multipath_dist_detection_mitigation.c'],['../multipath__dist__detection__mitigation_8c.html#a8cce1ff9263404ca1e58d67910f38d47',1,'recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_matrix[m][3], matrix_t noised_r_vec[m], uint8_t anchors_optimal_combi[k], matrix_t start_optimal_pos[3]):&#160;multipath_dist_detection_mitigation.c']]],
  ['row_5fnum_251',['row_num',['../structmatrix__dim__t.html#a853244a0562d43bfdcdcb704ed707721',1,'matrix_dim_t']]]
];
