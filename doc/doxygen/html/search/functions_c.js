var searchData=
[
  ['recog_5fmitigate_5fmultipath_566',['recog_mitigate_multipath',['../multipath__dist__detection__mitigation_8h.html#a2840a12d90b5b26b59d50f449f754fbd',1,'recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_Matrix[m][3], matrix_t r_noised_vec[m], uint8_t anchors_optimal[k], matrix_t start_optimal[3]):&#160;multipath_dist_detection_mitigation.c'],['../multipath__dist__detection__mitigation_8c.html#a8cce1ff9263404ca1e58d67910f38d47',1,'recog_mitigate_multipath(uint8_t k, uint8_t m, matrix_t ref_matrix[m][3], matrix_t noised_r_vec[m], uint8_t anchors_optimal_combi[k], matrix_t start_optimal_pos[3]):&#160;multipath_dist_detection_mitigation.c']]]
];
