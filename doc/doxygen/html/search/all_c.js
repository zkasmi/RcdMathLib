var searchData=
[
  ['newton_5fraphson_195',['Newton_Raphson',['../fsolve_8h.html#a806eab83c4b4a937b86ebcfd984f9206a44c226e2406536a0eba3ee940a4a7117',1,'Newton_Raphson():&#160;fsolve.h'],['../newton__raphson_8h.html#ac6837f695990867bd72cf23ee93961cc',1,'newton_raphson(uint8_t f_length, uint8_t n, vector_t x0_arr[], double eps, uint8_t max_it_num, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][n])):&#160;newton_raphson.c'],['../newton__raphson_8c.html#ac6837f695990867bd72cf23ee93961cc',1,'newton_raphson(uint8_t f_length, uint8_t n, vector_t x0_arr[], double eps, uint8_t max_it_num, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][n])):&#160;newton_raphson.c']]],
  ['newton_5fraphson_2ec_196',['newton_raphson.c',['../newton__raphson_8c.html',1,'']]],
  ['newton_5fraphson_2eh_197',['newton_raphson.h',['../newton__raphson_8h.html',1,'']]],
  ['non_5flin_5falgorithm_198',['NON_LIN_ALGORITHM',['../fsolve_8h.html#a806eab83c4b4a937b86ebcfd984f9206',1,'fsolve.h']]],
  ['non_5flinear_5falgebra_199',['NON_LINEAR_ALGEBRA',['../group__non__linear__algebra.html',1,'']]],
  ['norm_5fdist_5frnd_5fgenerator_2ec_200',['norm_dist_rnd_generator.c',['../norm__dist__rnd__generator_8c.html',1,'']]],
  ['norm_5fdist_5frnd_5fgenerator_2eh_201',['norm_dist_rnd_generator.h',['../norm__dist__rnd__generator_8h.html',1,'']]],
  ['nw_202',['NW',['../magnetic__based__position_8h.html#af9cccf331f045b89a9f12366df5f7687',1,'magnetic_based_position.h']]]
];
