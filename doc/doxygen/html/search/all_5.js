var searchData=
[
  ['fsolve_38',['fsolve',['../fsolve_8c.html#a690a05654627b94634ffed0893946ba7',1,'fsolve(uint8_t f_length, uint8_t x0_length, vector_t x0_arr[], enum NON_LIN_ALGORITHM algo, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][x0_length])):&#160;fsolve.c'],['../fsolve_8h.html#a690a05654627b94634ffed0893946ba7',1,'fsolve(uint8_t f_length, uint8_t x0_length, vector_t x0_arr[], enum NON_LIN_ALGORITHM algo, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][x0_length])):&#160;fsolve.c']]],
  ['fsolve_2ec_39',['fsolve.c',['../fsolve_8c.html',1,'']]],
  ['fsolve_2eh_40',['fsolve.h',['../fsolve_8h.html',1,'']]],
  ['fsolve_5ftest_41',['fsolve_test',['../fsolve__test_8c.html#a53bbdeb6ad57eeb4b512c4620b64cc37',1,'fsolve_test(void):&#160;fsolve_test.c'],['../fsolve__test_8h.html#a53bbdeb6ad57eeb4b512c4620b64cc37',1,'fsolve_test(void):&#160;fsolve_test.c']]],
  ['fsolve_5ftest_2ec_42',['fsolve_test.c',['../fsolve__test_8c.html',1,'']]],
  ['fsolve_5ftest_2eh_43',['fsolve_test.h',['../fsolve__test_8h.html',1,'']]]
];
