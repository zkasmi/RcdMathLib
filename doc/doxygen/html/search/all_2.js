var searchData=
[
  ['col_5fnum_3',['col_num',['../structmatrix__dim__t.html#aac6b6ff205184047714d5c22019bdbab',1,'matrix_dim_t']]],
  ['combi_5fempty_4',['COMBI_EMPTY',['../combinatorics_8h.html#a744ae7aa581aa908643d95d470394872',1,'combinatorics.h']]],
  ['combi_5fend_5',['COMBI_END',['../combinatorics_8h.html#a16396d8294127e6ea7dbbdaa3b8132e7',1,'combinatorics.h']]],
  ['combi_5ferror_6',['COMBI_ERROR',['../combinatorics_8h.html#a1249f9e7a382225c0889b8ef630d6509',1,'combinatorics.h']]],
  ['combi_5fsuccess_7',['COMBI_SUCCESS',['../combinatorics_8h.html#af92c32bbf9a391a055cb472b6f43c498',1,'combinatorics.h']]],
  ['combinatorics_2ec_8',['combinatorics.c',['../combinatorics_8c.html',1,'']]],
  ['combinatorics_2eh_9',['combinatorics.h',['../combinatorics_8h.html',1,'']]],
  ['combinatorics_5fget_5fnext_5fwithout_5frep_10',['combinatorics_get_next_without_rep',['../combinatorics_8c.html#ae500ce949afe79089e8146eb1fb5cbb0',1,'combinatorics_get_next_without_rep(uint8_t n, uint8_t k, uint8_t comb_arr[]):&#160;combinatorics.c'],['../combinatorics_8h.html#ae500ce949afe79089e8146eb1fb5cbb0',1,'combinatorics_get_next_without_rep(uint8_t n, uint8_t k, uint8_t comb_arr[]):&#160;combinatorics.c']]],
  ['combinatorics_5finit_11',['combinatorics_init',['../combinatorics_8c.html#a1bcaeef00a7027e9b9b7694da2efd1b9',1,'combinatorics_init(uint8_t n, uint8_t k, uint8_t comb_arr[]):&#160;combinatorics.c'],['../combinatorics_8h.html#a1bcaeef00a7027e9b9b7694da2efd1b9',1,'combinatorics_init(uint8_t n, uint8_t k, uint8_t comb_arr[]):&#160;combinatorics.c']]],
  ['creating_20an_20application_12',['Creating an application',['../creating-an-application.html',1,'']]],
  ['creating_20modules_13',['Creating modules',['../creating-modules.html',1,'']]]
];
