var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuv",
  1: "m",
  2: "cdfghlmnopqstuv",
  3: "cdfghilmnopqrstuv",
  4: "cr",
  5: "anq",
  6: "dghmn",
  7: "acikmnprsv",
  8: "bdelmnopsu",
  9: "cgr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

