var searchData=
[
  ['qr_5falgorithm_230',['QR_ALGORITHM',['../qr__common_8h.html#ac62c54358ecc0c35cdef1c3fb37cc73b',1,'qr_common.h']]],
  ['qr_5fcommon_2ec_231',['qr_common.c',['../qr__common_8c.html',1,'']]],
  ['qr_5fcommon_2eh_232',['qr_common.h',['../qr__common_8h.html',1,'']]],
  ['qr_5fcommon_5fbackward_5fsubst_233',['qr_common_backward_subst',['../qr__common_8h.html#adad26253c0e08a0d9fe2dfe0f6d64112',1,'qr_common_backward_subst(uint8_t m, uint8_t n, matrix_t U[][n], matrix_t b[m], matrix_t x_sol[m]):&#160;qr_common.c'],['../qr__common_8c.html#adad26253c0e08a0d9fe2dfe0f6d64112',1,'qr_common_backward_subst(uint8_t m, uint8_t n, matrix_t U[][n], matrix_t b[m], matrix_t x_sol[m]):&#160;qr_common.c']]],
  ['qr_5fcommon_5fget_5freduced_5fqr_234',['qr_common_get_reduced_QR',['../qr__common_8h.html#a3144f99da14a69ec80bf9ded23714612',1,'qr_common_get_reduced_QR(uint8_t m, uint8_t n, matrix_t Q[m][m], matrix_t R[m][n], matrix_t red_Q[m][n], matrix_t red_R[n][n]):&#160;qr_common.c'],['../qr__common_8c.html#a47e88b1e236d3637717ea144811cdc1b',1,'qr_common_get_reduced_QR(uint8_t m, uint8_t n, matrix_t Q[m][m], matrix_t R[m][n], matrix_t reduc_Q[m][n], matrix_t reduc_R[n][n]):&#160;qr_common.c']]],
  ['qr_5fget_5fpinv_235',['qr_get_pinv',['../qr__pseudo__inverse_8h.html#a9fb1ebdfe21c5a66cd322f00bf886d8d',1,'qr_get_pinv(uint8_t m, uint8_t n, matrix_t A[m][n], matrix_t pinv_A[n][m], enum QR_ALGORITHM algo):&#160;qr_pseudo_inverse.c'],['../qr__pseudo__inverse_8c.html#a9fb1ebdfe21c5a66cd322f00bf886d8d',1,'qr_get_pinv(uint8_t m, uint8_t n, matrix_t A[m][n], matrix_t pinv_A[n][m], enum QR_ALGORITHM algo):&#160;qr_pseudo_inverse.c']]],
  ['qr_5fgivens_2ec_236',['qr_givens.c',['../qr__givens_8c.html',1,'']]],
  ['qr_5fgivens_2eh_237',['qr_givens.h',['../qr__givens_8h.html',1,'']]],
  ['qr_5fgivens_5fdecomp_238',['qr_givens_decomp',['../qr__givens_8h.html#a07adca97c58afc057a5cff738f6473cc',1,'qr_givens_decomp(uint8_t m, uint8_t n, matrix_t A[][n], uint8_t q_col_num, matrix_t Q[][q_col_num], bool reduced):&#160;qr_givens.c'],['../qr__givens_8c.html#a07adca97c58afc057a5cff738f6473cc',1,'qr_givens_decomp(uint8_t m, uint8_t n, matrix_t A[][n], uint8_t q_col_num, matrix_t Q[][q_col_num], bool reduced):&#160;qr_givens.c']]],
  ['qr_5fgivens_5fget_5fparams_239',['qr_givens_get_params',['../qr__givens_8h.html#a53f748fbc6f8a129deb6a048d423d644',1,'qr_givens_get_params(matrix_t xjj, matrix_t xij, matrix_t c_s_t_r_vec[]):&#160;qr_givens.c'],['../qr__givens_8c.html#a53f748fbc6f8a129deb6a048d423d644',1,'qr_givens_get_params(matrix_t xjj, matrix_t xij, matrix_t c_s_t_r_vec[]):&#160;qr_givens.c']]],
  ['qr_5fhouseholder_2ec_240',['qr_householder.c',['../qr__householder_8c.html',1,'']]],
  ['qr_5fhouseholder_2eh_241',['qr_householder.h',['../qr__householder_8h.html',1,'']]],
  ['qr_5fhouseholder_5fdecomp_242',['qr_householder_decomp',['../qr__householder_8h.html#a926e8bce91b658f64837d5c93b0bc74f',1,'qr_householder_decomp(uint8_t m, uint8_t n, matrix_t A[][n], uint8_t q_col_num, matrix_t Q[][q_col_num], bool reduced):&#160;qr_householder.c'],['../qr__householder_8c.html#a926e8bce91b658f64837d5c93b0bc74f',1,'qr_householder_decomp(uint8_t m, uint8_t n, matrix_t A[][n], uint8_t q_col_num, matrix_t Q[][q_col_num], bool reduced):&#160;qr_householder.c']]],
  ['qr_5fpinv_5ftest_243',['qr_pinv_test',['../qr__pinv__test_8h.html#a292d2351f35310e579dd0741ab4857db',1,'qr_pinv_test(void):&#160;qr_pinv_test.c'],['../qr__pinv__test_8c.html#a292d2351f35310e579dd0741ab4857db',1,'qr_pinv_test(void):&#160;qr_pinv_test.c']]],
  ['qr_5fpinv_5ftest_2ec_244',['qr_pinv_test.c',['../qr__pinv__test_8c.html',1,'']]],
  ['qr_5fpinv_5ftest_2eh_245',['qr_pinv_test.h',['../qr__pinv__test_8h.html',1,'']]],
  ['qr_5fpseudo_5finverse_2ec_246',['qr_pseudo_inverse.c',['../qr__pseudo__inverse_8c.html',1,'']]],
  ['qr_5fpseudo_5finverse_2eh_247',['qr_pseudo_inverse.h',['../qr__pseudo__inverse_8h.html',1,'']]]
];
