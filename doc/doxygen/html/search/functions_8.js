var searchData=
[
  ['newton_5fraphson_544',['newton_raphson',['../newton__raphson_8h.html#ac6837f695990867bd72cf23ee93961cc',1,'newton_raphson(uint8_t f_length, uint8_t n, vector_t x0_arr[], double eps, uint8_t max_it_num, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][n])):&#160;newton_raphson.c'],['../newton__raphson_8c.html#ac6837f695990867bd72cf23ee93961cc',1,'newton_raphson(uint8_t f_length, uint8_t n, vector_t x0_arr[], double eps, uint8_t max_it_num, vector_t est_x_arr[], void(*get_non_lin_sys)(vector_t x_arr[], vector_t f_vec[]), void(*get_jacobian)(vector_t x_arr[], matrix_t J[][n])):&#160;newton_raphson.c']]]
];
