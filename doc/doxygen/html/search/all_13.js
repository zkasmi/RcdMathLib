var searchData=
[
  ['utilities_296',['UTILITIES',['../group__utilities.html',1,'']]],
  ['utils_2ec_297',['utils.c',['../utils_8c.html',1,'']]],
  ['utils_2eh_298',['utils.h',['../utils_8h.html',1,'']]],
  ['utils_5fget_5fmedian_299',['utils_get_median',['../utils_8h.html#a971d455baef2d25c61e94c4bc4ac13ee',1,'utils_get_median(vector_t arr[], uint8_t length):&#160;utils.c'],['../utils_8c.html#a971d455baef2d25c61e94c4bc4ac13ee',1,'utils_get_median(vector_t arr[], uint8_t length):&#160;utils.c']]],
  ['utils_5fget_5fsave_5fsquare_5froot_300',['utils_get_save_square_root',['../utils_8h.html#a8cda5a6c15e99e2b76abfb04316301d8',1,'utils_get_save_square_root(double x, double y):&#160;utils.c'],['../utils_8c.html#a8cda5a6c15e99e2b76abfb04316301d8',1,'utils_get_save_square_root(double x, double y):&#160;utils.c']]],
  ['utils_5fmax_301',['utils_max',['../utils_8h.html#a0ae8ea233ea6c519cd0434f8c5bde5e5',1,'utils_max(double a, double b):&#160;utils.c'],['../utils_8c.html#a0ae8ea233ea6c519cd0434f8c5bde5e5',1,'utils_max(double a, double b):&#160;utils.c']]],
  ['utils_5fmean_302',['utils_mean',['../utils_8h.html#acda1e3a1b76af6e0e57b507472986ad6',1,'utils_mean(uint8_t arr_size, vector_t in_arr[]):&#160;utils.c'],['../utils_8c.html#acda1e3a1b76af6e0e57b507472986ad6',1,'utils_mean(uint8_t arr_size, vector_t in_arr[]):&#160;utils.c']]],
  ['utils_5fmin_303',['utils_min',['../utils_8h.html#ab734a543e5543adb2bd98e1bf279a23d',1,'utils_min(double a, double b):&#160;utils.c'],['../utils_8c.html#ab734a543e5543adb2bd98e1bf279a23d',1,'utils_min(double a, double b):&#160;utils.c']]],
  ['utils_5fmoving_5faverage_304',['utils_moving_average',['../utils_8h.html#a535a04a4bff82c88599e4648dfa7cfe1',1,'utils_moving_average(uint8_t arr_size, vector_t in_arr[], uint8_t window_size, vector_t out_arr[]):&#160;utils.c'],['../utils_8c.html#a535a04a4bff82c88599e4648dfa7cfe1',1,'utils_moving_average(uint8_t arr_size, vector_t in_arr[], uint8_t window_size, vector_t out_arr[]):&#160;utils.c']]],
  ['utils_5fprintf_305',['utils_printf',['../utils_8h.html#a335d8d6c4b945638261228cd116cccd6',1,'utils_printf(char *format_str,...):&#160;utils.c'],['../utils_8c.html#a335d8d6c4b945638261228cd116cccd6',1,'utils_printf(char *format_str,...):&#160;utils.c']]],
  ['utils_5fsind_306',['utils_sind',['../utils_8h.html#aad6440ba12fa7e2e1fae5ba6dc99cabf',1,'utils_sind(double deg_angle):&#160;utils.c'],['../utils_8c.html#aad6440ba12fa7e2e1fae5ba6dc99cabf',1,'utils_sind(double deg_angle):&#160;utils.c']]],
  ['utils_5fswap_307',['utils_swap',['../utils_8h.html#ad21f654c48115ba2419ca98644a44ae9',1,'utils_swap(uint8_t *a, uint8_t *b):&#160;utils.c'],['../utils_8c.html#ad21f654c48115ba2419ca98644a44ae9',1,'utils_swap(uint8_t *a, uint8_t *b):&#160;utils.c']]],
  ['utils_5ftest_308',['utils_test',['../utils__test_8h.html#a91c61f3d2a5985466b46f5b15ad0faf7',1,'utils_test(void):&#160;utils_test.c'],['../utils__test_8c.html#a91c61f3d2a5985466b46f5b15ad0faf7',1,'utils_test(void):&#160;utils_test.c']]],
  ['utils_5ftest_2ec_309',['utils_test.c',['../utils__test_8c.html',1,'']]],
  ['utils_5ftest_2eh_310',['utils_test.h',['../utils__test_8h.html',1,'']]],
  ['utils_5fto_5fradian_311',['utils_to_radian',['../utils_8h.html#a689d2d3db40d341609423404abc5a23a',1,'utils_to_radian(double deg_angle):&#160;utils.c'],['../utils_8c.html#a689d2d3db40d341609423404abc5a23a',1,'utils_to_radian(double deg_angle):&#160;utils.c']]],
  ['utils_5fu8_5fmax_312',['utils_u8_max',['../utils_8h.html#a8163bb6a1c9742b9607fd87c6b3df5c5',1,'utils_u8_max(uint8_t a, uint8_t b):&#160;utils.c'],['../utils_8c.html#a8163bb6a1c9742b9607fd87c6b3df5c5',1,'utils_u8_max(uint8_t a, uint8_t b):&#160;utils.c']]],
  ['utils_5fu8_5fmin_313',['utils_u8_min',['../utils_8h.html#a81dabd4841ab656130497f0600979850',1,'utils_u8_min(uint8_t a, uint8_t b):&#160;utils.c'],['../utils_8c.html#a81dabd4841ab656130497f0600979850',1,'utils_u8_min(uint8_t a, uint8_t b):&#160;utils.c']]]
];
