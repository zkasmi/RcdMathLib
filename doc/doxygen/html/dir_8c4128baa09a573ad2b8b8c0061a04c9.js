var dir_8c4128baa09a573ad2b8b8c0061a04c9 =
[
    [ "basic_operations", "dir_c416454c2678f04387f9348cb110b9ee.html", "dir_c416454c2678f04387f9348cb110b9ee" ],
    [ "matrix_decompositions", "dir_997ac76a3d72a25dec85442c5f6c585d.html", "dir_997ac76a3d72a25dec85442c5f6c585d" ],
    [ "pseudo_inverse", "dir_26c6b27917397f8a74a0fdde30fa8c1c.html", "dir_26c6b27917397f8a74a0fdde30fa8c1c" ],
    [ "solve_linear_equations", "dir_f6d35df7bd2aa2399e29232b65f55b23.html", "dir_f6d35df7bd2aa2399e29232b65f55b23" ],
    [ "utilities", "dir_08749d3fb0d5743bfaf3c7bce248dc40.html", "dir_08749d3fb0d5743bfaf3c7bce248dc40" ]
];