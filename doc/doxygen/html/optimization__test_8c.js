var optimization__test_8c =
[
    [ "optimization_exponential_data_test", "optimization__test_8c.html#a5f1f1ed377535a6d4c5758c38174fcb9", null ],
    [ "optimization_get_exp_f", "optimization__test_8c.html#ac11e842f01767e97924c100df684476d", null ],
    [ "optimization_get_exp_Jacobian", "optimization__test_8c.html#a98fdeac6ec9a38cb63f48e7c0f0085f0", null ],
    [ "optimization_get_f_error", "optimization__test_8c.html#a610651298b8018db96cf21ee31740d3b", null ],
    [ "optimization_get_J", "optimization__test_8c.html#ac7361316d224b72baa4a681297d9f51c", null ],
    [ "optimization_get_sin_f", "optimization__test_8c.html#a1daefb42bf903899e7e1d5237ba62c0b", null ],
    [ "optimization_get_sin_Jacobian", "optimization__test_8c.html#acb161b62c37921a9edd0cb7aeb76342a", null ],
    [ "optimization_sinusoidal_data_test", "optimization__test_8c.html#a5f89a9747d6a7bec1ad0a80ba31647b9", null ],
    [ "optimization_test", "optimization__test_8c.html#a1062ae4598fabcf957ec0b855737db20", null ]
];