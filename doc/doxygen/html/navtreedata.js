/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "RcdMathLib_doc", "index.html", [
    [ "RcdMathLib Documentation", "index.html", [
      [ "RcdMathLib in a nutshell", "index.html#RcdMathLib-in-a-nutshell", null ],
      [ "Download and use the RcdMathLib", "index.html#download-use-RcdMathLib", null ],
      [ "The quickest start", "index.html#the-quickest-start", null ],
      [ "Structure", "index.html#structure", [
        [ "Linear Algebra", "index.html#autotoc_md0", null ],
        [ "Non-Linear Algebra", "index.html#autotoc_md1", null ],
        [ "Localization", "index.html#autotoc_md2", null ],
        [ "examples", "index.html#autotoc_md3", null ],
        [ "doc", "index.html#autotoc_md4", null ]
      ] ],
      [ "Further information", "index.html#further-information", null ]
    ] ],
    [ "Creating an application", "creating-an-application.html", [
      [ "Creating an application for full-fledged devices", "creating-an-application.html#creating-an-application-full-fledge", null ],
      [ "Creating an application for resource-limited devices", "creating-an-application.html#creating-an-application-res-lim", null ],
      [ "The main function", "creating-an-application.html#the-main-function", null ],
      [ "The application's Makefile", "creating-an-application.html#the-applications-makefile", [
        [ "The minimal Makefile", "creating-an-application.html#the-minimal-makefile", null ],
        [ "Including modules", "creating-an-application.html#including-modules", null ]
      ] ]
    ] ],
    [ "Creating modules", "creating-modules.html", [
      [ "The general structure", "creating-modules.html#the-general-structure", null ],
      [ "Module dependencies", "creating-modules.html#module-dependencies", null ]
    ] ],
    [ "Getting started", "getting-started.html", [
      [ "Downloading RcdMathLib code", "getting-started.html#downloading-RcdMathLib-code", null ],
      [ "Compiling RcdMathLib", "getting-started.html#compiling-RcdMathLib", [
        [ "Setting up a toolchain for full-fledged devices", "getting-started.html#setting-up-a-toolchain-full-fledge", null ],
        [ "Setting up a toolchain for resource-limited devices", "getting-started.html#setting-up-a-toolchain-res-lim", null ],
        [ "The build system for full-fledged devices", "getting-started.html#the-build-system-full-fledge", null ],
        [ "The build system for resource-limited devices", "getting-started.html#the-build-system-res-lim", null ],
        [ "Building and executing an example for resource-limited devices", "getting-started.html#building-and-executing-an-example-res-lim", null ]
      ] ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"DOP_8c.html",
"magnetic__based__position_8c.html#ae584dff239a0111c47b7c5d1e197af59",
"qr__givens_8h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';