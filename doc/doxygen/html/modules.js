var modules =
[
    [ "EXAMPLES", "group__examples.html", "group__examples" ],
    [ "LINEAR_ALGEBRA", "group__linear__algebra.html", "group__linear__algebra" ],
    [ "LOCALIZATION", "group__localization.html", "group__localization" ],
    [ "NON_LINEAR_ALGEBRA", "group__non__linear__algebra.html", "group__non__linear__algebra" ]
];