var pseudo__inverse_8h =
[
    [ "ALGORITHM", "pseudo__inverse_8h.html#aeead1612e2f846fe32e4c50e1ab3d7c3", [
      [ "Moore_Penrose", "pseudo__inverse_8h.html#aeead1612e2f846fe32e4c50e1ab3d7c3ae835310f82eb01d07137e72133cd5b06", null ],
      [ "Householder", "pseudo__inverse_8h.html#aeead1612e2f846fe32e4c50e1ab3d7c3a9db08339065f28367b9ae058d1127a04", null ],
      [ "Givens", "pseudo__inverse_8h.html#aeead1612e2f846fe32e4c50e1ab3d7c3aab4d4fdd6aa659e7306fa9abf3d0a991", null ],
      [ "Gauss", "pseudo__inverse_8h.html#aeead1612e2f846fe32e4c50e1ab3d7c3ab15a7891aa5223439e4692a1048cb220", null ]
    ] ]
];