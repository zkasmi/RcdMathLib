var group__linear__algebra =
[
    [ "BASIC_OPERATIONS", "group__basic__operations.html", "group__basic__operations" ],
    [ "MATRIX_DECOMPOSITIONS", "group__matrix__decompositions.html", "group__matrix__decompositions" ],
    [ "PSEUDO_INVERSE", "group__pseudo__inverse.html", "group__pseudo__inverse" ],
    [ "SOLVE_LINEAR_EQUATIONS", "group__solve__linear__equations.html", "group__solve__linear__equations" ],
    [ "UTILITIES", "group__utilities.html", "group__utilities" ]
];