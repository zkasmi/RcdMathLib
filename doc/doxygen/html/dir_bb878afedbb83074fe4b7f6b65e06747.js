var dir_bb878afedbb83074fe4b7f6b65e06747 =
[
    [ "basic_operations", "dir_d1d208ee40a2f911c6ff49bd490e63cd.html", "dir_d1d208ee40a2f911c6ff49bd490e63cd" ],
    [ "matrix_decompositions", "dir_7288dc322cfbdfeb7861adfdccb1ddcd.html", "dir_7288dc322cfbdfeb7861adfdccb1ddcd" ],
    [ "pseudo_inverse", "dir_1513adee4b8f281871e1ff2ded4af7fe.html", "dir_1513adee4b8f281871e1ff2ded4af7fe" ],
    [ "solve_linear_equations", "dir_c00c1b9b31ec8172d0e33f1bbb777699.html", "dir_c00c1b9b31ec8172d0e33f1bbb777699" ],
    [ "utilities", "dir_8e5c3f26a7da9560da5fd7be1ee471fe.html", "dir_8e5c3f26a7da9560da5fd7be1ee471fe" ]
];