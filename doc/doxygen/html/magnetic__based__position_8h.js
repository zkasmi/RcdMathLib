var magnetic__based__position_8h =
[
    [ "AR", "magnetic__based__position_8h.html#a6f98ae7b0908254a0dfd1627e652bebe", null ],
    [ "I0", "magnetic__based__position_8h.html#a73d113d91ab7ec355ca707cc930eb718", null ],
    [ "K", "magnetic__based__position_8h.html#a97d832ae23af4f215e801e37e4f94254", null ],
    [ "K_T", "magnetic__based__position_8h.html#a3ee4ec231172a5fd866f72acead15bc4", null ],
    [ "MG_TO_T", "magnetic__based__position_8h.html#a544d6868e9b6cf4f9b9db9e1ad413691", null ],
    [ "MILPS_MAX_DIST", "magnetic__based__position_8h.html#a2ab193fcd7e0bc8f46ce92c3299b99bc", null ],
    [ "MU0", "magnetic__based__position_8h.html#a0fa11f33bdb89b468b60f19395381cc4", null ],
    [ "NW", "magnetic__based__position_8h.html#af9cccf331f045b89a9f12366df5f7687", null ],
    [ "PI", "magnetic__based__position_8h.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "R0", "magnetic__based__position_8h.html#a30ab9da60435727bc80839d416461b8f", null ],
    [ "magnetic_based_get_absolute_error", "magnetic__based__position_8h.html#ad5a7513337486e277ea028480403569d", null ],
    [ "magnetic_based_get_distances", "magnetic__based__position_8h.html#ae584dff239a0111c47b7c5d1e197af59", null ],
    [ "magnetic_based_get_distances_to_anchors", "magnetic__based__position_8h.html#ac852e4c0dcb8e9901aa3fab5efd91599", null ],
    [ "magnetic_based_get_magnetic_field", "magnetic__based__position_8h.html#a361f3f3c4f9516a1aa6107367ebfda8d", null ],
    [ "magnetic_based_get_magnetic_field_vec", "magnetic__based__position_8h.html#aac026f28838a5b30c8f6f189e3ab354c", null ],
    [ "magnetic_based_get_r", "magnetic__based__position_8h.html#a01bb024303e8eb202a19dbd70b291c06", null ],
    [ "magnetic_based_preprocessing_get_position", "magnetic__based__position_8h.html#af316f67cb87759b57bc815326b8d1cef", null ]
];