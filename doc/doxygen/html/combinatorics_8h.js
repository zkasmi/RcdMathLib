var combinatorics_8h =
[
    [ "COMBI_EMPTY", "combinatorics_8h.html#a744ae7aa581aa908643d95d470394872", null ],
    [ "COMBI_END", "combinatorics_8h.html#a16396d8294127e6ea7dbbdaa3b8132e7", null ],
    [ "COMBI_ERROR", "combinatorics_8h.html#a1249f9e7a382225c0889b8ef630d6509", null ],
    [ "COMBI_SUCCESS", "combinatorics_8h.html#af92c32bbf9a391a055cb472b6f43c498", null ],
    [ "combinatorics_get_next_without_rep", "combinatorics_8h.html#ae500ce949afe79089e8146eb1fb5cbb0", null ],
    [ "combinatorics_init", "combinatorics_8h.html#a1bcaeef00a7027e9b9b7694da2efd1b9", null ]
];