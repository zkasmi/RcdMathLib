var group__examples =
[
    [ "matrix_test.h", "matrix__test_8h.html", null ],
    [ "vector_test.h", "vector__test_8h.html", null ],
    [ "givens_test.h", "givens__test_8h.html", null ],
    [ "householder_test.h", "householder__test_8h.html", null ],
    [ "lu_decomp_test.h", "lu__decomp__test_8h.html", null ],
    [ "svd_test.h", "svd__test_8h.html", null ],
    [ "moore_penrose_pinv_test.h", "moore__penrose__pinv__test_8h.html", null ],
    [ "qr_pinv_test.h", "qr__pinv__test_8h.html", null ],
    [ "solve_test.h", "solve__test_8h.html", null ],
    [ "utils_test.h", "utils__test_8h.html", null ],
    [ "distance_based_test.h", "distance__based__test_8h.html", null ],
    [ "magnetic_based_test.h", "magnetic__based__test_8h.html", null ],
    [ "pos_algos_common_test.h", "pos__algos__common__test_8h.html", null ],
    [ "multipath_algo_own_norm_distr_test.h", "multipath__algo__own__norm__distr__test_8h.html", null ],
    [ "position_optimization_test.h", "position__optimization__test_8h.html", null ],
    [ "optimization_test.h", "optimization__test_8h.html", null ],
    [ "fsolve_test.h", "fsolve__test_8h.html", null ]
];