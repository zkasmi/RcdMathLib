var ieee__mobile__computing__non__lin__alg_8c =
[
    [ "ieee_mobile_comp_exponential_data_test", "ieee__mobile__computing__non__lin__alg_8c.html#ac9fa8d6b7e20b065ebecd1339bc0f66b", null ],
    [ "ieee_mobile_comp_get_exp_f", "ieee__mobile__computing__non__lin__alg_8c.html#a1903dbf211715600f9830349fb69aaf7", null ],
    [ "ieee_mobile_comp_get_exp_Jacobian", "ieee__mobile__computing__non__lin__alg_8c.html#acd3427ef350de38a6e8a2446d7c9ff48", null ],
    [ "ieee_mobile_comp_get_sin_f", "ieee__mobile__computing__non__lin__alg_8c.html#a65a525d446eb62da17cd1b6b7242f3bd", null ],
    [ "ieee_mobile_comp_get_sin_Jacobian", "ieee__mobile__computing__non__lin__alg_8c.html#ad46e7112151698bf6ec5d1df6c1b3785", null ],
    [ "ieee_mobile_comp_sinusoidal_data_test", "ieee__mobile__computing__non__lin__alg_8c.html#a441918d542f57adcbf82b36643218e99", null ]
];