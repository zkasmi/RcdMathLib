# Creating an application     {#creating-an-application}

\author Zakaria Kasmi

[TOC]

An application can be created for full-fledged or resource-limited devices.

# Creating an application for full-fledged devices    {#creating-an-application-full-fledge}

We recommend to use the [Eclipse IDE] (https://www.eclipse.org/) for C/C++ Developers for creating
own application. The simplest way to write your own application, is to put your *.c and *.h files under the 
`src` directory. Another way is to create a directory containing the multiple C file(s) with your
source code. The header files can be imported in the Eclipse IDE by opening Project->Properties and 
selecting  ''C/C++ General->Paths and Symbols->Languages->GNU C'' from the Selection Wizard. Click the ''Add..'' 
button to browse to the header-directory and the select the ''Apply'' and ''OK'' buttons. The users can be 
oriented to the main.c and the examples in the 
[main-directory](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/) of the RcdMathLib. 

  
# Creating an application for resource-limited devices  {#creating-an-application-res-lim}

To create your own application for a resource-limited device you need to create a directory
containing one or multiple C file(s) with your source code and a Makefile. An example Makefile is
available in the `src` folder of the [Eclipse project for resource-limited devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_resource_limited_devices.zip).

# The main function           {#the-main-function}

RIOT starts two threads the idle and main threads after the board is initialized.
The idle thread has the lowest priority while the main thread has a priority that
is in the middle between the lowest and the highest available priorities. The main 
thread is the first that runs and calls the `main()` function.
This function needs to be defined in the source code of each application
(typically located in the `main.c` file). 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
#include <stdio.h>

#include "matrix_test.h"
#include "vector_test.h"

int main(void)
{
    puts("RcdMathLib Application!");
    // Test the basic operations module;
    matrix_test();
    vector_test();
    
    return EXIT_SUCCESS;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above C code shows an application testing the basic operations sub-modules. 
This application run operations of the vector and matrix sub-modules. 

# The application's Makefile  {#the-applications-makefile}

## The minimal Makefile           {#the-minimal-makefile}

At minimum the Makefile of an application (see @ref getting-started) needs to
define the following macros:
 * `APPLICATION`: contains the name of your application
 * `RIOTBASE`: specifies the path to your copy of the RIOT repository (note,
   the `$(CURDIR)` macro can be used to give a relative path)
 * `RCDMATHLIB`: specifies the path to the copy of the RcdMathLib repository (note,
   the `$(CURDIR)` macro can be used to give a relative path)
 * `USEMODULE`: specifies the module of the RcdMathLib that you may want to use

The `BOARD` macro is also required and recommended to be set to `native` by
default, but is recommended to be overridable with the `?=` operator.
Additionally, it is required to include the `Makefile.include` from the RcdMathLib and from the
`RIOTBASE`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.mk}
# Set the name of your application:
APPLICATION = foobar

# If no BOARD is found in the environment, use this default:
BOARD ?= native

# This has to be the absolute path to the RcdMathLib base directory:
RCDMATHLIB ?= $(CURDIR)/../RcdMathLib

# This has to be the absolute path to the RIOT base directory:
RIOTBASE ?= $(CURDIR)/../../RIOT


include $(RCDMATHLIB)/Makefile.include
include $(RIOTBASE)/Makefile.include
~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Including modules          {#including-modules}

The modules of the RcdMathLib as well as of the RcdMathLib can be included.
In order to use additional modules, such as a particular driver or a system library, the modules' 
names must be appended to the USEMODULE variable. For example, to build an application using the SHT11 
temperature sensor and UDP/IPv6 functionalities, the Makefile needs to contain the following lines: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.mk}
USEMODULE += sht11
USEMODULE += gnrc_ipv6_default
USEMODULE += gnrc_udp
~~~~~~~~~~~~~~~~~~~~~~~~~~~

For example, to create an application using the matrix decompositions, the pseudo-inverse, and solving
linear equations sub-modules, the Makefile must comprise the following lines: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.mk}
USEMODULE += matrix_decompositions
USEMODULE += pseudo_inverse
USEMODULE += utilities
~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
