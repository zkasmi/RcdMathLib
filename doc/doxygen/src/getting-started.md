# Getting started                                              {#getting-started}


\author Zakaria Kasmi

[TOC]

# Downloading RcdMathLib code   {#downloading-RcdMathLib-code}                         

You can obtain the latest RcdMathLib code from
our [GitLab](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/) repository either by
[downloading the Eclipse project for resource-limited devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_resource_limited_devices.zip) zip file, the [Eclipse project for full-fledged devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_full_fledged_devices.zip) zip file, or by cloning the [git repository](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/).

In order to clone the RcdMathLib repository, you need the
[Git revision control system](http://git-scm.com/) and run the following command:

~~~~~~~~ {.sh}
git clone https://git.imp.fu-berlin.de/zkasmi/RcdMathLib.git
~~~~~~~~

# Compiling RcdMathLib   {#compiling-RcdMathLib}                                             


## Setting up a toolchain for full-fledged devices {#setting-up-a-toolchain-full-fledge}                    

RcdMathLib can be run on full-fledged devices such as a Personal Computer
(PC) or a server. It can also be executed on the embedded system Raspberry Pi 
running an Operating System (OS) such as the Raspbian or the Ubuntu Core. We recommend 
the GNU Compiler Collection (GCC) to compile the RcdMathLib on operating systems like the 
Linux or Windows. We also recommend the use of the [Eclipse (IDE)](https://www.eclipse.org/).

The PATH environment variable should be set with bin-directory of the GCC compiler.
On a typical shell like bash or zsh this can be done using export, e.g.

<BLOCKQUOTE>
export PATH=${PATH}:/path/to/gcc/bin
</BLOCKQUOTE>

For windows, add the bin-directory to your PATH under System Properties->Advanced->
Environment Variables, e.g.

<BLOCKQUOTE>
"C:\gcc\bin"
</BLOCKQUOTE>

## Setting up a toolchain for resource-limited devices   {#setting-up-a-toolchain-res-lim}            

Depending on the hardware you want to use, you need to first install a
corresponding toolchain. We recommend the use of the [RIOT-OS](https://riot-os.org/), since 
it supports 8-bit platforms (e.g. Arduino Mega 2560), 16-bit platforms (e.g. MSP430), and 32-bit 
platforms (e.g. ARM). The [RIOT-OS](https://riot-os.org/) is an open source IoT operating system 
developed at the ''Freie Universität Berlin''. In general, an OS allows the management and sharing 
of resources as well as the development of multi-tasking applications in a computer system. The 
[Wiki](https://github.com/RIOT-OS/RIOT/wiki) on RIOT's Github page contains a lot of information 
that can help you with your platform:

* [ARM-based platforms](https://github.com/RIOT-OS/RIOT/wiki/Family:-ARM)
* [TI MSP430](https://github.com/RIOT-OS/RIOT/wiki/Family:-MSP430)
* [Atmel ATmega](https://github.com/RIOT-OS/RIOT/wiki/Family%3A-ATmega)
* [native](https://github.com/RIOT-OS/RIOT/wiki/Family:-native)

## The build system for full-fledged devices       {#the-build-system-full-fledge}                           

RcdMathLib can be built using the [Eclipse IDE](https://www.eclipse.org/) for C/C++ Developers.
The simplest way to compile and link an application with the RcdMathLib is: Firstly, download the 
[RcdMathLib_full_fledged_devices.zip](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_full_fledged_devices.zip) 
file or clone it as follows:

~~~~~~~~ {.sh}
git clone https://git.imp.fu-berlin.de/zkasmi/RcdMathLib.git
cd RcdMathLib/eclipse_projects/
~~~~~~~~

After cloning or downloading the RcdMathLib, the Eclipse version of the RcdMathLib can be used as follows:
* Create a workspace folder (e.g. rcd_math_lib_workspace).
* Change the current directory to the ''rcd_math_lib_workspace'' directory and unzip the 
  RcdMathLib_full_fledged_devices.zip file.
  ~~~~~~~~ {.sh}
   cd rcd_math_lib_workspace
   unzip RcdMathLib_full_fledged_devices.zip 
   ~~~~~~~~
* Start the Eclipse IDE and browse to the created workspace folder.
* Compile the whole project.

Another method to use the RcdMathLib in the Eclipse IDE is:
* Clone the whole:
   ~~~~~~~~ {.sh}
   git clone https://git.imp.fu-berlin.de/zkasmi/RcdMathLib.git
   ~~~~~~~~
* Create a new C project by choosing the project type under Executable: `Hello World ANSI C Project`,
   as well as the `Cross GCC` Tool-chains.
* Try to compile and execute the `Hello World ANSI C Project`.
* Copy the RcdMathLib as well as the [eclipse_path_includes_settings.xml](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/eclipse_path_includes_settings.xml) 
  configuration file into the src-directory of your project.
* Import the 'eclipse_path_includes_settings.xml' file as follows:

  <BLOCKQUOTE>
   * Open File -> Import -> C/C++ and Select ''C/C++ Project Settings'' from the Selection Wizard.
   * Click Next->''Select Settings file''-->Browse.
   * After Browsing to the xml-configuration file, click on Finish.
  </BLOCKQUOTE>
* Compile the whole project.
* Optional: replace the main()-function with the main.c file of the cloned RcdMathLib version for full-fledged devices.

## The build system for resource-limited devices   {#the-build-system-res-lim}                  


RcdMathLib and RIOT use [GNU make](https://www.gnu.org/software/make/) as build system. The
simplest way to compile and link an application with RcdMathLib, is to use the 
[Eclipse project for resource-limited devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_resource_limited_devices.zip) 
and set up a Makefile providing at least the following variables:

 * `APPLICATION`: should contain the (unique) name of your application
 * `BOARD`: specifies the platform the application should be build for by
   default
 * `RIOTBASE`: specifies the path to the copy of the RIOT repository (note,
   the `$(CURDIR)` macro can be used to give a relative path)
 * `RCDMATHLIB`: specifies the path to the copy of the RcdMathLib repository (note,
   the `$(CURDIR)` macro can be used to give a relative path)
 * `USEMODULE`: specifies the module of the RcdMathLib that you may want to use

Additionally it has to include the `Makefile.include`, located in RcdMathLib's as well as RIOT's 
root directories. A minimal application Makefile looks like this:

~~~~~~~~ {.mk}
# a minimal application Makefile
APPLICATION = mini-makefile
BOARD ?= native
RIOTBASE ?= $(CURDIR)/../RIOT
RCDMATHLIB ?= $(CURDIR)/../RcdMathLib
USEMODULE += basic_operations

include $(RIOTBASE)/MicroPosMath-Lib/Makefile.include
include $(RIOTBASE)/Makefile.include
~~~~~~~~

The `?=` operator can be used in order to allow overwriting variables from the command line.
For example, the target platform can be specified from the command line as follows:


~~~~~~~~ {.sh}
make BOARD=stm32f4discovery
~~~~~~~~

In this case the STM32F4 discovery board is specified. Furthermore, the basic operations sub-module is 
selected by the `USEMODULE` macro.

Other sub-modules such as the matrix decompositions or utilities modules can be selected, e.g.:

~~~~~~~~ {.mk}
USEMODULE += matrix_decompositions 
USEMODULE += utilities  
~~~~~~~~
The dependency of the RcdMathLib modules and sub-modules to each other is automatically calculated 
by special Makefiles located in the module directories.

Besides typical targets like `clean`, `all`, or `doc`, RIOT provides the
special targets `flash` and `term` to invoke the configured flashing and
terminal tools for the specified platform. These targets use the variable
`PORT` for the serial communication to the device. Neither this variable nor
the targets `flash` and `term` are mandatory for the native port.

Unless specified otherwise, make will create an elf-file as well as an Intel
hex file in the `bin` folder of your application directory.
 
Please visit the [Wiki](https://github.com/RIOT-OS/RIOT/wiki/The-Make-Build-System) to learn more
about the build system of the RIOT.


## Building and executing an example for resource-limited devices  {#building-and-executing-an-example-res-lim} 


RcdMathLib provides a number of examples in the 
[examples-directory](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/examples). Every example
has a README that documents its usage and its purpose. Furthermore the examples are described in the 
[doc-directory](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/doc/doxygen). You can build your 
own application or the examples by typing:

~~~~~~~~ {.sh}
make BOARD=stm32f4discovery
~~~~~~~~

or

~~~~~~~~ {.sh}
make all BOARD=stm32f4discovery
~~~~~~~~

into your shell.

To flash the application to a board just type

~~~~~~~~ {.sh}
make flash BOARD=stm32f4discovery
~~~~~~~~

You can then access the board via the serial interface:

~~~~~~~~ {.sh}
make term BOARD=stm32f4discovery
~~~~~~~~

If you are using multiple boards you can use the `PORT` macro to specify the
serial interface:

~~~~~~~~ {.sh}
make term BOARD=stm32f4discovery PORT=/dev/ttyACM1
~~~~~~~~

We use `pyterm` as the default terminal application. It is shipped with RIOT in
the `dist/tools/pyterm/` directory. If you choose to use another terminal
program you can set `TERMPROG` (and if need be the `TERMFLAGS`) macros:

~~~~~~~~ {.sh}
make -C examples/gnrc_networking/ term \
    BOARD=samr21-xpro \
    TERMPROG=gtkterm \
    TERMFLAGS="-s 115200 -p /dev/ttyACM0 -e"
~~~~~~~~

Please visit the [Wiki](https://github.com/RIOT-OS/RIOT/wiki) to learn more
about flashing devices.