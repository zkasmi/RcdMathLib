# Creating modules            {#creating-modules}


\author Zakaria Kasmi

[TOC]

Well-defined units of code in the RcdMathLib that provide a set of features are encapsulated in a
module. RcdMathLib is module-based and composed of the following main modules:

* Linear algebra module
* Non-Linear algebra module
* Localization module

Each main module includes sub-modules, for more details see 
@ref structure "The structure of the RcdMathLib".

@note The following chapters concerning only resource-limited devices. 

# The general structure       {#the-general-structure}

Modules are directories containing source and header files as well as a Makefile.
Furthermore, their API can be defined in one or more header files, residing in the include path of
their super-module.

For example, the matrix sub-module is implemented in the @ref basic_operations sub-module, in the 
`linear_algebra/basic_operations` directory. Its API is defined in
`linear_algebra/basic_operations/matrix.h`. 

A module's Makefile just needs to include `Makefile.base` in the RIOT repository as well as 
`Makefile.base` and `Makefile.include` in the RcdMathLib repository:

~~~~~~~~~~~~~~~~~~~ {.mk}
include $(RIOTBASE)/Makefile.base
include $(RCDMATHLIB)/linear_algebra/matrix_decompositions/Makefile.include
include $(RCDMATHLIB)/linear_algebra/matrix_decompositions/Makefile.dep 
~~~~~~~~~~~~~~~~~~~

The `Makefile.base` and `Makefile.include` macros in the example above are includes for 
the linear algebra module.

If your module's name differs from the name of the directory it resides in you
need to set the `MODULE` macro in addition.

The `Makefile.dep` serves to define dependencies and the `Makefile.include` to append target
specific information to variables like INCLUDES. Modules can be used by adding their name to the
`USEMODULE` macro of the application's Makefile.


# Module dependencies {#module-dependencies}

The module may depend on other modules to minimize code duplication. These
dependencies are defined in `Makefile.dep` with the following syntax:

~~~~~~~~~~~~~~~~~~~ {.mk}
ifneq (,$(filter your_module,$(USEMODULE))) # if module in USEMODULE
  USEMODULE += dep1                         # add dependencies to USEMODULE
  USEMODULE += dep2
endif
~~~~~~~~~~~~~~~~~~~

@note `Makefile.dep` is processed only once therefore, the dependency block for a module must be 
added *before*  dependencies pull in their dependencies.

