# RcdMathLib Documentation                                       {#mainpage}

\author Zakaria Kasmi

[TOC]

# RcdMathLib in a nutshell    {#RcdMathLib-in-a-nutshell}

RcdMathLib is an open-source library for numerical linear and non-linear 
algebra designed to match the requirements of resource-limited or embedded 
devices. RcdMathLib supports solving linear and non-linear equation systems.
Furthermore, it provides general-purpose implemented methods that facilitate 
solving problems of regression smoothing and curve fitting. It also allows for 
calculating as well as optimizing a position on a mobile device.


# Download and use the RcdMathLib  {#download-use-RcdMathLib}                                


The simplest way to use the RcdMathLib for resource-limited devices is to 
download the [Eclipse project for resource-limited devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_resource_limited_devices.zip) 
zip file. Another [Eclipse project for full-fledged devices](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/blob/master/eclipse_projects/RcdMathLib_full_fledged_devices.zip) 
zip file enables also the use of the RcdMathLib for full-fledged platforms 
or single-board computers such as a [Raspberry Pi](https://www.elinux.org/RPi_Hub). 


# The quickest start          {#the-quickest-start}

You can run the RcdMathLib on most resource-limited devices such as the 
[STM32F4 Discovery board](https://www.st.com/en/evaluation-tools/stm32f4discovery.html) 
as well as on full-fledged computers like a PC, or single-board devices such as a [Raspberry Pi](https://www.elinux.org/RPi_Hub). 
Try it right now in your terminal window:

~~~~~~~{.sh}
mkdir RcdMathLib_resource_limited_devices 
cd RcdMathLib_resource_limited_devices
git clone https://git.imp.fu-berlin.de/zkasmi/RcdMathLib.git    # assumption: git is pre-installed
unzip RcdMathLib_resource_limited_devices.zip                                                                  
cd RcdMathLib/examples/linear_algebra/basic_operation/
make all
~~~~~~~

The example above shows how to use the basic operations of the RcdMathLib such as the vector or
the matrix operations.

# Structure    {#structure}


This section walks you through RcdMathLib's structure to easily find your way around in RcdMathLib's code base.


![Software Architecture](soft_architecture_01_09_2019.svg)


RcdMathLib's source code is composed into three groups:

 - Linear algebra
 - Non-linear algebra
 - Localization
 

In addition RcdMathLib includes various [examples](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/examples) 
to familiarize the user with the software as well as an API to facilitate the use and the further development of the library. 
The structural groups are projected onto the [directory structure](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib) of 
RcdMathLib, where each of these groups resides in one or two directories in the [main](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/) 
RcdMathLib directory.

The following list gives a more detailed description of each of RcdMathLib's
top-level directories:

## Linear Algebra

This directory contains functions that are specific to vector and matrix operations, and 
other algebraic operations. It provides functions to perform basic matrix operations such 
as matrix addition, multiplication, or transposition. It also provides algorithms for complex 
operations like matrix decomposition algorithms, algorithms to calculate the pseudo-inverse 
of a matrix, or methods to solve systems of linear equations. The linear algebra module is 
divided in the following sub-modules:
- Basic operations sub-module.
- Matrix decompositions sub-module.
- Pseudo-Inverse sub-module.
- Solve linear equations sub-module.
- Utilities sub-module. 


See the @ref linear_algebra module in the [main](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/) 
RcdMathLib directory as well as in the [documentation](https://git.imp.fu-berlin.de/zkasmi/RcdMathLib/-/tree/master/doc/doxygen) 
repository directory for further information and API documentations.


## Non-Linear Algebra

The non-linear algebra module contains functions to solve multi-variant nonlinear 
equations as wells algorithms solving problems of regression smoothing and curve 
fitting. This module also enables enables the optimization of an approximate solution
by using Non-linear Least Squares (NLS) methods such as modified Gauss--Newton (GN) or 
the Levenberg--Marquardt (LVM) algorithms. The non-linear algebra module is 
divided in the two following sub-modules:

- Solve non-linear equations sub-module.
- Optimization sub-module.

See here @ref non_linear_algebra module for further information.

## Localization

The localization module contains functions to compute a position of a mobile device 
using distance measurements or DC-pulsed, magnetic signals. This module also includes 
optimization algorithms such as the Levenberg--Marquardt approach to optimize the 
calculated position. The localization module also involves a method to recognize and 
mitigate the multipath errors on the mobile station.

In the `position_algos` sub-directory you can find the implementations of the 
distance-based localization system (see the @ref distance_based module) as well as 
the implementations of the DC-pulsed, magnetic position system (see the @ref magnetic_based
module). The `pos_algos_common` sub-directory contains common localization algorithms like 
the trilateration method (see the @ref pos_algos_common module). The optimization algorithms are 
localted in the `optimization` sub-directory providing the following optimization approaches:

- Gauss--Newton (GN) algorithm.
- Levenberg--Marquardt (LVM) algorithm.  
- Multipath Distance Detection and Mitigation (MDDM) algorithm.

See the @ref localization module for more detailed information.

## examples

Here you find a number of example applications that demonstrate certain
features of RcdMathLib. The examples found in this directory is a good
starting point for anyone who is new to RcdMathLib.

For more information best browse that directory and have a look at the
`README.md` files that ship with each example.

To create your own application - here or anywhere else - see @ref creating-an-application


## doc

The `doc` directory contains the doxygen configuration and also contains the compiled doxygen
output after running `make doc`.



# Further information        {#further-information}

 - @ref getting-started
 - @ref creating-an-application
 - @ref creating-modules

